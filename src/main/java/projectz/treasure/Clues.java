package projectz.treasure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;

import projectz.Main;
import projectz.treasure.ClueScrollHandler.ClueScrollDifficulty;
import projectz.treasure.ClueScrollHandler.ClueScrollType;

public class Clues {
    
    private static int CoordinateRange, CoordinateRangeNether;
    
    public static ClueScrollType[] easyClueTypes = {
        ClueScrollType.CRAFT_ITEM,
        ClueScrollType.MINE_BLOCK,
        ClueScrollType.KILL_X_ENEMY,
        ClueScrollType.CATCH_FISH,
    };
    
    public static ClueScrollType[] medClueTypes = {
        ClueScrollType.CRAFT_ITEM,
        ClueScrollType.MINE_BLOCK,
        ClueScrollType.KILL_X_ENEMY,
        ClueScrollType.CATCH_FISH,
        ClueScrollType.DIG_AT_COORDS
    };
    
    public static ClueScrollType[] hardClueTypes = {
        ClueScrollType.CRAFT_ITEM,
        ClueScrollType.MINE_BLOCK,
        ClueScrollType.KILL_X_ENEMY,
        ClueScrollType.CATCH_FISH,
        ClueScrollType.DIG_AT_COORDS,
        ClueScrollType.DIG_AT_NETHER_COORDS
    };
    
    public static Map<ClueScrollDifficulty, List<Material>> CraftItem = new HashMap<ClueScrollDifficulty, List<Material>>();
    public static Map<ClueScrollDifficulty, List<Material>> MineBlock = new HashMap<ClueScrollDifficulty, List<Material>>();
    public static Map<ClueScrollDifficulty, List<ItemStack>> CatchFish = new HashMap<ClueScrollDifficulty, List<ItemStack>>();
    public static Map<ClueScrollDifficulty, Map<EntityType, Integer[]>> KillMobs = new HashMap<ClueScrollDifficulty, Map<EntityType, Integer[]>>();
    
    public static void load(YamlConfiguration config) {
        CoordinateRange = config.getInt("COORDINATE_RANGE");
        CoordinateRangeNether = config.getInt("COORDINATE_RANGE_NETHER");
        
        ConfigurationSection craftitem = config.getConfigurationSection("CRAFT_ITEM");
        ConfigurationSection mineblock = config.getConfigurationSection("MINE_BLOCK");
        ConfigurationSection catchfish = config.getConfigurationSection("CATCH_FISH");
        ConfigurationSection killmobs = config.getConfigurationSection("KILL_X_ENEMY");
        
        for (String section : craftitem.getKeys(false)) {
            List<Material> materials = new ArrayList<Material>();
            for (String value : craftitem.getStringList(section)) {
                materials.add(Material.valueOf(value.toUpperCase()));
            }
            CraftItem.put(ClueScrollDifficulty.valueOf(section.toUpperCase()), materials);
        }
        for (String section : mineblock.getKeys(false)) {
            List<Material> materials = new ArrayList<Material>();
            for (String value : mineblock.getStringList(section)) {
                materials.add(Material.valueOf(value.toUpperCase()));
            }
            MineBlock.put(ClueScrollDifficulty.valueOf(section.toUpperCase()), materials);
        }
        for (String section : catchfish.getKeys(false)) {
            List<ItemStack> fish = new ArrayList<ItemStack>();
            for (String value : catchfish.getStringList(section)) {
                String[] parts = value.split(",");
                fish.add(new ItemStack(Material.RAW_FISH, Integer.parseInt(parts[0]), Short.parseShort(parts[1])));
            }
            CatchFish.put(ClueScrollDifficulty.valueOf(section.toUpperCase()), fish);
        }
        for (String section : killmobs.getKeys(false)) {
            Map<EntityType, Integer[]> mobs = new HashMap<EntityType, Integer[]>();
            for (String value : killmobs.getStringList(section)) {
                String[] parts = value.split(",");
                mobs.put(EntityType.valueOf(parts[0].toUpperCase()), new Integer[] {Integer.parseInt(parts[1]), Integer.parseInt(parts[2])});
            }
            KillMobs.put(ClueScrollDifficulty.valueOf(section.toUpperCase()), mobs);
        }
    }
    
    public static ClueScrollType getRandomClueType(ClueScrollDifficulty type) {
        switch (type) {
        case MEDIUM:
            return medClueTypes[Main.random(medClueTypes.length - 1)];
        case HARD:
            return hardClueTypes[Main.random(hardClueTypes.length - 1)];
        default:
            return easyClueTypes[Main.random(easyClueTypes.length - 1)];
        }
    }
    
    public static Location getCoordinateClue(boolean nether) {
        if (nether) {
            Location loc = new Location(Main.getNether(), (Main.random(1) == 0 ? -1 : 1) * Main.random(CoordinateRangeNether), 64, (Main.random(1) == 0 ? -1 : 1) * Main.random(CoordinateRangeNether));
            loc.setY(Main.getNether().getHighestBlockYAt(loc));
            return loc;
        }
        Location loc = new Location(Main.getOverworld(), (Main.random(1) == 0 ? -1 : 1) * Main.random(CoordinateRange), 64, (Main.random(1) == 0 ? -1 : 1) * Main.random(CoordinateRange));
        loc.setY(Main.getOverworld().getHighestBlockYAt(loc));
        return loc;
    }
    
    public static Material getCraftItemClue(ClueScrollDifficulty diff) {
        List<Material> possible = CraftItem.get(diff);
        return possible.get(Main.random(possible.size() - 1));
    }
    
    public static Material getMineBlockClue(ClueScrollDifficulty diff) {
        List<Material> possible = MineBlock.get(diff);
        return possible.get(Main.random(possible.size() - 1));
    }
    
    public static ItemStack getCatchFishClue(ClueScrollDifficulty diff) {
        List<ItemStack> possible = CatchFish.get(diff);
        ItemStack i = possible.get(Main.random(possible.size() - 1));
        i.setAmount(Main.random(i.getAmount()) + 1);
        return i;
    }
    
    public static EntityType getKillMobClueType(ClueScrollDifficulty diff) {
        Map<EntityType, Integer[]> m = KillMobs.get(diff);
        List<EntityType> possibilities = new ArrayList<EntityType>();
        possibilities.addAll(m.keySet());
        return possibilities.get(Main.random(possibilities.size() - 1));
    }
    
    public static int getKillMobClueAmount(ClueScrollDifficulty diff, EntityType type) {
        Integer[] range = KillMobs.get(diff).get(type);
        return range[0] + Main.random(range[1] - range[0]);
    }

}
