package projectz.treasure.cluescrolls;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import projectz.Main;
import projectz.treasure.CasketHandler;
import projectz.treasure.ClueScrollHandler;
import projectz.treasure.ClueScrollHandler.ClueScrollDifficulty;
import projectz.treasure.ClueScrollHandler.ClueScrollType;

public class ClueScroll {
    
    public ClueScroll(ClueScrollType type, ClueScrollDifficulty difficulty, List<String> messages) {
        this.type = type;
        this.messages = new ArrayList<String>();
        this.diff = difficulty;
        this.messages.add(ChatColor.RED + "Clue Difficulty: " + Main.capitalizeFirst(difficulty.toString()));
        this.messages.addAll(messages);
    }
    
    private ClueScrollType type;
    private ClueScrollDifficulty diff;
    private List<String> messages;
    
    public ClueScrollType getType() {
        return type;
    }
    
    public List<String> getMessage() {
        return messages;
    }
    
    public ItemStack toItem() {
        ItemStack i = new ItemStack(Material.EMPTY_MAP, 1);
        ItemMeta meta = i.getItemMeta();
        meta.setDisplayName(ChatColor.GOLD + "Clue Scroll (" + Main.capitalizeFirst(diff.toString()) + ")");
        meta.setLore(messages);
        i.setItemMeta(meta);
        return i;
    }
    
    public void complete(Player p) {
        p.getInventory().removeItem(toItem());
        if (Main.random(ClueScrollHandler.getClueLength(diff) - 1) == 0) {
            ItemStack i;
            switch (diff) {
            case HARD:
                i = CasketHandler.OccultCasket;
                break;
            case MEDIUM:
                i = CasketHandler.PolishedCasket;
                break;
                default:
                    i = CasketHandler.RustyCasket;
                    break;
            }
            p.getInventory().addItem(i);
            p.sendMessage(ChatColor.GREEN + "Congratulations! You've completed this Clue Scroll!");
            p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0F, 0.5F);
            p.updateInventory();
        } else {
            p.getInventory().addItem(ClueScrollHandler.getRandomClueScroll(diff).toItem());
            p.sendMessage(ChatColor.GRAY + "You find another Clue Scroll upon completing your previous one!");
            p.playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_VILLAGER_CONVERTED, 1.0F, 1.0F);
        }
    }
    
    public static ClueScroll toClueScroll(ItemStack cluescroll) {
        Bukkit.getLogger().info("toClueScroll");
        List<String> lore = cluescroll.getItemMeta().getLore();
        ClueScrollDifficulty difficulty = ClueScrollDifficulty.valueOf(ChatColor.stripColor(lore.get(0).split(": ")[1].toUpperCase()));
        ClueScrollType type = ClueScrollType.getTypeFromPhrase(lore.get(1));
        List<String> messages = new ArrayList<String>();
            for (int i = 1; i < lore.size(); i++) {
                messages.add(lore.get(i));
            }
            return new ClueScroll(type, difficulty, messages);
    }
    
    public static boolean isClueScroll(ItemStack item) {
        return item.getItemMeta().getLore() == null ? false :
            (item.getItemMeta().getLore().get(0).contains(ChatColor.RED + "Clue Difficulty: ") ? true : false);
    }
    
    public static ClueScroll containsClueScoll(Inventory inv) {
        Bukkit.getLogger().info("contains clue?");
        ItemStack[] contents = inv.getContents();
        if (inv.contains(Material.EMPTY_MAP)) {
            for (int i = 0; i < 9; i++) {
                ItemStack item = contents[i];
                if (item != null && item.getType().equals(Material.EMPTY_MAP)) {
                    if (isClueScroll(item)) {
                        Bukkit.getLogger().info("Contains Clue!");
                        return toClueScroll(item);
                    }
                }
            }
        }
        return null;
    }

}