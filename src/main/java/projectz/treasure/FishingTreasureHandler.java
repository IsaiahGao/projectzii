package projectz.treasure;

import java.util.List;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;

import projectz.Main;
import projectz.Utilities;

public class FishingTreasureHandler {
    
    private static List<ItemStack> FISHING_ROD_JUNK;
    private static List<ItemStack> FISHING_ROD_TREASURE;
    private static List<ItemStack> SMALL_FISHING_NET_JUNK;
    private static List<ItemStack> SMALL_FISHING_NET_TREASURE;
    private static List<ItemStack> BIG_FISHING_NET_JUNK;
    private static List<ItemStack> BIG_FISHING_NET_TREASURE;
    
    public static void load(FileConfiguration config) {
        ConfigurationSection fishing = config.getConfigurationSection("fishing-treasures");
        FISHING_ROD_JUNK = Utilities.MaterialDataAmountWeightFormat(fishing.getStringList("fishing-rod.junk"));
        FISHING_ROD_TREASURE = Utilities.MaterialDataAmountWeightFormat(fishing.getStringList("fishing-rod.treasure"));
        SMALL_FISHING_NET_JUNK = Utilities.MaterialDataAmountWeightFormat(fishing.getStringList("small-fishing-net.junk"));
        SMALL_FISHING_NET_TREASURE = Utilities.MaterialDataAmountWeightFormat(fishing.getStringList("small-fishing-net.treasure"));
        BIG_FISHING_NET_JUNK = Utilities.MaterialDataAmountWeightFormat(fishing.getStringList("big-fishing-net.junk"));
        BIG_FISHING_NET_TREASURE = Utilities.MaterialDataAmountWeightFormat(fishing.getStringList("big-fishing-net.treasure"));
    }

    public enum FishingTreasure {
        FISHING_ROD(FISHING_ROD_JUNK, FISHING_ROD_TREASURE, "1/200"),
        SMALL_FISHING_NET(SMALL_FISHING_NET_JUNK, SMALL_FISHING_NET_TREASURE, "1/200"),
        BIG_FISHING_NET(BIG_FISHING_NET_JUNK, BIG_FISHING_NET_TREASURE, "1/200");
        
        private FishingTreasure(List<ItemStack> junk, List<ItemStack> treasure, String chance) {
            String[] s = chance.split("/");
            this.Chance = Integer.parseInt(s[0]);
            this.outOf = Integer.parseInt(s[1]);
            this.junk = junk;
            this.treasure = treasure;
        }
        
        private List<ItemStack> junk;
        private List<ItemStack> treasure;
        private int Chance, outOf;
        
        public ItemStack getRandomTreasure(float chanceBoost) {
            return (Main.random(outOf - 1) <= (Chance - 1) * (1 + chanceBoost)) ?
                    treasure.get(Main.random(treasure.size() - 1)) :
                    junk.get(Main.random(junk.size() - 1));
        }
    }
    
}
