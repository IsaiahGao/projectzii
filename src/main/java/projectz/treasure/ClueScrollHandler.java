package projectz.treasure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;

import projectz.Main;
import projectz.treasure.cluescrolls.ClueScroll;


public class ClueScrollHandler {
    
    public enum ClueScrollType {
        
        DIG_AT_COORDS("Dig near these coordinates to complete this clue:"),
        DIG_AT_NETHER_COORDS("Dig near the following location to complete this clue:"),
        CRAFT_ITEM("Craft the following item:"),
        MINE_BLOCK("Break the following block:"),
        CATCH_FISH("Catch:"),
        KILL_X_ENEMY("Kill the following entity:");
        
        private ClueScrollType(String phrase) {
            this.phrase = phrase;
        }
        
        private String phrase;
        
        public String getPhrase() {
            return phrase;
        }
        
        private static Map<String, ClueScrollType> byPhrase = new HashMap<String, ClueScrollType>();
        
        static {
            for (ClueScrollType t : ClueScrollType.values()) {
                byPhrase.put(t.getPhrase(), t);
            }
        }
        
        public static ClueScrollType getTypeFromPhrase(String phrase) {
            Bukkit.getLogger().info(phrase);
            return byPhrase.get(ChatColor.stripColor(phrase));
        }
        
    }
    
    public enum ClueScrollDifficulty {
        
        EASY(Clues.easyClueTypes),
        MEDIUM(Clues.medClueTypes),
        HARD(Clues.hardClueTypes);
        
        private ClueScrollDifficulty(ClueScrollType[] possibilities) {
            this.p = possibilities;
        }
        
        private ClueScrollType[] p;
        
        public ClueScrollType getRandomClueType() {
            return p[Main.random(p.length - 1)];
        }
    }
    
    public static ClueScroll getRandomClueScroll(ClueScrollDifficulty d) {
        ClueScrollType type = Clues.getRandomClueType(d);
        List<String> lore = new ArrayList<String>();
        switch (type) {
        case CRAFT_ITEM:
            lore.add(ChatColor.GRAY + "Craft the following item:");
            lore.add(ChatColor.WHITE + Main.capitalizeFirst(Clues.getCraftItemClue(d).toString()));
            lore.add(ChatColor.GRAY + "to complete this clue.");
            break;
        case MINE_BLOCK:
            lore.add(ChatColor.GRAY + "Break the following block:");
            lore.add(ChatColor.WHITE + Main.capitalizeFirst(Clues.getMineBlockClue(d).toString()));
            lore.add(ChatColor.GRAY + "to complete this clue.");
            break;
        case KILL_X_ENEMY:
            EntityType e = Clues.getKillMobClueType(d);
            lore.add(ChatColor.GRAY + "Kill the following entity:");
            lore.add(ChatColor.WHITE + Main.capitalizeFirst(e.toString()));
            lore.add(ChatColor.WHITE + "" + Clues.getKillMobClueAmount(d, e) + ChatColor.GRAY + " more times");
            lore.add(ChatColor.GRAY + "to complete this clue.");
            break;
        case CATCH_FISH:
            ItemStack fish = Clues.getCatchFishClue(d);
            lore.add(ChatColor.GRAY + "Catch:");
            lore.add(ChatColor.WHITE + "" + fish.getAmount() + ChatColor.GRAY + " more " + ChatColor.WHITE + getFishName(fish.getDurability()));
            lore.add(ChatColor.GRAY + "to complete this clue.");
            break;
        case DIG_AT_COORDS:
            Location loc = Clues.getCoordinateClue(false);
            lore.add(ChatColor.GRAY + "Dig near these coordinates to complete this clue:");
            lore.add(ChatColor.WHITE + "X: " + loc.getX() + ", Y: " + loc.getY() + ", Z: " + loc.getZ());
            break;
        case DIG_AT_NETHER_COORDS:
            Location nloc = Clues.getCoordinateClue(true);
            lore.add(ChatColor.GRAY + "Dig near the following location to complete this clue:");
            lore.add(ChatColor.WHITE + "X: " + nloc.getX() + ", Y: " + nloc.getY() + ", Z: " + nloc.getZ());
            lore.add(ChatColor.GRAY + "Consider bringing a fire-proof spade or");
            lore.add(ChatColor.GRAY + "fire protection; you may need to place");
            lore.add(ChatColor.GRAY + "and break a block yourself.");
            break;
        }
        return new ClueScroll(type, d, lore);
    }
    
    public static String getFishName(short data) {
        switch (data) {
        case 1:
            return "Salmon";
        case 3:
            return "Pufferfish";
        case 2:
            return "Clownfish";
            default:
                return "Normal fish";
        }
    }
    
    public static short getFishData(String name) {
        if (name.equals("Salmon")) return 1;
        if (name.equals("Pufferfish")) return 3;
        if (name.equals("Clownfish")) return 2;
        return 0;
    }
    
    public static int getClueLength(ClueScrollDifficulty d) {
        switch (d) {
            case EASY:
                return 3;
            case MEDIUM:
                return 3;
            case HARD:
                return 4;
                default:
                    return 4;
        }
    }
    
}
