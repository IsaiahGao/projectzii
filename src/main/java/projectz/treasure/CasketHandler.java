package projectz.treasure;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import projectz.Main;
import projectz.Utilities;
import utilities.particles.ParticleEffects;

public class CasketHandler {
    
    public static final ItemStack RustyCasket = new ItemStack(Material.CHEST, 1);
    public static final ItemStack PolishedCasket = new ItemStack(Material.TRAPPED_CHEST, 1);
    public static final ItemStack OccultCasket = new ItemStack(Material.ENDER_CHEST, 1);
    public static List<ItemStack> RUSTY_CASKET_LOOT;
    public static List<ItemStack> POLISHED_CASKET_LOOT;
    public static List<ItemStack> OCCULT_CASKET_LOOT;
    
    static {
        ItemMeta meta = RustyCasket.getItemMeta();
        List<String> lore = new ArrayList<String>();
        lore.add(ChatColor.GRAY + "An old chest crusted with tarnish.");
        lore.add(ChatColor.GRAY + "I wonder what's inside?");
        meta.setDisplayName(ChatColor.RESET + "" + ChatColor.RED + "Rusty Casket");
        meta.setLore(lore);
        RustyCasket.setItemMeta(meta);
        
        lore.set(0, ChatColor.GRAY + "An opulent chest studded with gems.");
        meta.setDisplayName(ChatColor.RESET + "" + ChatColor.GREEN + "Polished Casket");
        meta.setLore(lore);
        PolishedCasket.setItemMeta(meta);
        
        meta = OccultCasket.getItemMeta();
        lore.set(0, ChatColor.GRAY + "A glowing chest covered in arcane runes.");
        meta.setDisplayName(ChatColor.RESET + "" + ChatColor.AQUA + "Occult Casket");
        meta.setLore(lore);
        OccultCasket.setItemMeta(meta);
    }
    
    public static void load(FileConfiguration config) {
        ConfigurationSection caskets = config.getConfigurationSection("casket-loot");
        RUSTY_CASKET_LOOT = Utilities.MaterialDataAmountWeightFormat(caskets.getStringList("rusty-casket"));
        POLISHED_CASKET_LOOT = Utilities.MaterialDataAmountWeightFormat(caskets.getStringList("polished-casket"));
        OCCULT_CASKET_LOOT = Utilities.MaterialDataAmountWeightFormat(caskets.getStringList("occult-casket"));
    }
    
    public static Casket getCasketType(Material mat) {
        if (mat.equals(Material.ENDER_CHEST))
            return Casket.OCCULT;
        return mat.equals(Material.TRAPPED_CHEST) ? Casket.POLISHED : Casket.RUSTY;
    }
    
    public enum Casket {
        RUSTY(Material.CHEST, RUSTY_CASKET_LOOT, ParticleEffects.SMOKE_LARGE, ParticleEffects.CLOUD),
        POLISHED(Material.TRAPPED_CHEST, POLISHED_CASKET_LOOT, ParticleEffects.FIREWORKS_SPARK, ParticleEffects.VILLAGER_HAPPY),
        OCCULT(Material.ENDER_CHEST, OCCULT_CASKET_LOOT, ParticleEffects.FLAME, ParticleEffects.WITCH_MAGIC);
        
        private Casket(Material chest, List<ItemStack> loot, ParticleEffects eff1, ParticleEffects eff2) {
            this.chesttype = chest;
            this.loots = loot;
            this.eff1 = eff1;
            this.eff2 = eff2;
        }
        
        private Material chesttype;
        private List<ItemStack> loots;
        private ParticleEffects eff1;
        private ParticleEffects eff2;
        
        public boolean dropChest(final Player p) {
            Location ploc = p.getLocation();
            Vector facing = ploc.getDirection();
            Block block = new Location(p.getWorld(), ploc.getX() + facing.getX(), ploc.getY(), ploc.getZ() + facing.getZ()).getBlock();
            if (!block.getType().equals(Material.AIR)) {
                p.sendMessage(ChatColor.RED + "You need a clear spot in front of you to open that.");
                p.playSound(ploc, Sound.ENTITY_VILLAGER_NO, 1.0F, 1.0F);
                return false;
            }
            final Location target = block.getLocation().add(0.5, 0.5, 0.5);
            block.getWorld().playSound(target, Sound.ENTITY_ENDERMEN_TELEPORT, 1.0F, 0.5F);
            new BukkitRunnable() {
                int x = 0;
                final Location top = target.clone().add(0, 8, 0);
                public void run() {
                    double angle, fx, fy, fz, fx2, fz2;
                    angle = 2 * Math.PI * x / 12;
                    fx = Math.cos(angle)/2;
                    fz = Math.sin(angle)/2;
                    fx2 = Math.cos(angle + Math.PI)/2;
                    fz2 = Math.sin(angle + Math.PI)/2;
                    fy = x * -.25;
                    eff1.display(0, 0, 0, 0.0001F, 1, top.clone().add(fx, fy, fz), 36);
                    eff1.display(0, 0, 0, 0.0001F, 1, top.clone().add(fx2, fy, fz2), 36);
                    if (eff2 != null)
                        eff2.display(0, 0, 0, 0.0001F, 1, top.clone().add(0, fy, 0), 36);
                    x++;
                    if (x == 34) {
                        this.cancel();
                        dropChest(target, p.getName());
                    }
                }
            }.runTaskTimer(Main.getPlugin(), 0L, 2L);
            return true;
        }
        
        private void dropChest(Location target, String playername) {
            final Block block = target.getBlock();
            final Location loc = block.getLocation().add(0.5, 0.5, 0.5);
            block.setType(chesttype);
            block.setMetadata("casket.holder", new FixedMetadataValue(Main.getPlugin(), playername));
                eff1.display(0, 0, 0, 0.1F, 30, loc, 36);
                loc.getWorld().playSound(loc, Sound.ENTITY_ZOMBIE_BREAK_DOOR_WOOD, 0.4F, 1.2F);
                loc.getWorld().playSound(loc, Sound.ENTITY_HORSE_ARMOR, 1.0F, 1.2F);
                new BukkitRunnable() {
                    public void run() {
                        eff1.display(0, 0, 0, 0.1F, 4, loc, 36);
                        if (block.getType() != chesttype) {
                            block.removeMetadata("casket.holder", Main.getPlugin());
                            this.cancel();
                        }
                    }
                }.runTaskTimer(Main.getPlugin(), 0L, 5L);
        }
        
        public void openChest(Block b, Player p) {
            int itemAmount = Main.random(3) + 3;
            p.getWorld().playSound(p.getLocation(), Sound.BLOCK_CHEST_OPEN, 1.0F, 1.0F);
            ParticleEffects.EXPLOSION_LARGE.display(0, 0, 0, 0.1F, 1, b.getLocation().add(0.5, 0.5, 0.5), 36);
            b.setType(Material.AIR);
            Inventory lootDisplay = Bukkit.createInventory(null, 27, Main.capitalizeFirst(this.name()) + " Casket");
            for (int i = 0; i < itemAmount; i++) {
                ItemStack slotLoot = Utilities.randomTermFromList(loots);
                if (slotLoot != null) {
                    slotLoot.setAmount(Main.random(slotLoot.getAmount() - 1) + 1);
                    lootDisplay.setItem(Main.random(26), slotLoot);
                }
            }
            p.openInventory(lootDisplay);
        }
    }

}
