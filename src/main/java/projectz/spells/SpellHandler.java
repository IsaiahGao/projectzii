package projectz.spells;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.Location;

import projectz.spells.spells.*;

public class SpellHandler {

    private static Map<String, Spell> spells = new HashMap<String, Spell>();
    
    static {
        spells.put(ChatColor.YELLOW + "" + ChatColor.BOLD + "Healing Spell", new HealingSpell());
        spells.put(ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "Rage Spell", new RageSpell());
        spells.put(ChatColor.BLUE + "" + ChatColor.BOLD + "Hope Spell", new HopeSpell());
        spells.put(ChatColor.GOLD + "" + ChatColor.BOLD + "Forge Spell", new ForgeSpell());
        spells.put(ChatColor.WHITE + "" + ChatColor.BOLD + "Flash Spell", new FlashSpell());
        spells.put(ChatColor.GRAY + "" + ChatColor.BOLD + "Shadow Spell", new ShadowSpell());
        spells.put(ChatColor.AQUA + "" + ChatColor.BOLD + "Freeze Spell", new FreezeSpell());
        spells.put(ChatColor.BLUE + "" + ChatColor.BOLD + "Lightning Spell", new LightningSpell());
    }
    
    public static Spell getSpell(String name) {
        return spells.get(name);
    }
    
    public static double distanceSquared2D(Location l1, Location l2) {
        double x1 = l1.getX(), x2 = l2.getX();
        double z1 = l1.getZ(), z2 = l2.getZ();
        return Math.pow(x1 - x2, 2) + Math.pow(z1 - z2, 2);
    }
    
}
