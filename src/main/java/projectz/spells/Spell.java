package projectz.spells;

import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

public abstract class Spell {
    
    public abstract void activate(Player p, boolean upgraded);
    public abstract void doSpellPulse(Entity item, Location center, boolean upgraded);
    public abstract void createCircle(Location center, boolean upgraded);

}
