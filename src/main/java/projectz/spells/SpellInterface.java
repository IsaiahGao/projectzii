package projectz.spells;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import projectz.spells.SpellCreationHandler.SpellObject;
import utilities.Utils;

public class SpellInterface {
    
    private static ItemStack level = new ItemStack(Material.EXP_BOTTLE, 1);
    
    private static ItemStack flash = new ItemStack(Material.LEATHER_BOOTS, 1);
    private static ItemStack forge = new ItemStack(Material.COAL, 1);
    private static ItemStack freeze = new ItemStack(Material.ICE, 1);
    private static ItemStack poison = new ItemStack(Material.SKULL_ITEM, 1);
    private static ItemStack haste = new ItemStack(Material.IRON_PICKAXE, 1);
    private static ItemStack healing = new ItemStack(Material.SPECKLED_MELON, 1);
    private static ItemStack hope = new ItemStack(Material.NETHER_STAR, 1);
    private static ItemStack rage = new ItemStack(Material.BLAZE_POWDER, 1);
    private static ItemStack shadow = new ItemStack(Material.EYE_OF_ENDER, 1);
    private static ItemStack lightning = new ItemStack(Material.INK_SACK, 1, (short) 4);
    
    public static final Inventory spellInterface = Bukkit.createInventory(null, 27, "Spell Creation");
    
    static {
        List<String> lore = new ArrayList<String>();

        ItemMeta meta = flash.getItemMeta();
        meta.setDisplayName(ChatColor.YELLOW + "" + ChatColor.BOLD + "Flash Spell");
        lore.add(ChatColor.RED + "Level: 18" + ChatColor.WHITE + "" + ChatColor.BOLD + " | "  + ChatColor.RESET + ChatColor.GREEN + "Upgrade: 28");
        lore.add(ChatColor.RESET + "Click to create");
        lore.add("");
        lore.add(ChatColor.GRAY + "Allows the user to run like the wind");
        lore.add(ChatColor.GRAY + "for �e20 �a(+10)�7 seconds, but causes the");
        lore.add(ChatColor.GRAY + "user's Hydration and Hunger levels to");
        lore.add(ChatColor.GRAY + "respectively drop by �e36 �a(-8)�7 and");
        lore.add("�e90% �a(-20%)�7.");
        lore.add(ChatColor.RESET + "");
        lore.add(ChatColor.GOLD + "" + ChatColor.BOLD + "Creation cost:");
        lore.add(ChatColor.YELLOW + "� 3 x Ender Pearl");
        lore.add(ChatColor.YELLOW + "� 16 x Feather");
        lore.add(ChatColor.RESET + "");
        meta.setLore(lore);
        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        flash.setItemMeta(meta);

        lore.clear();
        meta = forge.getItemMeta();
        meta.setDisplayName(ChatColor.RED + "" + ChatColor.BOLD + "Forge Spell");
        lore.add(ChatColor.RED + "Level: 28" + ChatColor.WHITE + "" + ChatColor.BOLD + " | "  + ChatColor.RESET + ChatColor.GREEN + "Upgrade: 34");
        lore.add(ChatColor.RESET + "Click to create");
        lore.add("");
        lore.add(ChatColor.GRAY + "Creates a Ring of Fusion with radius of");
        lore.add(ChatColor.YELLOW + "5 �a(+1) " + ChatColor.GRAY + "blocks that lasts up to" + ChatColor.YELLOW + " 15 �a(+5) ");
        lore.add(ChatColor.GRAY + "seconds. Magically smelts and cooks raw");
        lore.add(ChatColor.GRAY + "materials of players inside the radius.");
        lore.add(ChatColor.RESET + "");
        lore.add(ChatColor.GOLD + "" + ChatColor.BOLD + "Creation cost:");
        lore.add(ChatColor.YELLOW + "� 1 x Lava Bucket");
        lore.add(ChatColor.YELLOW + "� 8 x Blaze Powder");
        lore.add(ChatColor.YELLOW + "� 16 x Coal");
        lore.add(ChatColor.RESET + "");
        meta.setLore(lore);
        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        forge.setItemMeta(meta);

        lore.clear();
        meta = freeze.getItemMeta();
        meta.setDisplayName(ChatColor.AQUA + "" + ChatColor.BOLD + "Freeze Spell");
        lore.add(ChatColor.RED + "Level: 30" + ChatColor.WHITE + "" + ChatColor.BOLD + " | "  + ChatColor.RESET + ChatColor.GREEN + "Upgrade: 35");
        lore.add(ChatColor.RESET + "Click to create");
        lore.add("");
        lore.add(ChatColor.GRAY + "Instantly freezes EVERYTHING except for the");
        lore.add(ChatColor.GRAY + "user of the spell within �e5 �a(+1)�7 blocks of the");
        lore.add(ChatColor.GRAY + "spell's location of use for �e10 �a(+5)�7 seconds.");
        lore.add(ChatColor.GRAY + "Blocks and entities freeze temporarily.");
        lore.add(ChatColor.RESET + "");
        lore.add(ChatColor.GOLD + "" + ChatColor.BOLD + "Creation cost:");
        lore.add(ChatColor.YELLOW + "� 4 x Water Bucket");
        lore.add(ChatColor.YELLOW + "� 16 x Snow Blocks");
        lore.add(ChatColor.YELLOW + "� 16 x Ice");
        lore.add(ChatColor.RESET + "");
        meta.setLore(lore);
        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        freeze.setItemMeta(meta);

        lore.clear();
        meta = healing.getItemMeta();
        meta.setDisplayName(ChatColor.AQUA + "" + ChatColor.BOLD + "Healing Spell");
        lore.add(ChatColor.RED + "Level: 24" + ChatColor.WHITE + "" + ChatColor.BOLD + " | "  + ChatColor.RESET + ChatColor.GREEN + "Upgrade: 32");
        lore.add(ChatColor.RESET + "Click to create");
        lore.add("");
        lore.add(ChatColor.GRAY + "Creates a Ring of Healing with radius of");
        lore.add(ChatColor.YELLOW + "5 �a(+1) " + ChatColor.GRAY + "blocks that lasts up to" + ChatColor.YELLOW + " 10 �a(+2.5) ");
        lore.add(ChatColor.GRAY + "seconds, healing all players inside the");
        lore.add(ChatColor.GRAY + "ring for 1 heart every 0.67 seconds.");
        lore.add(ChatColor.RESET + "");
        lore.add(ChatColor.GOLD + "" + ChatColor.BOLD + "Creation cost:");
        lore.add(ChatColor.YELLOW + "� 1 x Golden Apple");
        lore.add(ChatColor.YELLOW + "� 9 x Glistering Melon");
        lore.add(ChatColor.RESET + "");
        meta.setLore(lore);
        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        healing.setItemMeta(meta);

        lore.clear();
        meta = hope.getItemMeta();
        meta.setDisplayName(ChatColor.BLUE + "" + ChatColor.BOLD + "Hope Spell");
        lore.add(ChatColor.RED + "Level: 24" + ChatColor.WHITE + "" + ChatColor.BOLD + " | "  + ChatColor.RESET + ChatColor.GREEN + "Upgrade: 32");
        lore.add(ChatColor.RESET + "Click to create");
        lore.add("");
        lore.add(ChatColor.GRAY + "Creates a Ring of Hopefulness with radius of");
        lore.add(ChatColor.YELLOW + "3 �a(+1) " + ChatColor.GRAY + "that lasts up to" + ChatColor.YELLOW + " 8 �a(+2) " + ChatColor.GRAY + "seconds.");
        lore.add(ChatColor.GRAY + "Restores Sanity, Hydration, Warmth, and Hunger");
        lore.add(ChatColor.GRAY + "of all players inside the ring by �e2�a (+1)�7 Points");
        lore.add(ChatColor.GRAY + "every 0.67 seconds while active.");
        lore.add(ChatColor.RESET + "");
        lore.add(ChatColor.GOLD + "" + ChatColor.BOLD + "Creation cost:");
        lore.add(ChatColor.YELLOW + "� 3 x Water Bottle");
        lore.add(ChatColor.YELLOW + "� 8 x Apple");
        lore.add(ChatColor.YELLOW + "� 32 x Glowstone Dust");
        lore.add(ChatColor.RESET + "");
        meta.setLore(lore);
        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        hope.setItemMeta(meta);

        lore.clear();
        meta = lightning.getItemMeta();
        meta.setDisplayName(ChatColor.DARK_BLUE + "" + ChatColor.BOLD + "Lightning Spell");
        lore.add(ChatColor.RED + "Level: 16" + ChatColor.WHITE + "" + ChatColor.BOLD + " | "  + ChatColor.RESET + ChatColor.GREEN + "Upgrade: 24");
        lore.add(ChatColor.RESET + "Click to create");
        lore.add("");
        lore.add(ChatColor.GRAY + "Conjures an Orb of Lightning that activates");
        lore.add(ChatColor.GRAY + "after 3 seconds, calling �e7 �a(+2)�7 bolts of");
        lore.add(ChatColor.GRAY + "lightning to strike a " + ChatColor.YELLOW + "10x10 area " + ChatColor.GRAY + "around");
        lore.add(ChatColor.GRAY + "wherever the orb was when it activated.");
        lore.add(ChatColor.RESET + "");
        lore.add(ChatColor.GOLD + "" + ChatColor.BOLD + "Creation cost:");
        lore.add(ChatColor.YELLOW + "� 1 x Blaze Rod");
        lore.add(ChatColor.YELLOW + "� 4 x Iron Ingot");
        lore.add(ChatColor.YELLOW + "� 16 x Redstone");
        lore.add(ChatColor.RESET + "");
        meta.setLore(lore);
        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        lightning.setItemMeta(meta);

        lore.clear();
        meta = rage.getItemMeta();
        meta.setDisplayName(ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "Rage Spell");
        lore.add(ChatColor.RED + "Level: 26" + ChatColor.WHITE + "" + ChatColor.BOLD + " | "  + ChatColor.RESET + ChatColor.GREEN + "Upgrade: 33");
        lore.add(ChatColor.RESET + "Click to create");
        lore.add("");
        lore.add(ChatColor.GRAY + "Creates a Ring of Rage with radius of");
        lore.add(ChatColor.YELLOW + "5 �a(+1) " + ChatColor.GRAY + "blocks that lasts up to 15 seconds.");
        lore.add(ChatColor.GRAY + "Gives Speed �eII �a(+I)�7 and Strength �eII�a (+I)");
        lore.add(ChatColor.GRAY + "to all players inside the ring while active.");
        lore.add(ChatColor.RESET + "");
        lore.add(ChatColor.GOLD + "" + ChatColor.BOLD + "Creation cost:");
        lore.add(ChatColor.YELLOW + "� 32 x Blaze Powder");
        lore.add(ChatColor.RESET + "");
        meta.setLore(lore);
        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        rage.setItemMeta(meta);
        
        lore.clear();
        meta = shadow.getItemMeta();
        meta.setDisplayName(ChatColor.DARK_GRAY + "" + ChatColor.BOLD + "Shadow Spell");
        lore.add(ChatColor.RED + "Level: 30" + ChatColor.WHITE + "" + ChatColor.BOLD + " | "  + ChatColor.RESET + ChatColor.GREEN + "Upgrade: 35");
        lore.add(ChatColor.RESET + "Click to create");
        lore.add("");
        lore.add(ChatColor.GRAY + "Shrowds all players within " + ChatColor.YELLOW + "3 �a(+2) " + ChatColor.GRAY + "blocks of");
        lore.add(ChatColor.GRAY + "the user in shadow. For the next " + ChatColor.YELLOW + "60 �a(+30) " + ChatColor.GRAY + "seconds,");
        lore.add(ChatColor.GRAY + "all mobs will completely ignore the player, even");
        lore.add(ChatColor.GRAY + "if attacked. Doesn't cancel existing aggression.");
        lore.add(ChatColor.GRAY + "Effect duration does not stack.");
        lore.add(ChatColor.RESET + "");
        lore.add(ChatColor.GOLD + "" + ChatColor.BOLD + "Creation cost:");
        lore.add(ChatColor.YELLOW + "� 3 x Potion of Invisibility (3:00)");
        lore.add(ChatColor.YELLOW + "� 16 x Glass (Normal)");
        lore.add(ChatColor.RESET + "");
        meta.setLore(lore);
        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        shadow.setItemMeta(meta);
        
        lore.clear();
        meta = poison.getItemMeta();
        meta.setDisplayName(ChatColor.RED + "" + ChatColor.BOLD + "Poison Spell");
        lore.add(ChatColor.RED + "Level: 20" + ChatColor.WHITE + "" + ChatColor.BOLD + " | "  + ChatColor.RESET + ChatColor.GREEN + "Upgrade: 26");
        lore.add(ChatColor.RESET + "Click to create");
        lore.add("");
        lore.add(ChatColor.GRAY + "Throws a Ring of Poison with radius of");
        lore.add(ChatColor.YELLOW + "4 �a(+1) " + ChatColor.GRAY + "blocks that lasts �e12 �a(+6)�7 seconds.");
        lore.add(ChatColor.GRAY + "All living things inside the ring take");
        lore.add(ChatColor.GRAY + "�e1.5 �a(+0.5)�7 damage every 0.6 seconds.");
        lore.add(ChatColor.RESET + "");
        lore.add(ChatColor.GOLD + "" + ChatColor.BOLD + "Creation cost:");
        lore.add(ChatColor.YELLOW + "� 4 x Spider Eye");
        lore.add(ChatColor.YELLOW + "� 4 x Pufferfish");
        lore.add(ChatColor.YELLOW + "� 16 x Soul Sand");
        lore.add(ChatColor.RESET + "");
        meta.setLore(lore);
        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        poison.setItemMeta(meta);
        
        lore.clear();
        meta = haste.getItemMeta();
        meta.setDisplayName(ChatColor.DARK_PURPLE + "" + ChatColor.BOLD + "Haste Spell");
        lore.add(ChatColor.RED + "Level: 20" + ChatColor.WHITE + "" + ChatColor.BOLD + " | "  + ChatColor.RESET + ChatColor.GREEN + "Upgrade: 26");
        lore.add(ChatColor.RESET + "Click to create");
        lore.add("");
        lore.add(ChatColor.GRAY + "Creates a Ring of Haste with radius of");
        lore.add(ChatColor.YELLOW + "4 �a(+1) " + ChatColor.GRAY + "blocks that lasts �e12 �a(+3)�7 seconds.");
        lore.add(ChatColor.GRAY + "Gives Speed �eIII �a(+I)�7 and Haste �eIII�a (+I)");
        lore.add(ChatColor.GRAY + "to all players inside the ring while active.");
        lore.add(ChatColor.RESET + "");
        lore.add(ChatColor.GOLD + "" + ChatColor.BOLD + "Creation cost:");
        lore.add(ChatColor.YELLOW + "� 3 x Iron Pickaxe");
        lore.add(ChatColor.YELLOW + "� 8 x Blaze Powder");
        lore.add(ChatColor.RESET + "");
        meta.setLore(lore);
        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        haste.setItemMeta(meta);
        
        
        lore.clear();
        meta = level.getItemMeta();
        meta.setDisplayName(ChatColor.GREEN + "" + ChatColor.BOLD + "Experience Level: " + ChatColor.RESET + ChatColor.YELLOW + "I");
        lore.add(ChatColor.GRAY + "This is your current XP Level. Higher-tiered spells");
        lore.add(ChatColor.GRAY + "require more knowledge to create, so they cost more");
        lore.add(ChatColor.GRAY + "XP to create in addition to the ingredients.");
        lore.add(ChatColor.RESET + "");
        meta.setLore(lore);
        level.setItemMeta(meta);
        //    10    11    12    13    14    15    16    17    18
        //    19    20    21    22    23    24    25    26    27
        spellInterface.setItem(4, level);
        spellInterface.setItem(11, lightning);
        spellInterface.setItem(12, flash);
        spellInterface.setItem(13, poison);
        spellInterface.setItem(14, haste);
        spellInterface.setItem(15, healing);
        spellInterface.setItem(20, hope);
        spellInterface.setItem(21, forge);
        spellInterface.setItem(22, rage);
        spellInterface.setItem(23, shadow);
        spellInterface.setItem(24, freeze);
    }
    
    public static void openSpellInventory(Player p) {
        Inventory inv = Bukkit.createInventory(null, 27, "Spells (" + p.getEntityId() + ")");
        inv.setContents(spellInterface.getContents());
        ItemMeta meta = inv.getItem(4).getItemMeta();
        int lvl = p.getLevel();
        meta.setDisplayName(ChatColor.GREEN + "" + ChatColor.BOLD + "Experience Level: " + ChatColor.RESET + ChatColor.YELLOW + lvl);
        inv.getItem(4).setItemMeta(meta);
        for (int i = 10; i < 24; i++) {
            if (inv.getItem(i) != null) {
                if (inv.getItem(i).getType() != Material.AIR) {
                    ItemStack item = inv.getItem(i);
                    
                    SpellObject spell = SpellObject.getSpell(item.getType());
                    int standardLevel = spell.getLevel(false), highLevel = spell.getLevel(true);
                    if (standardLevel > lvl) {
                        item.setType(Material.INK_SACK);
                        item.setDurability((short) 8);
                    } else if (lvl >= highLevel) {
                        item.addUnsafeEnchantment(Utils.glow, 1);
                    }
                }
            }
        }
    p.openInventory(inv);
    }

}
