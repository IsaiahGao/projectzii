package projectz.spells.spells;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import projectz.Main;
import projectz.spells.Spell;
import projectz.spells.SpellHandler;
import utilities.particles.ParticleEffects;

public class HasteSpell extends Spell {
    
    @Override
    public void activate(final Player user, final boolean u) {
        //Bukkit.getServer().broadcastMessage(ChatColor.GOLD + user.getName() + " just used a Rage Spell.");
        user.getWorld().playSound(user.getLocation(), Sound.ENTITY_WITHER_AMBIENT, 1.0F, 1.4F);
        for (int i = 0; i < 8; i++)
            ParticleEffects.SNOW_SHOVEL.display(0.3F, 0.4F, 0.3F, 0.1F, 1, user.getEyeLocation(), 256);
        Location center = user.getLocation();
        final Entity item = center.getWorld().dropItem(center.clone(), new ItemStack(Material.BLAZE_POWDER, 1));
        item.setVelocity(new Vector());
        item.setMetadata("unpickupable", new FixedMetadataValue(Main.getPlugin(), 1));
        doSpellPulse(item, center, u);
        new BukkitRunnable() {
            @Override
            public void run() {
                item.remove();
            }
        }.runTaskLater(Main.getPlugin(), 1L);
    }
    
    @Override
    public void doSpellPulse(final Entity item, final Location center, final boolean u) {
        new BukkitRunnable() {
            int cycle = u ? -10 : 0;
            double r = u ? 5.0 : 4.0;
            int a = u ? 3 : 2;
            @Override
            public void run() {
                createCircle(center, u);
                for (Entity e : item.getNearbyEntities(r, r, r)) {
                    if (e instanceof Player && SpellHandler.distanceSquared2D(e.getLocation(), center) <= r * r) {
                        Player p = (Player) e;
                        p.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, 30, a), true);
                        p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 30, a), true);
                    }
                }
                cycle ++;
                if (cycle == 40) {
                    this.cancel();
                }
            }
        }.runTaskTimer(Main.getPlugin(), 10L, 6L);
    }
    
    @Override
    public void createCircle(final Location center, final boolean u) {
    int pts = u ? 30 : 24;
    int radius = u ? 5 : 4;
        for (int i = 0; i < pts; i++) {
            double angle = 2 * Math.PI * i / pts;
            double x = radius * Math.cos(angle);
            double z = radius * Math.sin(angle);
            Location loc = center.clone().add(x, 0, z);
            ParticleEffects.WITCH_MAGIC.display(0, 0, 0, 0.0F, 1, loc, 256);
            ParticleEffects.WITCH_MAGIC.display(0, 0, 0, 0.0F, 1, loc.add(0, 0.33, 0), 256);
            ParticleEffects.WITCH_MAGIC.display(0, 0, 0, 0.0F, 1, loc.add(0, 0.33, 0), 256);
        }
    }

}
