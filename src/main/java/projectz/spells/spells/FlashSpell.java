package projectz.spells.spells;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import projectz.Main;
import projectz.Utilities;
import projectz.spells.Spell;
import projectz.tasks.DepletionTaskCycler.StatType;
import utilities.particles.ParticleEffects;

public class FlashSpell extends Spell {
    
    @Override
    public void activate(final Player user, final boolean u) {
        //Bukkit.getServer().broadcastMessage(ChatColor.GOLD + user.getName() + " just used a Flash Spell.");
        user.getWorld().playSound(user.getLocation(), Sound.ENTITY_BAT_TAKEOFF, 1.0F, 2.0F);
        Utilities.playChargeSound(user);
        for (int i = 0; i < 16; i++)
            ParticleEffects.SMOKE_LARGE.display((float) (Math.random()/3), (float) (Math.random()/2), (float) (Math.random()/3), 0.1F, 1, user.getEyeLocation(), 256);
        user.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, u ? 620 : 420, 50, true), true);
        final BukkitTask task = new BukkitRunnable() {
            @Override
            public void run() {
                if (!user.isOnline())
                    this.cancel();
                ParticleEffects.SMOKE_LARGE.display(0, 0, 0, 0.1F, 1, user.getLocation(), 256);
            }
        }.runTaskTimer(Main.getPlugin(), 0, 1L);
        new BukkitRunnable() {
            int d = u ? -28 : -36;
            int d2 = u ? 7 : 10;
            @Override
            public void run() {
                if (!user.isOnline()) {
                    StatType.HYDRATION.change(user, d);
                    user.setFoodLevel(user.getFoodLevel() / d2);
                } else {
                    task.cancel();
                    StatType.HYDRATION.change(user, d);
                    user.setFoodLevel(user.getFoodLevel() / d2);
                    user.removePotionEffect(PotionEffectType.SPEED);
                    user.sendMessage(ChatColor.GRAY + "Your Flash Spell has expired, leaving you exhausted.");
                    user.getWorld().playSound(user.getLocation(), Sound.ENTITY_ZOMBIE_BREAK_DOOR_WOOD, 1.0F, 0.7F);
                }
            }
        }.runTaskLater(Main.getPlugin(), u ? 600L : 400L);
    }
    
    @Override
    public void doSpellPulse(Entity item, Location center, final boolean u) {
        
    }

    @Override
    public void createCircle(Location center, final boolean u) {
        
    }
}
