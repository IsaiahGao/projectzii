package projectz.spells.spells;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import projectz.Main;
import projectz.Utilities;
import projectz.spells.Spell;
import projectz.spells.SpellHandler;
import utilities.particles.ParticleEffects;

public class HealingSpell extends Spell {
    
    @Override
    public void activate(final Player user, final boolean u) {
        //Bukkit.getServer().broadcastMessage(ChatColor.GOLD + user.getName() + " just used a Healing Spell.");
        user.getWorld().playSound(user.getLocation(), Sound.BLOCK_PORTAL_TRAVEL, 0.2F, 2.0F);
        for (int i = 0; i < 8; i++)
            ParticleEffects.HEART.display((float) (Math.random()/3), (float) (Math.random()/2), (float) (Math.random()/3), 1.0F, 1, user.getEyeLocation(), 256);
        Location center = user.getLocation();
        final Entity item = center.getWorld().dropItem(center.clone(), new ItemStack(Material.NETHER_STAR, 1));
        item.setVelocity(new Vector());
        item.setMetadata("unpickupable", new FixedMetadataValue(Main.getPlugin(), 1));
        doSpellPulse(item, center, u);
        new BukkitRunnable() {
            @Override
            public void run() {
                item.remove();
            }
        }.runTaskLater(Main.getPlugin(), 1L);
    }
    
    @Override
    public void doSpellPulse(final Entity item, final Location center, final boolean u) {
        new BukkitRunnable() {
            int cycle = u ? -10 : 0;
            int r = u ? 6 : 5;
            @Override
            public void run() {
                createCircle(center, u);
                for (Entity e : item.getNearbyEntities(r, r, r)) {
                    if (e instanceof Player && SpellHandler.distanceSquared2D(e.getLocation(), center) <= r * r) {
                        Player p = (Player) e;
                        Utilities.Heal(p, 2);
                    }
                }
                cycle ++;
                if (cycle == 40) {
                    this.cancel();
                }
            }
        }.runTaskTimer(Main.getPlugin(), 10L, 6L);
    }
    
    @Override
    public void createCircle(final Location center, final boolean u) {
    int pts = u ? 30 : 24;
    int radius = u ? 6 : 5;
        for (int i = 0; i < pts; i++) {
            double angle = 2 * Math.PI * i / pts;
            double x = radius * Math.cos(angle);
            double z = radius * Math.sin(angle);
            Location loc = center.clone().add(x, 0, z);
            ParticleEffects.HEART.display(0, 0, 0, 1.0F, 1, loc, 256);
            ParticleEffects.HEART.display(0, 0, 0, 1.0F, 1, loc.add(0, 0.33, 0), 256);
            ParticleEffects.HEART.display(0, 0, 0, 1.0F, 1, loc.add(0, 0.33, 0), 256);
        }
    }

}
