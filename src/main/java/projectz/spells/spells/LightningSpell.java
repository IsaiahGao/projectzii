package projectz.spells.spells;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import projectz.Main;
import projectz.spells.Spell;
import utilities.particles.ParticleEffects;

public class LightningSpell extends Spell {
    
    public void activate(final Player user, final boolean u) {
        //Bukkit.getServer().broadcastMessage(ChatColor.GOLD + user.getName() + " just used a Lightning Spell.");
        Location center = user.getLocation();
        Entity item = center.getWorld().dropItemNaturally(center.clone().add(0,1,0), new ItemStack(Material.DIAMOND_BLOCK, 1));
        item.setVelocity(user.getLocation().getDirection().add(new Vector(0,0.4,0)).multiply(0.75));
        item.setMetadata("unpickupable", new FixedMetadataValue(Main.getPlugin(), 1));
        doSpellPulse(item, center, u);
    }
    
    public void doSpellPulse(final Entity item, final Location c, final boolean u) {
        new BukkitRunnable() {
            int cycle = u ? -2 : 0;
            public void run() {
                Location center = item.getLocation();
                item.remove();
                int x = Main.random(5);
                int z = Main.random(5);
                if (Math.random() < 0.5) x = -x;
                if (Math.random() < 0.5) z = -z;
                int xcoord = (int) (center.getX() + x);
                int zcoord = (int) (center.getZ() + z);
                center.getWorld().strikeLightning(new Location(center.getWorld(), xcoord, center.getWorld().getHighestBlockYAt(xcoord, zcoord), zcoord));
                cycle ++;
                if (cycle == 7)
                    this.cancel();
            }
        }.runTaskTimer(Main.getPlugin(), 100L, 20L);
        new BukkitRunnable() {
            public void run() {
                createCircle(item.getLocation(), u);
            }
        }.runTaskLater(Main.getPlugin(), 60L);
    }
    
    public void createCircle(final Location center, final boolean u) {
        new BukkitRunnable() {
        int radius = 8;
        int pts = 32 - radius;
        int cycle = 0;
            public void run() {
                for (int i = 0; i < pts; i++) {
                    double angle = 2 * Math.PI * i / pts;
                    double x = radius * Math.cos(angle);
                    double z = radius * Math.sin(angle);
                    Location loc = center.clone().add(x, 0, z);
                    ParticleEffects.FIREWORKS_SPARK.display(0, 0, 0, 0.0F, 1, loc, 256);
                }
                radius --;
                cycle ++;
                if (cycle == 7)
                    this.cancel();
            }
        }.runTaskTimer(Main.getPlugin(), 0L, 3L);
    }

}
