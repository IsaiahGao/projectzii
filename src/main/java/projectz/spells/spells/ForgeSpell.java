package projectz.spells.spells;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import projectz.Main;
import projectz.Utilities;
import projectz.spells.Spell;
import projectz.spells.SpellHandler;
import utilities.particles.ParticleEffects;

public class ForgeSpell extends Spell {
    
    @Override
    public void activate(final Player user, final boolean u) {
        //Bukkit.getServer().broadcastMessage(ChatColor.GOLD + user.getName() + " just used a Forge Spell.");
        user.getWorld().playSound(user.getLocation(), Sound.ENTITY_ZOMBIE_VILLAGER_CURE, 0.6F, 2.0F);
        for (int i = 0; i < 8; i++)
            ParticleEffects.FLAME.display(0.3F, 0.3F, 0.3F, 0.1F, 1, user.getEyeLocation(), 256);
        Location center = user.getLocation();
        final Entity item = center.getWorld().dropItem(center.clone(), new ItemStack(Material.COAL, 1));
        item.setVelocity(new Vector());
        item.setMetadata("unpickupable", new FixedMetadataValue(Main.getPlugin(), 1));
        doSpellPulse(item, center, u);
        new BukkitRunnable() {
            @Override
            public void run() {
                item.remove();
            }
        }.runTaskLater(Main.getPlugin(), 1L);
    }
    
    @Override
    public void doSpellPulse(final Entity item, final Location center, final boolean u) {
        new BukkitRunnable() {
            int cycle = u ? -22 : 0;
            int r = u ? 4 : 3;
            
            @Override
            public void run() {
                createCircle(center, u);
                for (Entity e : item.getNearbyEntities(r, r + 1, r)) {
                    if (e instanceof Player && SpellHandler.distanceSquared2D(e.getLocation(), center) <= r * r) {
                        Player p = (Player) e;
                        Inventory inv = p.getInventory();
                        for (ItemStack i : inv.getContents()) {
                            if (i != null) {
                                switch(i.getType()) {
                                    case LOG:
                                    case LOG_2:
                                        inv.removeItem(Utilities.setItemAmount(i, 1));
                                        inv.addItem(new ItemStack(Material.COAL, 1, (short) 1));
                                        break;
                                    case COAL_ORE:
                                        inv.removeItem(Utilities.setItemAmount(i, 1));
                                        inv.addItem(new ItemStack(Material.COAL, 1));
                                        break;
                                    case IRON_ORE:
                                        inv.removeItem(Utilities.setItemAmount(i, 1));
                                        inv.addItem(new ItemStack(Material.IRON_INGOT, 1));
                                        break;
                                    case LAPIS_ORE:
                                        inv.removeItem(Utilities.setItemAmount(i, 1));
                                        ItemStack lapis = new ItemStack(Material.INK_SACK, 1, (short) 4);
                                        inv.addItem(lapis);
                                        break;
                                    case EMERALD_ORE:
                                        inv.removeItem(Utilities.setItemAmount(i, 1));
                                        inv.addItem(new ItemStack(Material.EMERALD, 1));
                                        break;
                                    case REDSTONE_ORE:
                                        inv.removeItem(Utilities.setItemAmount(i, 1));
                                        inv.addItem(new ItemStack(Material.REDSTONE, 1));
                                        break;
                                    case DIAMOND_ORE:
                                        inv.removeItem(Utilities.setItemAmount(i, 1));
                                        inv.addItem(new ItemStack(Material.DIAMOND, 1));
                                        break;
                                    case GOLD_ORE:
                                        inv.removeItem(Utilities.setItemAmount(i, 1));
                                        inv.addItem(new ItemStack(Material.GOLD_INGOT, 1));
                                        break;
                                    case COBBLESTONE:
                                        inv.removeItem(Utilities.setItemAmount(i, 1));
                                        inv.addItem(new ItemStack(Material.STONE, 1));
                                        break;
                                    case PORK:
                                        inv.removeItem(Utilities.setItemAmount(i, 1));
                                        inv.addItem(new ItemStack(Material.GRILLED_PORK, 1));
                                        break;
                                    case RAW_BEEF:
                                        inv.removeItem(Utilities.setItemAmount(i, 1));
                                        inv.addItem(new ItemStack(Material.COOKED_BEEF, 1));
                                        break;
                                    case RAW_CHICKEN:
                                        inv.removeItem(Utilities.setItemAmount(i, 1));
                                        inv.addItem(new ItemStack(Material.COOKED_CHICKEN, 1));
                                        break;
                                    case MUTTON:
                                        inv.removeItem(Utilities.setItemAmount(i, 1));
                                        inv.addItem(new ItemStack(Material.COOKED_MUTTON, 1));
                                        break;
                                    case RAW_FISH:
                                        inv.removeItem(Utilities.setItemAmount(i, 1));
                                        inv.addItem(new ItemStack(Material.COOKED_FISH, 1, i.getDurability()));
                                        break;
                                    default:
                                        break;
                                }
                            p.updateInventory();
                            }
                        }
                    }
                }
                cycle ++;
                if (cycle == 64) {
                    this.cancel();
                }
            }
        }.runTaskTimer(Main.getPlugin(), 10L, 6L);
    }
    
    @Override
    public void createCircle(final Location center, final boolean u) {
    int pts = u ? 30 : 24;
    int radius = u ? 4 : 3;
        for (int i = 0; i < pts; i++) {
            double angle = 2 * Math.PI * i / pts;
            double x = radius * Math.cos(angle);
            double z = radius * Math.sin(angle);
            Location loc = center.clone().add(x, 0, z);
            ParticleEffects.FLAME.display(0, 0, 0, 0.01F, 1, loc, 256);
            ParticleEffects.FLAME.display(0, 0, 0, 0.01F, 1, loc.add(0, 0.33, 0), 256);
            ParticleEffects.FLAME.display(0, 0, 0, 0.01F, 1, loc.add(0, 0.33, 0), 256);
        }
    }

}
