package projectz.spells.spells;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import projectz.Main;
import projectz.spells.Spell;
import projectz.spells.SpellHandler;
import utilities.particles.ParticleEffects;

public class FreezeSpell extends Spell {
    
    public void activate(final Player user, final boolean u) {
        //Bukkit.getServer().broadcastMessage(ChatColor.GOLD + user.getName() + " just used a Freeze Spell.");
        for (int i = 0; i < 6; i++)
            ParticleEffects.EXPLOSION_LARGE.display((float) (Math.random()/3), (float) (Math.random()/2), (float) (Math.random()/3), 1.0F, 1, user.getEyeLocation(), 256);
        Location center = user.getLocation();
        Entity item = center.getWorld().dropItem(center.clone(), new ItemStack(Material.ICE, 1));
        item.setVelocity(new Vector());
        item.setMetadata("unpickupable", new FixedMetadataValue(Main.getPlugin(), 1));
        item.setMetadata("caster", new FixedMetadataValue(Main.getPlugin(), user.getName()));
        for (int i = 0; i < 3; i++)
            user.getWorld().playEffect(center, Effect.STEP_SOUND, Material.ICE);
        doSpellPulse(item, center, u);
    }
    
    @SuppressWarnings("deprecation")
    public void doSpellPulse(final Entity item, final Location center, final boolean u) {
        createCircle(center, u);
        int r = u ? 6 : 5;
        final Map<Block, Material> blocks = new HashMap<Block, Material>();
        Player caster = Bukkit.getPlayer(item.getMetadata("caster").get(0).asString());
        for (Entity e : item.getNearbyEntities(r, r+2, r)) {
            if ((e instanceof LivingEntity) && !e.equals(caster) && SpellHandler.distanceSquared2D(e.getLocation(), center) <= r * r) {
                ((LivingEntity) e).addPotionEffect(new PotionEffect(PotionEffectType.SLOW, u ? 300 : 200, 128));
                ((LivingEntity) e).addPotionEffect(new PotionEffect(PotionEffectType.JUMP, u ? 300 : 200, 128));
            }
        }
        for (double x = center.getX() - r; x < center.getX() + r; x += 1) {
            for (double z = center.getZ() - r; z < center.getZ() + r; z += 1) {
                for (double y = center.getY() - r; y < center.getY() + r; y += 1) {
                    Location newloc = new Location(center.getWorld(), x, y, z);
                    if (center.distanceSquared(newloc.add(0.5,0.5,0.5)) <= r * r) {
                        Block b = newloc.getBlock();
                            switch (b.getType()) {
                            case AIR:
                                Material mat = b.getRelative(BlockFace.DOWN).getType();
                                if (!mat.equals(Material.AIR) &&
                                        !mat.equals(Material.OBSIDIAN) &&
                                        !mat.equals(Material.STATIONARY_LAVA) &&
                                        !mat.equals(Material.SNOW) &&
                                        !mat.equals(Material.LONG_GRASS)) {
                                    blocks.put(b, Material.AIR);
                                    b.setType(Material.SNOW);
                                }
                                break;
                            case LONG_GRASS:
                                if (Main.random(2) == 0)
                                    b.setType(Material.LONG_GRASS);
                                else {
                                    blocks.put(b, Material.AIR);
                                    b.setType(Material.SNOW);
                                }
                                break;
                            case STATIONARY_WATER:
                            case WATER:
                                if (b.getData() == 0) {
                                    b.setType(Material.ICE);
                                    blocks.put(b, Material.WATER);
                                } else {
                                    b.setType(Material.SNOW);
                                    blocks.put(b, Material.AIR);
                                }
                                break;
                            case STATIONARY_LAVA:
                            case LAVA:
                                if (b.getData() == 0) {
                                    b.setType(Material.OBSIDIAN);
                                    blocks.put(b, Material.LAVA);
                                } else {
                                    b.setType(Material.SNOW);
                                    blocks.put(b, Material.AIR);
                                }
                                break;
                            case FIRE:
                                b.setType(Material.SNOW);
                                blocks.put(b, Material.AIR);
                                break;
                            default:
                                break;
                            }
                    }
                }
            }
        }
        item.remove();
        new BukkitRunnable() {
            @SuppressWarnings("incomplete-switch")
            public void run() {
                for (Block bl : blocks.keySet()) {
                    switch (bl.getType()) {
                        case SNOW:
                        case ICE:
                        case OBSIDIAN:
                            bl.setType(blocks.get(bl));
                    }
                }
            }
        }.runTaskLater(Main.getPlugin(), u ? 300L : 200L);
    }
    
    public void createCircle(final Location center, final boolean u) {
    int pts = u ? 30 : 24;
    int radius = u ? 6 : 5;
        for (int i = 0; i < pts; i++) {
            double angle = 2 * Math.PI * i / pts;
            double x = radius * Math.cos(angle);
            double z = radius * Math.sin(angle);
            Location loc = center.clone().add(x, 0, z);
            ParticleEffects.SNOW_SHOVEL.display(0, 0, 0, 0.0F, 4, loc, 256);
            ParticleEffects.SNOW_SHOVEL.display(0, 0, 0, 0.0F, 4, loc.add(0, 0.33, 0), 256);
            ParticleEffects.SNOW_SHOVEL.display(0, 0, 0, 0.0F, 4, loc.add(0, 0.33, 0), 256);
        }
    }

}
