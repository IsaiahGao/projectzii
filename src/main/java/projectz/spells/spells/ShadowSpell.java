package projectz.spells.spells;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.util.Vector;

import projectz.Main;
import projectz.spells.Spell;
import projectz.spells.SpellHandler;
import utilities.particles.ParticleEffects;

public class ShadowSpell extends Spell {
    
    @Override
    public void activate(final Player user, final boolean u) {
        //Bukkit.getServer().broadcastMessage(ChatColor.GOLD + user.getName() + " just used a Shadow Spell.");
        user.getWorld().playSound(user.getLocation(), Sound.BLOCK_FIRE_EXTINGUISH, 0.2F, 0.5F);
        for (int i = 0; i < 6; i++)
            ParticleEffects.EXPLOSION_LARGE.display((float) (Math.random()/3), (float) (Math.random()/2), (float) (Math.random()/3), 1.0F, 1, user.getEyeLocation(), 256);
        Location center = user.getLocation();
        Entity item = center.getWorld().dropItem(center.clone(), new ItemStack(Material.INK_SACK, 1));
        item.setVelocity(new Vector());
        item.setMetadata("unpickupable", new FixedMetadataValue(Main.getPlugin(), 1));
        doSpellPulse(item, center, u);
    }
    
    @Override
    public void doSpellPulse(final Entity item, final Location center, final boolean u) {
        createCircle(center, u);
        double r = u ? 5.0 : 3.0;
        for (Entity e : item.getNearbyEntities(r, r + 2.0, r)) {
            if (e instanceof Player && SpellHandler.distanceSquared2D(e.getLocation(), center) <= r * r) {
            final Player p = (Player) e;
            if (p.hasMetadata("targetmeta"))
                Bukkit.getServer().getScheduler().cancelTask(p.getMetadata("targetmeta").get(0).asInt());
                p.setMetadata("untargetable", new FixedMetadataValue(Main.getPlugin(), 1));
                BukkitTask task = new BukkitRunnable() {
                    @Override
                    public void run() {
                        if (p != null) {
                            p.removeMetadata("untargetable", Main.getPlugin());
                            p.removeMetadata("targetmeta", Main.getPlugin());
                            p.sendMessage(ChatColor.GRAY + "Your shadow spell has expired!");
                        }
                    }
                }.runTaskLater(Main.getPlugin(), u ? 1800L : 1200L);
                p.setMetadata("targetmeta", new FixedMetadataValue(Main.getPlugin(), task.getTaskId()));
            }
        }
        item.remove();
    }
    
    @Override
    public void createCircle(final Location center, final boolean u) {
    int pts = u ? 36 : 24;
    int radius = u ? 5 : 3;
        for (int i = 0; i < pts; i++) {
            double angle = 2 * Math.PI * i / pts;
            double x = radius * Math.cos(angle);
            double z = radius * Math.sin(angle);
            Location loc = center.clone().add(x, 0, z);
            ParticleEffects.SPELL.display(0, 0, 0, 0.0F, 4, loc, 256);
            ParticleEffects.SPELL.display(0, 0, 0, 0.0F, 4, loc.add(0, 0.33, 0), 256);
            ParticleEffects.SPELL.display(0, 0, 0, 0.0F, 4, loc.add(0, 0.33, 0), 256);
        }
    }

}
