package projectz.spells.spells;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import projectz.Main;
import projectz.spells.Spell;
import projectz.spells.SpellHandler;
import utilities.particles.ParticleEffects;
import utilities.particles.ParticleEffects.OrdinaryColor;

public class PoisonSpell extends Spell {
    
    private static OrdinaryColor color = new OrdinaryColor(255, 106, 1);
    
    @Override
    public void activate(final Player user, final boolean u) {
        //Bukkit.getServer().broadcastMessage(ChatColor.GOLD + user.getName() + " just used a Lightning Spell.");
        Location center = user.getLocation();
        Entity item = center.getWorld().dropItemNaturally(center.clone().add(0,1,0), new ItemStack(Material.STAINED_CLAY, 1));
        item.setVelocity(user.getLocation().getDirection().add(new Vector(0,0.4,0)).multiply(0.6));
        item.setMetadata("unpickupable", new FixedMetadataValue(Main.getPlugin(), 1));
        user.getWorld().playSound(user.getLocation(), Sound.ENTITY_WITHER_SHOOT, 0.4F, 1.4F);
        doSpellPulse(item, center, u);
    }
    
    @Override
    public void doSpellPulse(final Entity item, final Location thrown, final boolean u) {
        new BukkitRunnable() {
            int cycle = u ? -20 : 0;
            int r = u ? 5 : 4;
            boolean b = false;
            @Override
            public void run() {
                Location c = item.getLocation();
                
                if (!b) {
                    c.getWorld().playSound(c, Sound.ENTITY_FIREWORK_LARGE_BLAST, 1.0F, 0.8F);
                    item.remove();
                    b = true;
                }
                
                createCircle(c, u);
                if (cycle % 2 == 0)
                    for (Entity e : item.getNearbyEntities(r, r, r)) {
                        if (e instanceof LivingEntity && SpellHandler.distanceSquared2D(e.getLocation(), c) <= r * r) {
                            LivingEntity le = (LivingEntity) e;
                            le.damage(u ? 2.0 : 1.5);
                            le.setNoDamageTicks(0);
                        }
                    }
                cycle ++;
                if (cycle == 40) {
                    this.cancel();
                }
            }
        }.runTaskTimer(Main.getPlugin(), 60L, 6L);
    }
    
    @Override
    public void createCircle(final Location center, final boolean u) {
        int pts = u ? 28 : 22;
        int radius = u ? 5 : 4;
            for (int i = 0; i < pts; i++) {
                double angle = 2 * Math.PI * i / pts;
                double x = radius * Math.cos(angle);
                double z = radius * Math.sin(angle);
                Location loc = center.clone().add(x, 0, z);
                ParticleEffects.MOB_SPELL.display(color, loc, 256);
                ParticleEffects.MOB_SPELL.display(color, loc.add(0, 0.33, 0), 256);
                ParticleEffects.MOB_SPELL.display(color, loc.add(0, 0.33, 0), 256);
            }
    }

}
