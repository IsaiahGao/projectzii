package projectz.spells;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkEffectMeta;

import projectz.ItemDef;
import projectz.spells.spells.*;

public class SpellCreationHandler {
    
    public enum SpellObject {
        LIGHTNING(16, 24, Material.INK_SACK, ItemDef.lightningSpell, new ItemStack[] {
                new ItemStack(Material.BLAZE_ROD, 1),
                new ItemStack(Material.IRON_INGOT, 4),
                new ItemStack(Material.REDSTONE, 16)
                }, 150, Color.BLUE, Color.NAVY, new LightningSpell()),
        FLASH(18, 28, Material.LEATHER_BOOTS, ItemDef.flashSpell, new ItemStack[] {
                new ItemStack(Material.ENDER_PEARL, 3),
                new ItemStack(Material.FEATHER, 16)
                }, 320, Color.WHITE, Color.YELLOW, new FlashSpell()),
        HEALING(24, 32, Material.SPECKLED_MELON, ItemDef.healingSpell, new ItemStack[] {
                new ItemStack(Material.GOLDEN_APPLE, 1),
                new ItemStack(Material.SPECKLED_MELON, 9)
                }, 880, Color.ORANGE, Color.YELLOW, new HealingSpell()),
        HOPE(24, 32, Material.NETHER_STAR, ItemDef.hopeSpell, new ItemStack[] {
                new ItemStack(Material.POTION, 3),
                new ItemStack(Material.APPLE, 8),
                new ItemStack(Material.GLOWSTONE_DUST, 32)
                }, 880, Color.AQUA, Color.WHITE, new HopeSpell()),
        FORGE(28, 34, Material.COAL, ItemDef.forgeSpell, new ItemStack[] {
                new ItemStack(Material.LAVA_BUCKET, 1),
                new ItemStack(Material.COAL, 16),
                new ItemStack(Material.BLAZE_POWDER, 8)
                }, 1680, Color.ORANGE, Color.MAROON, new ForgeSpell()),
        RAGE(26, 33, Material.BLAZE_POWDER, ItemDef.rageSpell, new ItemStack[] {
                new ItemStack(Material.BLAZE_POWDER, 32)
                }, 1680, Color.PURPLE, Color.FUCHSIA, new RageSpell()),
        SHADOW(30, 36, Material.EYE_OF_ENDER, ItemDef.sneakingSpell, new ItemStack[] {
                new ItemStack(Material.POTION, 3, (short) 8206),
                new ItemStack(Material.GLASS, 16)
                }, 3320, Color.BLACK, Color.SILVER, new ShadowSpell()),
        FREEZE(30, 36, Material.ICE, ItemDef.freezeSpell, new ItemStack[] {
                new ItemStack(Material.WATER_BUCKET, 4),
                new ItemStack(Material.ICE, 16),
                new ItemStack(Material.SNOW_BLOCK, 16)
                }, 6280, Color.fromBGR(255, 239, 191), Color.fromBGR(255, 239, 191), new FreezeSpell()),
        POISON(20, 26, Material.SKULL_ITEM, ItemDef.poisonSpell, new ItemStack[] {
                new ItemStack(Material.SPIDER_EYE, 4),
                new ItemStack(Material.SOUL_SAND, 16),
                new ItemStack(Material.RAW_FISH, 4, (short) 2)
                }, 6280, Color.RED, Color.ORANGE, new PoisonSpell()),
        HASTE(20, 26, Material.IRON_PICKAXE, ItemDef.hasteSpell, new ItemStack[] {
                new ItemStack(Material.IRON_PICKAXE, 3),
                new ItemStack(Material.BLAZE_POWDER, 8)
                }, 6280, Color.PURPLE, Color.WHITE, new HasteSpell());
        
        private SpellObject(int tier, int upgrade, Material itemType, ItemStack spellItem, ItemStack[] ingredients, int exp, Color color, Color fade, Spell spell) {
            this.tier = tier;
            this.upgrade = upgrade;
            this.icon = itemType;
            
            FireworkEffectMeta meta = (FireworkEffectMeta) spellItem.getItemMeta();
            meta.setEffect(FireworkEffect.builder().withColor(color).withFade(fade).build());
            meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
            meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
            spellItem.setItemMeta(meta);
            
            this.spell = spellItem;
            this.ingredients = ingredients;
            this.s = spell;
        }
        
        private final int tier, upgrade;
        private Material icon;
        private ItemStack spell;
        private ItemStack[] ingredients;
        private Spell s;
        
        private static Map<Material, SpellObject> matspell = new HashMap<Material, SpellObject>();
        private static Map<String, SpellObject> bylore = new HashMap<String, SpellObject>();
        static {
            for (SpellObject s : SpellObject.values()) {
                matspell.put(s.getIcon(), s);
                bylore.put(s.spell.getItemMeta().getLore().get(0), s);
            }
        }
        
        private Material getIcon() {
            return icon;
        }
        
        public ItemStack getItem() {
            return getItem(false);
        }
        
        public ItemStack getItem(boolean upg) {
            ItemStack stack = spell.clone();
            if (upg)
                stack.addUnsafeEnchantment(Enchantment.ARROW_DAMAGE, 1);
            return stack;
        }
        
        public Spell getSpell() {
            return s;
        }
        
        public int getLevel(boolean upgraded) {
            return upgraded ? upgrade : tier;
        }
        
        public static SpellObject getSpell(Material m) {
            return matspell.get(m);
        }
        
        public static SpellObject getSpell(ItemStack item) {
            return item.getItemMeta().getLore() == null ? null : bylore.get(item.getItemMeta().getLore().get(0));
        }
        
        public void createSpell(Player p) {
            if (hasAllItems(p.getInventory())) {
                int lvl = p.getLevel();
                if (lvl >= tier) {
                    for (ItemStack i : ingredients) {
                        p.getInventory().removeItem(i);
                        if (i.getType().toString().endsWith("_BUCKET"))
                            p.getInventory().addItem(new ItemStack(Material.BUCKET, i.getAmount()));
                        if (i.getType() == Material.POTION)
                            p.getInventory().addItem(new ItemStack(Material.GLASS_BOTTLE, i.getAmount()));
                    }
                        ItemStack s = spell.clone();
                        String str = "a spell!";
                        if (lvl >= upgrade) {
                            s.addUnsafeEnchantment(Enchantment.ARROW_DAMAGE, 1);
                            str = "an upgraded spell!";
                            p.setLevel(p.getLevel() - upgrade);
                        } else {
                            p.setLevel(p.getLevel() - tier);
                        }
                        p.getWorld().dropItemNaturally(p.getEyeLocation(), s);
                        p.playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_VILLAGER_CONVERTED, 1.0F, 2.0F);
                        p.sendMessage(ChatColor.GREEN + "You mix the catalysts together to forge " + str);
                        //Skill.KNOWLEDGE.addXP(p, xp);
                        p.closeInventory();
                } else {
                    p.sendMessage(ChatColor.RED + "You need more Experience Levels to forge this spell.");
                    p.playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 1.0F, 2.0F);
                }
            } else {
                p.sendMessage(ChatColor.RED + "You do not have enough catalysts to forge this spell.");
                p.playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 1.0F, 2.0F);
            }
        }
        
        private boolean hasAllItems(Inventory i) {
            for (ItemStack item : ingredients) {
                if (!i.containsAtLeast(item, item.getAmount()))
                    return false;
            }
        return true;
        }
    }

}
