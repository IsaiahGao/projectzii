package projectz;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.inventory.ItemStack;

public class TeleTabs {
    
    public enum TeleTab {
        SPAWN(ItemDef.spawnTab),
        HOME(ItemDef.houseTab),
        CLAN(ItemDef.clanTab);
        
        private TeleTab(ItemStack item) {
            this.item = item;
        }
        
        private ItemStack item;
        
        public ItemStack getItem() {
            return item;
        }
        
        private static Map<String, TeleTab> byLore = new HashMap<String, TeleTab>();
        static {
            for (TeleTab tab : TeleTab.values())
                byLore.put(tab.item.getItemMeta().getLore().get(0), tab);
        }
        
        public static TeleTab getTab(ItemStack item) {
            return item.getItemMeta().getLore() == null ? null : byLore.get(item.getItemMeta().getLore().get(0));
        }
    }

}
