package projectz.handlers;

public class Vitals {
    
    public Vitals(String data) {
        if (data == null) {
            this.data[0] = 75F;
            this.data[1] = 0F;
            this.data[2] = 75F;
            this.data[3] = 75F;
        } else {
            String[] arr = data.split(":");
            this.data[0] = Float.parseFloat(arr[0]);
            this.data[1] = Float.parseFloat(arr[1]);
            this.data[2] = Float.parseFloat(arr[2]);
            this.data[3] = Float.parseFloat(arr[3]);
        }
    }

    // hydration, hydrationSat, sanity, warmth
    public float[] data = new float[4];
    
    @Override
    public String toString() {
        return data[0] + ":" + data[1] + ":" + data[2] + ":" + data[3];
    }

}
