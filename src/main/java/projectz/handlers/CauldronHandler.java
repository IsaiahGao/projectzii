package projectz.handlers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;

import utilities.Utils;

public class CauldronHandler {
    
    private static Set<Location> flags = new HashSet<>();
    
    public static boolean isContaminated(Block b) {
        return flags.contains(b.getLocation());
    }
    
    public static void contaminate(Block b) {
        flags.add(b.getLocation());
    }
    
    public static void decontaminate(Block b) {
        flags.remove(b.getLocation());
    }
    
    public static void load(FileConfiguration config) {
        for (String s : config.getStringList("contaminated")) {
            flags.add(Utils.stringToLoc(s));
        }
    }
    
    public static void save(FileConfiguration config) {
        List<String> list = new ArrayList<>(flags.size());
        for (Location loc : flags) {
            list.add(Utils.locToString(loc));
        }
        config.set("contaminated", list);
        try {
            config.save("cauldrons.yml");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
