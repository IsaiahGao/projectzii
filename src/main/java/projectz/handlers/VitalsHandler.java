package projectz.handlers;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import projectz.Main;

public class VitalsHandler {
    
    public static Map<UUID, Vitals> vitals = new HashMap<>();
    
    public static void save(Player p) {
        Main.stats.set("stats." + p.getUniqueId().toString(), vitals.get(p.getUniqueId()).toString());
        try {
            Main.stats.save("plugins" + File.separator + "ProjectZ" + File.separator + "stats.yml");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static void save() {
        ConfigurationSection sec = Main.stats.getConfigurationSection("stats");
        for (Map.Entry<UUID, Vitals> entry : vitals.entrySet()) {
            sec.set(entry.getKey().toString(), entry.getValue().toString());
        }
        try {
            Main.stats.save("plugins" + File.separator + "ProjectZ" + File.separator + "stats.yml");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static Vitals getVitals(Player p) {
        return vitals.get(p.getUniqueId());
    }
    
    public static void loadVitals(Player p) {
        UUID uuid = p.getUniqueId();
        String s = Main.stats.getString("stats." + uuid.toString());
        vitals.put(uuid, new Vitals(s));
    }
    
}
