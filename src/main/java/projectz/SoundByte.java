package projectz;

import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class SoundByte {
    
    public SoundByte(Sound sound, float volume, float pitch) {
        s = sound;
        v = volume;
        p = pitch;
    }
    
    private Sound s;
    private float v, p;
    
    public void play(Location loc) {
        loc.getWorld().playSound(loc, s, v, p);
    }
    
    public void play(Player pl) {
        pl.playSound(pl.getLocation(), s, v, p);
    }

}
