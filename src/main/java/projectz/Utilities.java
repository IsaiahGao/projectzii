package projectz;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Builder;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Biome;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.block.DoubleChest;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import projectz.spells.SpellCreationHandler.SpellObject;
import utilities.Utils;
import utilities.particles.ParticleEffects;
import utilities.particles.ParticleEffects.OrdinaryColor;

public class Utilities {
    
    public static DecimalFormat df = new DecimalFormat("0.0");
    
    public static Set<EntityType> protectedMobs = new HashSet<EntityType>();
    private static Map<UUID, Integer> BleedingOfflinePlayers = new HashMap<UUID, Integer>();
    private static final int BED_SEARCH_RADIUS = 2;
    
    public static PotionEffectType[] spiderEffects = {
        PotionEffectType.SPEED, PotionEffectType.INCREASE_DAMAGE,
        PotionEffectType.INVISIBILITY, PotionEffectType.REGENERATION,
        PotionEffectType.FIRE_RESISTANCE
    };
    
    public static Sound[] randomSounds = {
        Sound.ENTITY_WOLF_HOWL, Sound.ENTITY_BAT_DEATH, Sound.BLOCK_WOODEN_DOOR_CLOSE, Sound.BLOCK_WOODEN_DOOR_OPEN, Sound.AMBIENT_CAVE, Sound.BLOCK_PORTAL_TRAVEL,
        Sound.ENTITY_GHAST_AMBIENT, Sound.ENTITY_GHAST_SCREAM, Sound.ENTITY_GHAST_WARN, Sound.AMBIENT_CAVE, Sound.ENTITY_WITHER_AMBIENT, Sound.ENTITY_WOLF_AMBIENT,
        Sound.ENTITY_ZOMBIE_VILLAGER_CURE, Sound.ENTITY_ZOMBIE_VILLAGER_CONVERTED, Sound.BLOCK_CHEST_OPEN, Sound.ENTITY_CAT_AMBIENT
    };
    
    private static EntityType[] protectedEntities = {
        EntityType.SHEEP,
        EntityType.COW,
        EntityType.CHICKEN,
        EntityType.RABBIT,
        EntityType.PIG,
        EntityType.ARMOR_STAND,
        EntityType.ITEM_FRAME,
        EntityType.SQUID,
        EntityType.WOLF,
        EntityType.OCELOT,
        EntityType.HORSE
    };
    
    static {
        for (EntityType e : protectedEntities) {
            protectedMobs.add(e);
        }
    }
    
    public enum CoolDownReason {
        DRINK,
        DART,
        BANDAGE,
        COMMAND
    }
    
    public static boolean cooledDown(Player p, CoolDownReason reason, double d) {
        Long time;
        String cooldown = reason.toString().toLowerCase();
        if (!p.hasMetadata(cooldown)) time = null;
        else time = p.getMetadata(cooldown).get(0).asLong();
                if (time == null || System.currentTimeMillis() - time >= (d*1000) || Main.hasPermission(p, "zday.cooldownbypass")) {
                    p.setMetadata(cooldown, new FixedMetadataValue(Main.getPlugin(), System.currentTimeMillis()));
                    return true;
                }
        return false;
    }
    
    public static boolean notifyCooledDown(Player p, CoolDownReason reason, double d) {
        Long time;
        String cooldown = reason.toString().toLowerCase();
        //d *= 1 - (getCooldownReduction(p.getItemInHand()) * 0.01);
        time = p.hasMetadata(cooldown) ? p.getMetadata(cooldown).get(0).asLong() : null;
                if (time == null || System.currentTimeMillis() - time >= (d*1000) || Main.hasPermission(p, "zday.cooldownbypass")) {
                    p.setMetadata(cooldown, new FixedMetadataValue(Main.getPlugin(), System.currentTimeMillis()));
                    return true;
                }
        p.sendMessage(ChatColor.RED + "Ability recharging! " + ChatColor.GOLD + "" + ChatColor.BOLD + "> " + ChatColor.RESET + "" + ChatColor.WHITE + df.format(((time + d*1000) - System.currentTimeMillis())/1000) + ChatColor.GRAY + " more seconds!");
        p.playSound(p.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0F, 1.0F);
        return false;
    }
    
    public static boolean isDesert(Block b) {
        return b.getTemperature() >= 1.0 || b.getBiome().equals(Biome.HELL);
    }
    
    public static boolean isCold(Block b) {
        switch (b.getBiome()) {
        case EXTREME_HILLS:
        case EXTREME_HILLS_WITH_TREES:
        case SMALLER_EXTREME_HILLS:
        case MUTATED_EXTREME_HILLS:
        case MUTATED_EXTREME_HILLS_WITH_TREES:
            return b.getY() > 100;
        default:
            return b.getTemperature() <= 0.1;
        }
    }
    
    public static void applyCustomEnchantment(ItemStack item, Enchantment ench, int level) {
        item.addUnsafeEnchantment(ench, level);
        ItemMeta meta = item.getItemMeta();
        List<String> lore = (meta.getLore() == null ? new ArrayList<String>() : meta.getLore());
        lore.add(ChatColor.GRAY + ench.getName() + " " + romanNumeral(level));
        meta.setLore(lore);
        item.setItemMeta(meta);
    }
    
    public static String romanNumeral(int i) {
        switch (i) {
        case 0:
            return "O";
        case 1:
            return "I";
        case 2:
            return "II";
        case 3:
            return "III";
        case 4:
            return "IV";
        case 5:
            return "V";
        case 6:
            return "VI";
        case 7:
            return "VII";
        case 8:
            return "VIII";
        case 9:
            return "IX";
        case 10:
            return "X";
        }
        return Integer.toString(i);
    }
    
    public static int getLeatherGear(Player p) {
    int count = 0;
        for (ItemStack i : p.getEquipment().getArmorContents()) {
            if (i == null)
                continue;
            
            if (i.getType().toString().contains("LEATHER_"))
                count++;
        }
        return count;
    }
    
    public static int getArmorCount(Player p) {
        int count = 0;
        for (ItemStack i : p.getInventory().getArmorContents()) {
            if (i != null && !i.getType().equals(Material.AIR))
                count++;
        }
        return count;
    }
    
    public static boolean hasFreeSpace(Player p) {
        for (ItemStack i : p.getInventory().getContents())
            if (i == null)
                return true;
        return false;
    }
    
    public static boolean isChestInventory(Inventory i) {
        return (i.getHolder() instanceof Chest) || (i.getHolder() instanceof DoubleChest);
    }
    
    public static void removeItem(Player p) {
    ItemStack i = p.getItemInHand();
        if (i.getAmount() > 1)
            i.setAmount(i.getAmount() - 1);
        else
            p.setItemInHand(i.getType().equals(Material.MUSHROOM_SOUP) || i.getType().equals(Material.RABBIT_STEW) ? new ItemStack(Material.BOWL, 1) : null);
    p.updateInventory();
    }
    
    public static ItemStack setItemAmount(ItemStack i, int amount) {
        ItemStack j = i.clone();
        j.setAmount(amount);
        return j;
    }
    
    public static void Heal(LivingEntity p, double amount) {
        p.setHealth(Math.min(p.getMaxHealth(), p.getHealth() + amount));
    }
    
    public static void extendPotionEffect(LivingEntity p, PotionEffectType type, int level, int ticksExtend) {
        if (!p.hasPotionEffect(type)) {
            p.addPotionEffect(new PotionEffect(type, ticksExtend, level));
            return;
        }
        for (PotionEffect effect : p.getActivePotionEffects()) {
            if (effect.getType().equals(type)) {
                int remaining = effect.getDuration();
                p.addPotionEffect(new PotionEffect(type, ticksExtend + remaining, level), false);
                break;
            }
        }
    }
    
    public static List<ItemStack> MaterialDataAmountWeightFormat(List<String> strings) {
        List<ItemStack> i = new ArrayList<ItemStack>();
        for (String value : strings) {
            String[] parts = value.split(":");
            if (parts.length == 1) {
                ItemStack item = translateAlternateString(value);
                for (int o = 0; o < Integer.parseInt(value.split(",")[1]); o++)
                    i.add(item);
            } else {
                /*if (parts[0].equals("xpbook")) {
                    int amount = Integer.parseInt(parts[2]), weight = Integer.parseInt(parts[4]);
                    ItemStack book = ItemDef.getXPBook(parts[1], amount, Integer.parseInt(parts[3]));
                    for (int j = 0; j < weight; j++)
                        i.add(book.clone());
                } else {*/
                    Material mat = Material.valueOf(parts[0].toUpperCase());
                    short data = Short.parseShort(parts[1]);
                    int amount = Integer.parseInt(parts[2]), weight = Integer.parseInt(parts[3]);
                    for (int j = 0; j < weight; j++)
                        i.add(new ItemStack(mat, amount, data));
                //}
            }
        }
        return i;
    }
    
    private static ItemStack translateAlternateString(String s) {
        //Bukkit.getLogger().info(s);
        if (s.equals("blowpipe"))
            return ItemDef.blowpipe;
        if (s.equals("toxicblowpipe"))
            return ItemDef.toxicblowpipe;
        if (s.equals("incendaryblowpipe"))
            return ItemDef.incendaryblowpipe;
        if (s.startsWith("spell-"))
            return SpellObject.valueOf(s.split("-")[1].split(",")[0].toUpperCase()).getItem();
        if (s.startsWith("upgradedspell-")) {
            ItemStack i = SpellObject.valueOf(s.split("-")[1].split(",")[0].toUpperCase()).getItem();
            i.addUnsafeEnchantment(Enchantment.ARROW_DAMAGE, 1);
            return i;
        }
        return null;
    }
    
    public static void removeDurability(Player p, EquipmentSlot hand) {
        boolean main = hand == EquipmentSlot.HAND;
        ItemStack i = main ? p.getInventory().getItemInMainHand() : p.getInventory().getItemInOffHand();
        i.setDurability((short) (i.getDurability() + 1));
        p.updateInventory();
        if (i.getDurability() > i.getType().getMaxDurability()) {
            if (main)
                p.getInventory().setItemInMainHand(null);
            else
                p.getInventory().setItemInOffHand(null);
            p.getWorld().playSound(p.getLocation(), Sound.ENTITY_ITEM_BREAK, 1.0F, 1.0F);
        }
    }
    
    public static void playChargeSound(final Player p) {
        new BukkitRunnable() {
            float pitch = 0.5F;
            public void run() {
                if (pitch >= 2.0F)
                    this.cancel();
                p.getWorld().playSound(p.getLocation(), Sound.BLOCK_NOTE_PLING, 1.0F, pitch);
                pitch += 0.1F;
            }
        }.runTaskTimer(Main.getPlugin(), 0L, 1L);
    }
    
    public static Set<Player> getNearbyPlayers(Entity e, double radius) {
        Set<Player> s = new HashSet<Player>();
        
        for (Entity en : e.getNearbyEntities(radius, radius * 2, radius)) {
            if (en instanceof Player) {
                s.add((Player) en);
            }
        }
        
        return s;
    }
    
    public static List<Player> getNearbyPlayersAsList(Entity e, double radius) {
        List<Player> s = new ArrayList<Player>();
        
        for (Entity en : e.getNearbyEntities(radius, radius, radius)) {
            if (en instanceof Player) {
                s.add((Player) en);
            }
        }
        
        return s;
    }
    
    public static int getNearbyPlayerCount(Entity e, double radius) {
        int count = 0;
        
        for (Entity en : e.getNearbyEntities(radius, radius * 2, radius)) {
            if (en instanceof Player) {
                count++;
            }
        }
        
        return count;
    }
    
    public static <T> T randomTermFromList(List<T> list) {
        return list.get(Main.random(list.size() - 1));
    }
    public static <T> T randomTermFromArray(T[] array) {
        return array[Main.random(array.length - 1)];
    }
    
    public static ItemStack ConstructItemStack(Material mat, int amount, short durability, String displayName, List<String> lore) {
        ItemStack i = new ItemStack(mat, amount, durability);
        ItemMeta meta = i.getItemMeta();
        meta.setDisplayName(displayName);
        meta.setLore(lore);
        i.setItemMeta(meta);
        return i;
    }
    public static ItemStack ConstructItemStack(Material mat, int amount, short durability, String displayName, String[] lore) {
        ItemStack i = new ItemStack(mat, amount, durability);
        ItemMeta meta = i.getItemMeta();
        List<String> loreList = new ArrayList<String>();
        for (String s : lore)
            loreList.add(s);
        meta.setDisplayName(displayName);
        meta.setLore(loreList);
        i.setItemMeta(meta);
        return i;
    }
    public static ItemStack ConstructItemStack(ItemStack i, String displayName, List<String> lore) {
        ItemMeta meta = i.getItemMeta();
        meta.setDisplayName(displayName);
        meta.setLore(lore);
        i.setItemMeta(meta);
        return i;
    }
    public static ItemStack ConstructItemStack(ItemStack i, String displayName, String[] strings) {
        ItemMeta meta = i.getItemMeta();
        List<String> lore = new ArrayList<String>();
        for (String s : strings)
            lore.add(s);
        meta.setDisplayName(displayName);
        meta.setLore(lore);
        i.setItemMeta(meta);
        return i;
    }
    
    public static int getItemAmount(Player p, Material m, short data) {
        int count = 0;
        
        for (ItemStack i : p.getInventory().getContents()) {
            if (i != null && i.getType() != Material.AIR) {
                if (i.getType() == m && i.getDurability() == data)
                    count += i.getAmount();
            }
        }
        return count;
    }
    
    public static boolean isInt(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }
    
    public static ItemStack colorLeatherArmor(ItemStack i, int red, int green, int blue) {
        if (i.getType().toString().contains("LEATHER_")) {
            LeatherArmorMeta meta = (LeatherArmorMeta) i.getItemMeta();
            meta.setColor(Color.fromRGB(red, green, blue));
            i.setItemMeta(meta);
        }
        return i;
    }
    
    public static boolean isWater(Block block) {
        return block.getType().equals(Material.STATIONARY_WATER) || block.getType().equals(Material.WATER);
    }
    
    public static boolean checkRespiteTalisman(Player p) {
        ItemStack[] items = p.getInventory().getContents();
        for (int i = 0; i < 9; i++) {
            if (items[i] != null && items[i].getType() == Material.BANNER && items[i].containsEnchantment(Utils.glow)) {
                if (items[i].getItemMeta().getDisplayName().equals(ItemDef.talismanOfRespite.getItemMeta().getDisplayName())) {
                    p.getInventory().setItem(i, null);
                    return true;
                }
            }
        }
        return false;
    }
    
    public static void launchFirework(Player p) {
        Firework fw = p.getWorld().spawn(p.getLocation(), Firework.class);
        FireworkMeta meta = fw.getFireworkMeta();
        Builder builder = FireworkEffect.builder();
        builder.withColor(Color.AQUA).withColor(Color.NAVY).withColor(Color.WHITE).flicker(true).trail(true).withFade(Color.WHITE);
        meta.addEffect(builder.build());
        fw.setFireworkMeta(meta);
    }
    
    private static final Map<UUID, Long> usageTimes = new HashMap<UUID, Long>();
    
    public static boolean checkResurrectionTalisman(Player p) {
        ItemStack[] items = p.getInventory().getContents();
        for (int i = 0; i < 9; i++) {
            if (items[i] != null && items[i].getType() == Material.BANNER && items[i].containsEnchantment(Utils.glow)) {
                if (!usageTimes.containsKey(p.getUniqueId()) || System.currentTimeMillis() - usageTimes.get(p.getUniqueId()) >= 1800000) {
                    if (items[i].getItemMeta().getDisplayName().equals(ItemDef.talismanOfResurrection.getItemMeta().getDisplayName())) {
                        p.getInventory().setItem(i, null);
                        usageTimes.put(p.getUniqueId(), System.currentTimeMillis());
                        return true;
                    }
                } else return false;
            }
        }
        return false;
    }
    
    public static void doStreakEffect(Entity p, int SPOKES, OrdinaryColor color) {
        for (double y = 0; y < 2; y += 0.5) {
            final Location center = p.getLocation();
            for (int i = 0; i < SPOKES; i++) {
                double angle = 2 * Math.PI * i / SPOKES;
                double x = Math.cos(angle);
                double z = Math.sin(angle);
                Location loc = center.clone().add(x, y, z);
                ParticleEffects.RED_DUST.display(color, loc, 257);
            }
        }
    }
    
    public static Location getSafeBedSpawnLocation(Player p) {
        Block bed = p.getBedSpawnLocation().getBlock();
        Location loc = Main.getOverworld().getHighestBlockAt(p.getBedSpawnLocation()).getLocation();
        for (int x = -BED_SEARCH_RADIUS; x < BED_SEARCH_RADIUS + 1; x++) {
            for (int y = -BED_SEARCH_RADIUS; y < BED_SEARCH_RADIUS + 1; y++) {
                for (int z = -BED_SEARCH_RADIUS; z < BED_SEARCH_RADIUS + 1; z++) {
                    if (bed.getRelative(x, y, z).getType() == Material.AIR && bed.getRelative(x, y + 1, z).getType() == Material.AIR) {
                        return bed.getRelative(x, y, z).getLocation().add(0.5, 0, 0.5);
                    }
                }
            }
        }
        p.sendMessage(ChatColor.GRAY + "Your Bed Spawn was �cnot safe�7, so you were moved above it!");
        Bukkit.getLogger().info("No safe bed found!");
        return loc;
    }
    
    public static String getLore(ItemStack i, int line) {
        return
        (i.getItemMeta().getLore() != null && i.getItemMeta().getLore().size() > line) ?
            i.getItemMeta().getLore().get(line) :
            "";
    }
    
    public static ItemStack oneLess(ItemStack i) {
        if (i.getAmount() == 1) return null;
        
        ItemStack a = i.clone();
        a.setAmount(a.getAmount() - 1);
        return a;
    }
    
    public static Set<ItemStack> getArmorAndWeapon(LivingEntity e) {
        Set<ItemStack> set = new HashSet<ItemStack>();
        for (ItemStack i : e.getEquipment().getArmorContents()) {
            if (i != null && i.getType() != Material.AIR)
                set.add(i);
        }
        if (e.getEquipment().getItemInMainHand() != null && e.getEquipment().getItemInMainHand().getType() != Material.AIR)
            set.add(e.getEquipment().getItemInMainHand());
        if (e.getEquipment().getItemInOffHand() != null && e.getEquipment().getItemInOffHand().getType() != Material.AIR)
            set.add(e.getEquipment().getItemInOffHand());
        return set;
    }

    
    public static LivingEntity getActualDamager(Entity e) {
        if (e instanceof LivingEntity)
            return (LivingEntity) e;
        if (e instanceof Projectile && ((Projectile) e).getShooter() instanceof LivingEntity)
            return (LivingEntity) ((Projectile) e).getShooter();
        return null;
    }
    

    /**
     * Display a ring of particles
     * @param eff the particle effect
     * @param loc center of ring
     * @param i radius of ring
     */
    public static void displayParticleRing(final ParticleEffects eff, final Location loc, final int i) {
        new BukkitRunnable() {
            double radius = 0;
            int timer = 0;
                public void run() {
                    for (int i = 0; i < 24; i++) {
                        double angle = (2 * Math.PI * i / 32) + (radius - 1) / 1.5;
                        double x = radius * Math.cos(angle);
                        double z = radius * Math.sin(angle);
                        eff.display(0.1F, 0.1F, 0.1F, 0.1F, 3, loc.clone().add(x, 0.2, z), 257);
                    }
                    radius += 0.5;
                    timer ++;
                    if (timer >= i * 2)
                        this.cancel();
                }
        }.runTaskTimer(Main.getPlugin(), 0L, 2L);
    }
    
    public static Vector toTarget(LivingEntity boss, LivingEntity target) {
        if (target == null)
            target = getNearestPlayer(boss, 50);
        if (target == null)
            return new Vector(0, 1, 0);
        
        Vector dir = target.getLocation().subtract(boss.getLocation()).toVector();
        return dir;
    }
    
    public static Player getNearestPlayer(LivingEntity boss, int radius) {
        double dist = Double.POSITIVE_INFINITY;
        Entity targ = null;
        
        for (Entity e : boss.getNearbyEntities(radius, radius, radius)) {
            if (e instanceof Player) {
                double d = e.getLocation().distanceSquared(boss.getLocation());
                if (d < dist) {
                    dist = d;
                    targ = e;
                }
            }
        }
        
        return (Player) targ;
    }

}
