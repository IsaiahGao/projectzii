package projectz.tasks;

import org.bukkit.block.Biome;

public class BiomeTemperature {
    
    /*
     *  -1 = cold
     *  0 = neutral
     *  1 = hot
     *  
     */
    
    public static float getTemperature(Biome biome, int ycoord) {
        switch (biome) {
            case BIRCH_FOREST:
            case BIRCH_FOREST_HILLS:
            case MUTATED_BIRCH_FOREST:
            case MUTATED_BIRCH_FOREST_HILLS:
                return 0F;
            case DEEP_OCEAN:
            case OCEAN:
            case RIVER:
            case STONE_BEACH:
            case BEACHES:
                return -0.05F;
            case DESERT:
            case DESERT_HILLS:
            case MUTATED_DESERT:
                return 1.0F;
            case EXTREME_HILLS:
            case EXTREME_HILLS_WITH_TREES:
            case MUTATED_EXTREME_HILLS:
            case MUTATED_EXTREME_HILLS_WITH_TREES:
            case SMALLER_EXTREME_HILLS:
                return ycoord > 100 ? -0.8F : -0.2F;
            case FOREST:
            case FOREST_HILLS:
            case MUTATED_FOREST:
                return 0F;
            case HELL:
                return 1.0F;
            case ICE_FLATS:
            case ICE_MOUNTAINS:
            case MUTATED_ICE_FLATS:
            case FROZEN_OCEAN:
            case FROZEN_RIVER:
                return -1.0F;
            case JUNGLE:
            case JUNGLE_EDGE:
            case JUNGLE_HILLS:
            case MUTATED_JUNGLE:
            case MUTATED_JUNGLE_EDGE:
                return 0.6F;
            case MESA:
            case MESA_CLEAR_ROCK:
            case MESA_ROCK:
            case MUTATED_MESA:
            case MUTATED_MESA_CLEAR_ROCK:
            case MUTATED_MESA_ROCK:
                return 0.9F;
            case MUSHROOM_ISLAND:
            case MUSHROOM_ISLAND_SHORE:
                return -0.05F;
            case PLAINS:
            case MUTATED_PLAINS:
                return 0.05F;
            case REDWOOD_TAIGA:
            case REDWOOD_TAIGA_HILLS:
            case MUTATED_REDWOOD_TAIGA:
            case MUTATED_REDWOOD_TAIGA_HILLS:
            case MUTATED_TAIGA:
            case TAIGA:
            case TAIGA_HILLS:
                return -0.3F;
            case ROOFED_FOREST:
            case MUTATED_ROOFED_FOREST:
                return -0.1F;
            case SAVANNA:
            case SAVANNA_ROCK:
            case MUTATED_SAVANNA:
            case MUTATED_SAVANNA_ROCK:
                return 0.7F;
            case TAIGA_COLD:
            case MUTATED_TAIGA_COLD:
            case TAIGA_COLD_HILLS:
            case COLD_BEACH:
                return -0.8F;
            case SWAMPLAND:
            case MUTATED_SWAMPLAND:
                return -0.2F;
            case SKY:
            case VOID:
                return -1.0F;
            default:
                return 0F;
        }
    }
    
    public static float getTargetTemperature(Biome b, int altitude) {
        float f = getTemperature(b, altitude);
        return (f + 1) * 100;
    }

}
