package projectz.tasks;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import projectz.Main;
import shadowrealm.augments.AugmentEnchants;
import shadowrealm.augments.AugmentEnums.Augment;
import utilities.Utils;
import utilities.particles.ParticleEffects;

public class ParticleTasks {
    
    private static Set<Player> SweatyPeople = new HashSet<Player>();
    private static Map<Player, Integer> SoakedPeople = new WeakHashMap<Player, Integer>();
    
    public ParticleTasks() {
        new BukkitRunnable() {
            @Override
            public void run() {
                for (Iterator<Player> it = SweatyPeople.iterator(); it.hasNext();) {
                    Player p = it.next();
                    if (p.isOnline()) {
                        ParticleEffects.SPLASH.display(0.2F, 0.4F, 0.2F, 1.0F, 12, p.getLocation().add(0, 1, 0), 257);
                    } else {
                        makeNotSweaty(p);
                        it.remove();
                    }
                }
                for (Iterator<Player> it = SoakedPeople.keySet().iterator(); it.hasNext();) {
                    Player p = it.next();
                    if (p.isOnline()) {
                        if (!((CraftPlayer) p).getHandle().inWater)
                            ParticleEffects.DRIP_WATER.display(0.2F, 0.4F, 0.2F, 1.0F, SoakedPeople.get(p) * 2, p.getLocation().add(0, 1, 0), 257);
                    } else {
                        it.remove();
                    }
                }
            }
        }.runTaskTimer(Main.getPlugin(), 40L, 5L);
    }
    
    public enum SoakLevel {
        WET(2, "�7The rain wets your clothes.", "�7You clothes are now almost dry."),
        SOAKED(4, "�7The rain soaks your clothes.", "�7You clothes are no longer as soaked."),
        DRENCHED(6, "�7Your clothes are now thoroughly drenched.", "�7You clothes are no longer as drenched.");
        
        private SoakLevel(int i, String msg, String dry) {
            this.i = i;
            this.msg = msg;
            this.dry = dry;
        }
        
        private int i;
        private String msg, dry;
        
        public int i() {
            return i;
        }
        
        public void sendMessage(Player p) {
            p.sendMessage(msg);
        }
        
        public void sendDryMessage(Player p) {
            p.sendMessage(dry);
        }
    }
    
    public static void makeSweaty(Player p) {
        if (SweatyPeople.add(p)) {
            p.sendMessage("�7You begin to sweat from the heat.");
        }
    }
    
    public static void makeNotSweaty(Player p) {
        if (SweatyPeople.remove(p)) {
            p.sendMessage("�7Your sweating subsides.");
        }
    }
    
    public static void Soak(Player p, int toAdd) {
        int ws = getWetsuit(p);
        if (ws == 4 || (Utils.random(4 - ws) == 0 && toAdd < 3))
            return;
        
        int original = SoakedPeople.containsKey(p) ? SoakedPeople.get(p) : 0;
        if (original == 8) return;
        
        int soak = Math.min(8, original + toAdd);
        SoakedPeople.put(p, soak);
        makeNotSweaty(p);
        
        if (soak == 6 || soak == 8)
            SoakLevel.DRENCHED.sendMessage(p);
        else if (soak == 4)
            SoakLevel.SOAKED.sendMessage(p);
        else if (soak == 2)
            SoakLevel.WET.sendMessage(p);
    }
    
    public static void Unsoak(Player p, int toRemove) {
        if (isSoaked(p)) {
            int soak = Math.max(0, SoakedPeople.get(p) - toRemove);
            SoakedPeople.put(p, soak);
        
            if (soak == 5)
                SoakLevel.DRENCHED.sendDryMessage(p);
            else if (soak == 3)
                SoakLevel.SOAKED.sendDryMessage(p);
            else if (soak == 1)
                SoakLevel.WET.sendDryMessage(p);
            else if (soak <= 0) {
                p.sendMessage("�7Your clothes have dried.");
                SoakedPeople.remove(p);
            } else
                p.sendMessage("�7Your clothes dry slightly.");
        }
    }
    
    public static int getWetsuit(Player p) {
        int count = 0;
        for (ItemStack i : p.getInventory().getArmorContents()) {
            if (i != null && i.containsEnchantment(Augment.WATERPROOF.getEnchantment()))
                count++;
        }
        return count;
    }
    
    public static void dryOff(Player p) {
        SoakedPeople.remove(p);
    }
    
    public static int getSoak(Player p) {
        return SoakedPeople.containsKey(p) ? SoakedPeople.get(p) : 0;
    }
    
    public static boolean isSweaty(Player p) {
        return SweatyPeople.contains(p);
    }
    
    public static boolean isSoaked(Player p) {
        return SoakedPeople.containsKey(p);
    }

}
