package projectz.tasks;

import java.text.DecimalFormat;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;

import projectz.Main;
import projectz.tasks.DepletionTaskCycler.StatType;

public class PersonalBoard {

    private static final String BLANK = "                ";
    private static final DecimalFormat DF = new DecimalFormat("00.0");
    
    public PersonalBoard(Player owner) {
        p = owner;
        board = Main.manager.getNewScoreboard();
        display = board.registerNewObjective("Stats", "dummy");
        b(7);
        b(5);
        c("�f�m-----------------", 9);
        
        for (StatType s : StatType.values())
            update(s);
        
        BoardHandler.createScoreboard(owner, this);

        display.setDisplayName("�7�l.:==<< �f�lStats �7�l>>==:.");
        display.setDisplaySlot(DisplaySlot.SIDEBAR);

        p.setScoreboard(board);
        Main.getCycler().addPlayerTask(p);
    }
    
    private Player p;
    private Scoreboard board;
    private Objective display;
    private Score Hydration, Sanity, Warmth;
    
    public void update(StatType type) {
        switch (type) {
        case HYDRATION:
            if (Hydration != null)
                board.resetScores(Hydration.getEntry());

            if (type.get(p) > 102)
                type.set(p, 102, false);
            
            // 💧
            Hydration = c("�f�l[�b�lHydration�f�l] " + a(type.get(p), true), 8);
            break;
        case SANITY:
            if (Sanity != null)
                board.resetScores(Sanity.getEntry());

            if (type.get(p) > 102)
                type.set(p, 102, false);
            
            Sanity = c("�f�l[�d�lSanity�f�l] " + a(type.get(p), true), 6);
            break;
        case WARMTH:
            if (Warmth != null)
                board.resetScores(Warmth.getEntry());
            Warmth = c("�f�l[�c�lWarmth�f�l] " + b(type.get(p)), 4);
            break;
        default:
            break;
        }
    }
    
    private String b(float stat) {
        // 37 = normal, 30 = 0, 43 = hot
        float delta = 14 * (stat / 200) + 30;
        return getColor(stat) + "" + DF.format(delta) + "�f ��lC";
    }
    
    private String a(float stat, boolean cap) {
        return getColor(stat) + "" + (int) (cap ? Math.min(100, stat) : stat) + "�f�l%";
    }
    
    private void b(int i) {
        Score blank = display.getScore(BLANK.substring(i));
        blank.setScore(i);
    }
    
    private Score c(String s, int i) {
        Score blank = display.getScore(s);
        blank.setScore(i);
        return blank;
    }
    
    public Scoreboard getScoreboard() {
        return board;
    }
    
    private static ChatColor getColor(float i) {
        if (i > 189 || i < 11) return ChatColor.DARK_RED;
        if (i > 174 || i < 26) return ChatColor.RED;
        if (i > 149 || i < 51) return ChatColor.GOLD;
        if (i > 109 || i < 89) return ChatColor.YELLOW;
        return ChatColor.GREEN;
    }

}
