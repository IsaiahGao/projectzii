package projectz.tasks;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public class RBN {
    
    // relevant blocks nearby
    public RBN(Player p) {
        Block b = p.getLocation().getBlock();
        for (int x = -2; x <= 2; x++) {
            for (int z = -2; z <= 2; z++) {
                for (int y = -2; y <= 2; y++) {
                    Block rel = b.getRelative(x, y, z);
                    switch (rel.getType()) {
                    case RED_ROSE:
                    case YELLOW_FLOWER:
                        flowers++;
                        break;
                    case DOUBLE_PLANT:
                        if (rel.getData() == 8)
                            flowers++;
                        break;
                    case TORCH:
                        heat += 0.08;
                        light += 0.5;
                        break;
                    case BURNING_FURNACE:
                        light += 1;
                        heat += 2;
                        break;
                    case GLOWSTONE:
                    case SEA_LANTERN:
                        light += 1;
                        break;
                    case FIRE:
                        heat += 1;
                        light += 1;
                        break;
                    case LAVA:
                        heat += 5 * (b.getData() > 7 ? 1 : (7 - b.getData()) / 7D);
                        light += 1;
                        break;
                    case STATIONARY_LAVA:
                        heat += 5;
                        light += 1;
                        break;
                    case ICE:
                    case PACKED_ICE:
                        heat -= 0.8;
                        break;
                    case SNOW_BLOCK:
                        heat -= 0.3;
                        break;
                    case SNOW:
                        heat -= 0.03;
                        break;
                    case SOUL_SAND:
                    case WEB:
                        insanity ++;
                        break;
                    default:
                        break;
                    }
                }
            }
        }
    }
    
    public double flowers, heat, light, insanity;
    
    public double getFlowers(double d) {
        return Math.min(flowers, d);
    }
    
    public double getHeat(double d) {
        return -heat < -d ? -d : (heat > d ? d : heat);
    }
    
    public double getLight(double d) {
        return Math.min(light, d);
    }

}
