package projectz.tasks;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.block.Biome;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import projectz.Main;
import projectz.Utilities;
import projectz.handlers.Vitals;
import projectz.handlers.VitalsHandler;
import shadowrealm.augments.AugmentEnums.Augment;
import shadowrealm.augments.augments.StatPassive;
import utilities.Utils;
import utilities.particles.ParticleEffects;

public class DepletionTaskCycler {

    private double factorWarmth = 0.7;
    private double factorHydration = 3;
    private double factorSanity = 0.7;
    private long ticks_elapsed;
    
    public DepletionTaskCycler(int cycletime) {
        this.cycletime = cycletime;
        activate();
    }
    
    private int cycletime;
    private List<Player> players = new ArrayList<Player>();
    
    private void activate() {
        Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(Main.getPlugin(), new Runnable() {
            
            @Override
            public void run() {
                
                ++ticks_elapsed;
                if (!players.isEmpty()) {
                        Iterator<Player> it = players.iterator();
                        while (it.hasNext()) {
                        Player p = it.next();
                            if (!p.isOnline()) {
                                it.remove();
                            } else if (!p.isDead() && p.getGameMode() != GameMode.CREATIVE) {
                                tickStats(p);
                            }
                        }
                }
                    
            }
            
        }, cycletime * 20L, cycletime * 20L);
    }
    
    public void addPlayerTask(Player player) {
        if (!players.contains(player)) {
            players.add(player);
        }
    }
    
    public void removePlayerTask(Player player) {
        if (players.contains(player)) {
            players.remove(player);
        }
    }
    
    private void tickStats(Player p) {
        Block block = p.getLocation().getBlock();
        boolean Cold = Utilities.isCold(block),
        Hot = Utilities.isDesert(block),
        Underground = block.getBiome() == Biome.HELL || block.getLightFromSky() < 15,
        Rain = block.getWorld().hasStorm(),
        IsWater = !p.isInsideVehicle() && Utilities.isWater(block),
        Sleeping = p.isSleeping();
        
        RBN rbn = new RBN(p);
        
        if (IsWater && (!ParticleTasks.isSoaked(p) || ParticleTasks.getSoak(p) < 8))
            ParticleTasks.Soak(p, 8);
        
        /*
         * HYDRATION
         */
        if (!Hot && Rain && !Underground) {
            if (Main.random(3) == 0) p.sendMessage(ChatColor.BLUE + "The rain slightly restores your hydration.");
            ParticleTasks.Soak(p, 1);
            StatType.HYDRATION.change(p,  1);
        } else if (!IsWater) {
            float h = StatType.HYDRATION.get(p);
            if (h < 25) {
                p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, Main.exhaustDelay * 20 + 40, (h < 13) ? 1 : 0, true), true);
                p.addPotionEffect(new PotionEffect(PotionEffectType.HUNGER, Main.exhaustDelay * 20 + 40, (h < 13) ? 1 : 0, true), true);
                p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, Main.exhaustDelay * 20 + 40, (h < 15) ? 1 : 0, true), true);
            }
            if (h < 10)
                p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, Main.exhaustDelay * 20 + 40, (h < 5) ? 3 : 2, true), true);
            double amount = (Underground) ? 0.1 : 0.2;
            if (Hot)
                amount += 0.3;
            if (ParticleTasks.isSweaty(p))
                amount += 0.3;
            amount += Utilities.getArmorCount(p) * 0.0725;
            
            if (p.hasMetadata("dehydration")) {
                if (amount > 0)
                    amount *= 2;
                int i = p.getMetadata("dehydration").get(0).asInt();
                if (i == 1) {
                    p.removeMetadata("dehydration", Main.getPlugin());
                    p.sendMessage(ChatColor.GRAY + "Your sickness from drinking Salt Water has worn off.");
                } else
                    p.setMetadata("dehydration", new FixedMetadataValue(Main.getPlugin(), i - 1));
            }
            
            float deltah = (float) Math.abs(amount * (Sleeping ? 0.4 * factorHydration : factorHydration));
            float hsat = StatType.HYDRATION_SAT.get(p);
            if (hsat > 0) {
                StatType.HYDRATION_SAT.change(p, -deltah);
                deltah -= hsat;
            }
            if (deltah > 0)
                StatType.HYDRATION.change(p, -deltah);
        }
                
        /*
         * SANITY
         */
        if (!Sleeping) {
            float s = StatType.SANITY.get(p);
            if (s < 25) {
                p.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, Main.exhaustDelay * 20 + 40, (s < 13) ? 1 : 0, true), true);
                p.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, Main.exhaustDelay * 20 + 40, 2, true), true);
                p.playSound(p.getLocation(), Utilities.randomSounds[Main.random(Utilities.randomSounds.length - 1)], 0.5F, (float) (Math.random() * 2));
                
                if (Math.random() < 7D / (s + 10)) {
                    try {
                        ParticleEffects.MOB_APPEARANCE.display(0, 0, 0, 1.0F, 1, p.getLocation(), p);
                    } catch(Exception e) {
                        Bukkit.getLogger().info("Guardian particle failed.");
                    }
                }
                
                if (Math.random() < 3D / (s + 10)) {
                    for (int i = 0; i < Utils.random(2) + 1; i++)
                        InsanityHandler.spawnFakeMob(p);
                }
                if (Math.random() < 5D / (s + 10)) {
                    for (int i = 0; i < Utils.random(2) + 1; i++)
                        InsanityHandler.spawnFakeOre(p);
                }
            }
            
            if (s < 10) {
                p.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, Main.exhaustDelay * 20 + 40, 0, true), true);
                p.damage(2 + Main.random((int) (10 - s)));
                p.playSound(p.getLocation(), Utilities.randomSounds[Main.random(Utilities.randomSounds.length - 1)], 1.0F, (float) (Math.random() + 1));
            }
            
            double amount2 = p.hasPotionEffect(PotionEffectType.NIGHT_VISION) ? 0 : (9 - block.getLightLevel()) * 5 / 20;
            amount2 += getSanityGain(p.getInventory().getItemInMainHand()) + getSanityGain(p.getInventory().getItemInOffHand());
            amount2 -= Math.min(0.45, (rbn.flowers + rbn.light) * 0.033);
            amount2 += 0.1 * (20 - p.getHealth());
            amount2 += Math.min(0.45, rbn.insanity * 0.05);
            if (block.getY() < 32 && block.getBiome() != Biome.HELL)
                amount2 += (32 - block.getY()) / 20;
            StatType.SANITY.change(p, (float) (amount2 * -factorSanity));
            //if (CommandHandler.debugging(p)) p.sendMessage("SANITY: " + (Math.round(-4 * amount2 / 5)));
        }
            
        /*
         * WARMTH
         */
        if (!Sleeping) {
            float w = StatType.WARMTH.get(p);
            if (w <= 25 || w > 175) {
                p.damage(Main.random((int) ((25 - w) / 2)));
                p.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, Main.exhaustDelay * 20 + 40, (w < 13) ? 3 : 1, true), true);
                p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, Main.exhaustDelay * 20 + 40, (w < 13) ? 1 : 0, true), true);
                if (w <= 10 || w >= 190) {
                    p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, Main.exhaustDelay * 20 + 40, 3, true), true);
                    p.sendMessage(w <= 10 ? ChatColor.GRAY + "You're too cold!" : ChatColor.GRAY + "You're overheating!");
                    p.damage((w <= 10 ? (11 - w) : (w - 190)) * 2);
                }
            }
            if (w > 150) {
                if (Main.random(800) <= w) {
                    p.setFireTicks((Main.random((int) (w / 15)) + 2) * 20);
                    p.sendMessage(ChatColor.GRAY + "You spontaneously combust from being overheated!");
                }
            }
            
            if (!IsWater) {
                if (Hot && w > 110 && !ParticleTasks.isSoaked(p))
                    ParticleTasks.makeSweaty(p);
                else if (w < 107)
                    ParticleTasks.makeNotSweaty(p);
            }
            
            float biomeTemp = BiomeTemperature.getTargetTemperature(block.getBiome(), block.getY());
            double toRemove = (w - biomeTemp) / 4; // TODO
            if (Math.abs(toRemove) > 14) {
                toRemove = Math.signum(toRemove) * 14;
            }
            toRemove += getWarmthChange(p.getInventory().getItemInMainHand());
            toRemove += getWarmthChange(p.getInventory().getItemInOffHand());
            
            long tod = p.getWorld().getTime();
            if (tod > 12000) {
                // night time
                toRemove += Math.min(1, (tod - 12000) / 7500D) * 5;
            } else {
                toRemove += Math.abs(6000 - tod) / 6000D * 5;
            }
            
            if (ParticleTasks.isSweaty(p))
                toRemove += 0.3;
            if (p.isSprinting())
                toRemove -= 0.5;
            
            if (Cold) {
                toRemove += 0.5;
                toRemove -= Utilities.getLeatherGear(p) * 5 / 20D;
                toRemove += 2D * block.getLightLevel() / 30D;
                
                if (IsWater)
                    toRemove += 7;
                else if (block.getLightFromBlocks() >= 13)
                    toRemove -= Math.pow(block.getLightFromBlocks(), 2) / 70D;

                if (block.getType() == Material.SNOW)
                    toRemove += 1;
                
                if (Rain && !Underground)
                    toRemove += 3.5;
            } else {
                if (Rain && !Underground)
                    toRemove += 2;
                if (Hot && !p.hasPotionEffect(PotionEffectType.FIRE_RESISTANCE)) {
                    toRemove += IsWater ? -0.5 : -0.1 * (block.getTemperature() * 12.5);
                } else {
                    if (!Rain)
                        toRemove += (w > 100) ? 0.5 : -0.5;
                    toRemove += (11 - block.getLightLevel()) * 0.2;
                }
                if (toRemove < 0) {
                    toRemove *= 0.05 + (0.05 * ((110 - StatType.HYDRATION.get(p)) / 50D));
                }
            }
            
            toRemove -= rbn.heat * 0.033;
            toRemove -= Math.min(Utilities.getNearbyPlayerCount(p, 2.5) * 0.6, 4.8);
            toRemove += ParticleTasks.getSoak(p) * (Hot ? 0.3 : (Cold ? 0.9 : 0.5));
                        
            StatType.WARMTH.change(p, (float) (toRemove * -factorWarmth));
            //if (CommandHandler.debugging(p)) p.sendMessage("WARMTH: " + (Math.round(-3 * toRemove / 4)));
        }
        
        if (Sleeping) {
            StatType.SANITY.change(p, 15);
            if (StatType.WARMTH.get(p) < 1000) StatType.WARMTH.change(p, 15);
        }
        
        if (ticks_elapsed % 2 == 0 && !IsWater && (!Rain || Underground || Hot) && block.getLightLevel() > 7) {
            ParticleTasks.Unsoak(p, Hot ? 2 : 1);
        }

    }
    
    private static double getWarmthChange(ItemStack item) {
        int toRemove = 0;
        switch (item.getType()) {
            case TORCH:
                toRemove -= 0.2;
                break;
            case LAVA_BUCKET:
                toRemove -= 1;
                break;
            case ICE:
            case PACKED_ICE:
                toRemove += 1;
                break;
                default:
                    break;
        }
        return toRemove;
    }

    private static int getSanityGain(ItemStack item) {
        int amount2 = 0;
        switch (item.getType()) {
            case TORCH:
            case RED_ROSE:
            case YELLOW_FLOWER:
                amount2 -= 0.5;
                break;
            case GLOWSTONE:
            case SEA_LANTERN:
            case JACK_O_LANTERN:
                amount2 -= 0.9;
                break;
                default:
                    break;
        }
        return amount2;
    }

    public static enum StatType {
        HYDRATION(0),
        HYDRATION_SAT(1),
        WARMTH(2),
        SANITY(3);
        
        private StatType(int index) {
            this.index = index;;
        }
        
        private int index;
        
        public int getIndex() {
            return index;
        }
        
        public float get(Player p) {
            return VitalsHandler.getVitals(p).data[index];
        }
        
        public void change(Player p, float amount) {
            if (amount == 0)
                return;

            if (p.getEquipment().getHelmet() != null)
                for (Enchantment e : p.getEquipment().getHelmet().getEnchantments().keySet()) {
                    Augment a = Augment.getAugment(e);
                    if (a != null && a.getAugment() instanceof StatPassive) {
                        float mod = ((StatPassive) a.getAugment()).getCombinedModifier(p, amount);
                        amount *= mod;
                    }
                }

            if (this == HYDRATION_SAT && amount > 0 && HYDRATION_SAT.get(p) < amount) {
                VitalsHandler.getVitals(p).data[index] = amount;
            } else {
                Vitals vitals = VitalsHandler.getVitals(p);
                vitals.data[index] += amount;
                if (vitals.data[index] < 0)
                    vitals.data[index] = 0;
            }
            BoardHandler.getBoard(p).update(this);
        }
        
        public void set(Player p, float amount, boolean update) {
            VitalsHandler.getVitals(p).data[index] = amount;
            if (update) BoardHandler.getBoard(p).update(this);
        }
    }

    public static void homeostasis(Player p, float amount) {
        if (amount == 0) return;
        float w = StatType.WARMTH.get(p);
            if (w >= 103) {
                StatType.WARMTH.change(p, -amount);
            } else if (w <= 97) {
                StatType.WARMTH.change(p, amount);
            }
    }
    
    public static void addDehydrationEffect(final Player p, int cycles) {
        int i = p.hasMetadata("dehydration") ? p.getMetadata("dehydration").get(0).asInt() : 0;
        p.setMetadata("dehydration", new FixedMetadataValue(Main.getPlugin(), cycles + i));
    }

}
