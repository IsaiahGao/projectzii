package projectz.tasks;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

public class BoardHandler {
    
    private static Map<UUID, PersonalBoard> scoreboards = new HashMap<UUID, PersonalBoard>();
     
    public static void createScoreboard(Player player, PersonalBoard pb) {
        scoreboards.put(player.getUniqueId(), pb);
    }
    
    public static void removeScoreboard(OfflinePlayer player) {
        scoreboards.remove(player.getUniqueId());
    }
    
    public static PersonalBoard getBoard(OfflinePlayer p) {
        return scoreboards.get(p.getUniqueId());
    }

}
