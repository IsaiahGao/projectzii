package projectz.tasks;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_12_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import net.minecraft.server.v1_12_R1.EntityBlaze;
import net.minecraft.server.v1_12_R1.EntityChicken;
import net.minecraft.server.v1_12_R1.EntityCow;
import net.minecraft.server.v1_12_R1.EntityCreeper;
import net.minecraft.server.v1_12_R1.EntityGhast;
import net.minecraft.server.v1_12_R1.EntityGuardian;
import net.minecraft.server.v1_12_R1.EntityLiving;
import net.minecraft.server.v1_12_R1.EntityPig;
import net.minecraft.server.v1_12_R1.EntitySkeleton;
import net.minecraft.server.v1_12_R1.EntitySpider;
import net.minecraft.server.v1_12_R1.EntityWitch;
import net.minecraft.server.v1_12_R1.EntityZombie;
import net.minecraft.server.v1_12_R1.PacketPlayOutSpawnEntityLiving;
import net.minecraft.server.v1_12_R1.World;
import net.minecraft.server.v1_12_R1.WorldServer;
import utilities.Utils;

public class InsanityHandler {
    
    private static List<Class<? extends EntityLiving>> mobs = new ArrayList<>();
    private static List<Material> blocks = new ArrayList<>();
    
    static {
        mobs.add(EntityGhast.class);
        mobs.add(EntityGuardian.class);
        mobs.add(EntitySpider.class);
        mobs.add(EntityZombie.class);
        mobs.add(EntitySkeleton.class);
        mobs.add(EntityPig.class);
        mobs.add(EntityChicken.class);
        mobs.add(EntityCreeper.class);
        mobs.add(EntityCow.class);
        mobs.add(EntityBlaze.class);
        mobs.add(EntityWitch.class);
        
        blocks.add(Material.DIAMOND_BLOCK);
        blocks.add(Material.DIAMOND_ORE);
        blocks.add(Material.EMERALD_ORE);
        blocks.add(Material.ANVIL);
        blocks.add(Material.GOLD_ORE);
        blocks.add(Material.GOLD_BLOCK);
        blocks.add(Material.REDSTONE_ORE);
    }
    
    public static void spawnFakeMob(Player p) {
        Location loc = null;
        
        for (int i = 0; i < 7; i++) {
            if (loc != null)
                break;
            
            loc = swim(p.getLocation().add(25 * (Math.random() - 0.5), 0, 25 * (Math.random() - 0.5)), 5, 5);
        }
        
        if (loc != null)
            spawnFakeMob(p, Utils.get(mobs), loc);
    }
    
    public static void spawnFakeMob(Player p, Class<? extends EntityLiving> clazz, Location loc) {
        try {
            WorldServer s = ((CraftWorld)loc.getWorld()).getHandle();
            Constructor<? extends EntityLiving> cons = clazz.getConstructor(World.class);
            EntityLiving entity = cons.newInstance(s);
           
            entity.setLocation(loc.getX(), loc.getY(), loc.getZ(), 0, 0);
           
            PacketPlayOutSpawnEntityLiving packet = new PacketPlayOutSpawnEntityLiving(entity);
            ((CraftPlayer)p).getHandle().playerConnection.sendPacket(packet);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static void spawnFakeOre(Player p) {
        Location loc = null;
        
        for (int i = 0; i < 7; i++) {
            if (loc != null)
                break;
            
            loc = swim(p.getLocation().add(25 * (Math.random() - 0.5), 0, 25 * (Math.random() - 0.5)), 5, 5);
        }
        
        if (loc != null) {
            p.sendBlockChange(loc.add(0, -1, 0), Utils.get(blocks), (byte) 0);
        }
    }
    
    public static Location swim(Location target, int limitBelow, int limitAbove) {
        int prog = 0;
        Location loc = target.clone();
        if (Utils.passable(loc.getBlock())) {
            while (Utils.passable(loc.getBlock())) {
                if (prog > limitBelow)
                    break;
                
                loc.add(0, -1, 0);
                prog++;
            }
            if (!Utils.passable(loc.getBlock()))
                return loc.add(0, 1, 0);
        }
        
        prog = 0;
        loc = target.clone();
        while (!Utils.passable(loc.getBlock())) {
            if (prog > limitAbove)
                break;
            
            loc.add(0, 1, 0);
            prog++;
        }
        
        if (Utils.passable(loc.getBlock()))
            return loc;
        return null;
    }

}
