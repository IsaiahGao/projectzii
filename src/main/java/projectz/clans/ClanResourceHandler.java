package projectz.clans;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import projectz.Utilities;

public class ClanResourceHandler {

    private static Inventory ResourceInterfaceBase = Bukkit.createInventory(null, 18);
    
    private static String[] BaseLore = {
        "�f�l[ �bLEFT CLICK �f�l> �bDeposit 1 �f�l]",
        "�f�l[ �eRIGHT CLICK �f�l> �bWithdraw 1 �f�l]",
        "�fHold SHIFT �7to deposit/withdraw 64 at a time."
    };
    
    static {
        ResourceInterfaceBase.setItem(1, Utilities.ConstructItemStack(new ItemStack(Material.LOG), "�cWood : �70", BaseLore));
        ResourceInterfaceBase.setItem(2, Utilities.ConstructItemStack(new ItemStack(Material.COBBLESTONE), "�cCobblestone : �70", BaseLore));
        ResourceInterfaceBase.setItem(3, Utilities.ConstructItemStack(new ItemStack(Material.STONE), "�cStone : �70", BaseLore));
        ResourceInterfaceBase.setItem(4, Utilities.ConstructItemStack(new ItemStack(Material.COAL), "�cCoal : �70", BaseLore));
        ResourceInterfaceBase.setItem(5, Utilities.ConstructItemStack(new ItemStack(Material.IRON_INGOT), "�cIron Ingots : �70", BaseLore));
        ResourceInterfaceBase.setItem(10, Utilities.ConstructItemStack(new ItemStack(Material.GOLD_INGOT), "�cGold Ingots : �70", BaseLore));
        ResourceInterfaceBase.setItem(11, Utilities.ConstructItemStack(new ItemStack(Material.REDSTONE), "�cRedstone : �70", BaseLore));
        ResourceInterfaceBase.setItem(12, Utilities.ConstructItemStack(new ItemStack(Material.DIAMOND), "�cDiamonds : �70", BaseLore));
        ResourceInterfaceBase.setItem(13, Utilities.ConstructItemStack(new ItemStack(Material.OBSIDIAN), "�cObsidian : �70", BaseLore));
        ResourceInterfaceBase.setItem(14, Utilities.ConstructItemStack(new ItemStack(Material.EMERALD), "�cEmeralds : �70", BaseLore));
        ResourceInterfaceBase.setItem(6, Utilities.ConstructItemStack(new ItemStack(Material.NETHERRACK), "�cNetherrack : �70", BaseLore));
        ResourceInterfaceBase.setItem(7, Utilities.ConstructItemStack(new ItemStack(Material.NETHER_BRICK), "�cNether Brick : �70", BaseLore));
        ResourceInterfaceBase.setItem(15, Utilities.ConstructItemStack(new ItemStack(Material.QUARTZ_BLOCK), "�cQuartz : �70", BaseLore));
        ResourceInterfaceBase.setItem(16, Utilities.ConstructItemStack(new ItemStack(Material.GLOWSTONE), "�cGlowstone : �70", BaseLore));
    }
    
    public static Inventory getInterfaceBase() {
        return ResourceInterfaceBase;
    }

}