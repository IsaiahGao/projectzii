package projectz.clans;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import projectz.Main;
import projectz.Utilities;
import projectz.clans.ClanInvite.InviteAction;
import utilities.Utils;

public class Clan {

    public enum Role {
        LEADER(0, "Leader", "[\u272F]"),
        CO_LEADER(1, "Co-Leader", "[\u2606]"),
        CAPTAIN(2, "Captain", "[+]"),
        MEMBER(3, "Member", "[\u25CB]"),
        RECRUIT(4, "Recruit", ""),
        UNDEFINED(5, "", "");
        
        private Role(int seniority, String name, String tag) {
            this.seniority = seniority;
            this.name = name;
            this.tag = tag;
        }
        
        private int seniority;
        private String name, tag;
        
        public int getID() {
            return seniority;
        }
        
        public String getName() {
            return name;
        }
        
        public String getTag() {
            return tag;
        }
        
        public String getRawTag() {
            return tag.replace("[", "").replace("]", "");
        }
        
        private static Map<Integer, Role> roles = new HashMap<Integer, Role>();
        static {
            for (Role r : Role.values()) {
                roles.put(r.getID(), r);
            }
        }
        
        public static Role getRole(int id) {
            return roles.get(id);
        }
    }
    
    public Clan(int ID, String name, String tag, int whoCanClaim, int whoCanWithdraw, int whoCanRecruit, Set<String> claimedchunks, Map<UUID, Role> members, Map<Material, Integer> resources, int hubX, int hubY, int hubZ) {
        this.Lvl = ID;
        this.name = name;
        this.tag = tag;
        this.hubX = hubX;
        this.hubY = hubY;
        this.hubZ = hubZ;
        this.chunks = claimedchunks;
        this.members = members;
        this.whoCanClaim = whoCanClaim;
        this.whoCanWithdraw = whoCanWithdraw;
        this.whoCanRecruit = whoCanRecruit;
        this.resources = resources;
        RegionHandler.takenTags.add(tag);
        RegionHandler.existingClans.add(this);
        RegionHandler.claimNames.put(name.toLowerCase(), this);
        for (UUID u : members.keySet()) {
            Role role = members.get(u);
            Member member =  new Member(u, this.name, members.get(u));
            if (role == Role.LEADER)
                Leader = member;
            //Bukkit.getLogger().info("Loaded member: " + Bukkit.getOfflinePlayer(u).getName());
        }
        for (String s : chunks) {
            Chunk c = RegionHandler.getChunkFromString(s, true);
            RegionHandler.claimedChunks.put(c, this);
        }
        ResourceInterface.setContents(ClanResourceHandler.getInterfaceBase().getContents());
        updateResourceInterface();
    }
    
    private int Lvl;
    private int whoCanClaim, whoCanWithdraw, whoCanRecruit;
    private int hubX, hubY, hubZ;
    private String name, tag;
    private Set<String> chunks;
    private Set<UUID> requests = new HashSet<UUID>();
    private Map<UUID, Role> members;
    private Map<Material, Integer> resources;
    private Inventory ResourceInterface = Bukkit.createInventory(null, 18, "Clan Resources");
    
    private Member Leader;
    
    public int getLevel() {
        return Lvl;
    }
    
    public String getName() {
        return name;
    }
    
    public int getHubX() {
        return hubX;
    }
    
    public int getHubY() {
        return hubY;
    }
    
    public int getHubZ() {
        return hubZ;
    }
    
    public String getTag() {
        return "[" + tag + "]";
    }
    
    public String getRawTag() {
        return tag;
    }
    
    public Set<UUID> getMembers() {
        return members.keySet();
    }
    
    public Member getLeader() {
        return Leader;
    }
    
    public void setLeader(Member newLeader) {
        Leader = newLeader;
    }
    
    public boolean isMember(Player p) {
        return members.containsKey(p.getUniqueId());
    }
    
    public boolean inRegion(Location loc) {
        return chunks.contains(RegionHandler.getStringFromChunk(loc.getChunk()));
    }
    
    public boolean inRegion(Player p) {
        return chunks.contains(RegionHandler.getStringFromChunk(p.getLocation().getChunk()));
    }
    
    public Inventory getResourceInterface() {
        return ResourceInterface;
    }
    
    public int getWhoCanClaim() {
        return whoCanClaim;
    }
    
    public int getWhoCanWithdraw() {
        return whoCanWithdraw;
    }
    
    public int getWhoCanRecruit() {
        return whoCanRecruit;
    }
    
    public Set<String> getClaimedChunks() {
        return chunks;
    }
    
    public Set<Material> getResourceSet() {
        return resources.keySet();
    }
    
    public int getResourceAmount(Material resource) {
        return resources.containsKey(resource) ? resources.get(resource) : 0;
    }
    
    public boolean removeResourceAmount(Material resource, int amount) {
        if (!resources.containsKey(resource) || resources.get(resource) < amount)
            return false;
        int i = resources.get(resource);
        resources.remove(resource);
        resources.put(resource, i - amount);
        return true;
    }
    
    public void setRole(Member m, Role newrole) {
        if (members.containsKey(m.getID())) members.remove(m);
        members.put(m.getID(), newrole);
    }
     
    public boolean setWhoCanClaim(Member m, int i) {
        if (!m.getRole().equals(Role.LEADER)) return false;
        whoCanClaim = i;
        return true;
    }
    
    public boolean setWhoCanClaim(Member m, String s) {
        if (s.equalsIgnoreCase("member")) {
            return setWhoCanClaim(m, 3);
        }
        if (s.equalsIgnoreCase("captain")) {
            return setWhoCanClaim(m, 2);
        }
        if (s.equalsIgnoreCase("co-leader") || s.equalsIgnoreCase("co")) {
            return setWhoCanClaim(m, 1);
        }
        if (s.equalsIgnoreCase("leader")) {
            return setWhoCanClaim(m, 0);
        }
        //m.getPlayer().sendMessage(ChatColor.RED + "Format error: " + ChatColor.GRAY + "accepted values: Member/Captain/Co-Leader/Leader");
        return false;
    }
    
    public boolean setWhoCanRecruit(Member m, int i) {
        if (!m.getRole().equals(Role.LEADER)) return false;
        whoCanRecruit = i;
        return true;
    }
    
    public boolean setWhoCanRecruit(Member m, String s) {
        if (s.equalsIgnoreCase("member")) {
            return setWhoCanRecruit(m, 3);
        }
        if (s.equalsIgnoreCase("captain")) {
            return setWhoCanRecruit(m, 2);
        }
        if (s.equalsIgnoreCase("co-leader") || s.equalsIgnoreCase("co")) {
            return setWhoCanRecruit(m, 1);
        }
        if (s.equalsIgnoreCase("leader")) {
            return setWhoCanRecruit(m, 0);
        }
        //m.getPlayer().sendMessage(ChatColor.RED + "Format error: " + ChatColor.GRAY + "accepted values: Member/Captain/Co-Leader/Leader");
        return false;
    }
     
    public boolean setWhoCanWithdraw(Member m, int i) {
        if (!m.getRole().equals(Role.LEADER)) return false;
        whoCanWithdraw = i;
        return true;
    }
    
    public boolean setWhoCanWithdraw(Member m, String s) {
        if (s.equalsIgnoreCase("member")) {
            return setWhoCanWithdraw(m, 3);
        }
        if (s.equalsIgnoreCase("captain")) {
            return setWhoCanWithdraw(m, 2);
        }
        if (s.equalsIgnoreCase("co-leader") || s.equalsIgnoreCase("co")) {
            return setWhoCanWithdraw(m, 1);
        }
        if (s.equalsIgnoreCase("leader")) {
            return setWhoCanWithdraw(m, 0);
        }
        //m.getPlayer().sendMessage(ChatColor.RED + "Format error: " + ChatColor.GRAY + "accepted values: Member/Captain/Co-Leader/Leader");
        return false;
    }
    
    public Location getClanSpawn() {
        return hubY == 1337 ? null : new Location(Main.getOverworld(), hubX, hubY, hubZ);
    }
    
    public boolean attemptChangeClanSpawn(Member m) {
        if (m.getRole().getID() <= 1) {
            Location toset = m.getPlayer().getLocation();
            if (!RegionHandler.getClan(toset.getChunk()).equals(this)) {
                m.getPlayer().sendMessage(ChatColor.RED + "You must first claim this chunk before you can set your Clan's Hub.");
                return false;
            }
            hubX = (int) toset.getX();
            hubY = (int) toset.getY();
            hubZ = (int) toset.getZ();
            sendClanMessage("�7" + m.getPlayer().getName() + " has changed the Clan Hub to �f" + hubX + ", " + hubY + ", " + hubZ + "�7!");
            return true;
        }
        
        m.getPlayer().sendMessage(ChatColor.RED + "Only Co-Leaders and up may change the Clan Hub.");
        return false;
    }
    
    public boolean attemptRemoveClanSpawn() {
        hubX = 0;
        hubY = 1337;
        hubZ = 0;
        return true;
    }
    
    public Role getRole(OfflinePlayer p) {
        return members.containsKey(p.getUniqueId()) ? members.get(p.getUniqueId()) : Role.UNDEFINED;
    }

    public Role getRole(UUID id) {
        return members.containsKey(id) ? members.get(id) : Role.UNDEFINED;
    }
    
    public Role getRole(Member m) {
        return m == null ? Role.UNDEFINED : members.get(m.getID());
    }
    
    public void updateRole(UUID id) {
        if (members.containsKey(id))
            members.put(id, RegionHandler.getMember(id).getRole());
    }
    
    public boolean setTag(Member m, String newtag) {
        if (m.getRole().getID() <= 1) {
            if (newtag.length() > 4) {
                m.getPlayer().sendMessage(ChatColor.RED + "Clan tags can be 4 characters maximum.");
                return false;
            }
            if (RegionHandler.takenTags.contains(newtag)) {
                m.getPlayer().sendMessage(ChatColor.RED + "That clan tag is already taken!");
                return false;
            }
            RegionHandler.takenTags.remove(tag);
            RegionHandler.takenTags.add(newtag);
            tag = newtag;
            this.sendClanMessage(ChatColor.GRAY + m.getPlayer().getName() + " has changed the clan tag to �a[" + newtag + "]�2!");
            return true;
        } else {
            m.getPlayer().sendMessage(ChatColor.RED + "Only Co-Leaders and up can set the clan tag.");
            return false;
        }
    }
    
    public int getMaxChunks() {
        return Lvl + Math.min(RegionHandler.START_CLAIM_CHUNKS + ((int) (Math.floor(members.size() / RegionHandler.INCREASE_PER_MEMBER))), RegionHandler.MAX_CLAIM_CHUNKS);
    }
    
    public void addChunk(Member m, Chunk c) {
        Player p = m.getPlayer();
        if (m.getRole().getID() <= whoCanClaim) {
            if (chunks.isEmpty() || chunks.size() < getMaxChunks()) {
                if (RegionHandler.getClan(c) != null && RegionHandler.getClan(c).equals(this)) {
                    p.playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 1.0F, 1.0F);
                    p.sendMessage(ChatColor.RED + "Your Clan already has that chunk claimed.");
                    return;
                }
                if (chunks.isEmpty() || hasAdjacentClaim(this, c)) {
                    chunks.add(RegionHandler.getStringFromChunk(c));
                    RegionHandler.claimedChunks.put(c, this);
                    p.playSound(p.getLocation(), Sound.ENTITY_ARROW_HIT_PLAYER, 1.0F, 1.0F);
                    p.sendMessage("�9�lClan > �r�7This chunk now belongs to your clan!");
                    
                    if (chunks.isEmpty()) {
                        m.getClan().sendClanMessage(ChatColor.GRAY + m.getRole().getTag() + " " + m.getPlayer().getName() + " has started a Claim at chunk �f" + c.getBlock(0,0,0).getX() + "�7, �f" + c.getBlock(0,0,0).getZ() + "�7!");
                        attemptChangeClanSpawn(m);
                    } else
                        m.getClan().sendClanMessage(ChatColor.GRAY + m.getRole().getTag() + " " + m.getPlayer().getName() + " has claimed the chunk at �f" + c.getBlock(0,0,0).getX() + "�7, �f" + c.getBlock(0,0,0).getZ() + "�7!");
                    m.getClan().sendClanMessage(ChatColor.GRAY + "Your clan can claim another " + (m.getClan().getMaxChunks() - m.getClan().getClaimedChunks().size()) + " chunks.");
                } else {
                    p.playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 1.0F, 1.0F);
                    p.sendMessage(ChatColor.RED + "You may only claim chunks adjacent to existing ones!");
                }
            } else {
                p.playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 1.0F, 1.0F);
                p.sendMessage(ChatColor.RED + "Your clan already has the maximum number of chunks claimed!");
            }
        } else {
            p.playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 1.0F, 1.0F);
            p.sendMessage(ChatColor.RED + "You are not authorized to claim or unclaim land!");
        }
    }
    
    private static boolean hasAdjacentClaim(Clan owner, Chunk c) {
        World w = c.getWorld();
        return RegionHandler.getClan(w.getChunkAt(c.getX() + 1, c.getZ())) == owner ||
                RegionHandler.getClan(w.getChunkAt(c.getX() - 1, c.getZ())) == owner ||
                RegionHandler.getClan(w.getChunkAt(c.getX(), c.getZ() + 1)) == owner ||
                RegionHandler.getClan(w.getChunkAt(c.getX(), c.getZ() - 1)) == owner;
    }
    
    public void removeChunk(Member m, Chunk c) {
        Player p = m.getPlayer();
        if (m.getRole().getID() <= whoCanClaim) {
            if (RegionHandler.getClan(c).equals(this)) {
                chunks.remove(RegionHandler.getStringFromChunk(c));
                RegionHandler.claimedChunks.remove(c);
                m.getClan().sendClanMessage(ChatColor.GRAY + m.getRole().getTag() + " " + m.getPlayer().getName() + " has unclaimed the chunk at �c" + c.getBlock(0,0,0).getX() + "�7, �c" + c.getBlock(0,0,0).getZ() + "�7!");
                m.getClan().sendClanMessage(ChatColor.GRAY + "Your Clan may now claim another " + (m.getClan().getMaxChunks() - m.getClan().getClaimedChunks().size()) + " chunks.");
                if ((hubZ == 0 && hubX == 0 && hubY == 1337) || c.equals(getClanSpawn().getChunk())) {
                    attemptRemoveClanSpawn();
                    m.getClan().sendClanMessage(ChatColor.GRAY + "Your Clan Hub has been �cdeleted �7with the chunk!");
                }
            } else {
                p.playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 1.0F, 1.0F);
                p.sendMessage(ChatColor.RED + "This chunk does not belong to your Clan!");
            }
        } else {
            p.playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 1.0F, 1.0F);
            p.sendMessage(ChatColor.RED + "You are not authorized to claim or unclaim land!");
        }
    }
    
    public void removeAllChunks(Member m) {
        Player p = m.getPlayer();
        if (m.getRole().getID() <= 1) {
            if (!chunks.isEmpty()) {
                attemptRemoveClanSpawn();
                for (String s : chunks) {
                    RegionHandler.removeClaimedChunk(s);
                }
                chunks.clear();
                sendClanMessage(ChatColor.GRAY + m.getRole().getTag() + " " + m.getPlayer().getName() + " has unclaimed all chunks!");
                attemptRemoveClanSpawn();
                sendClanMessage(ChatColor.GRAY + "Your Clan Hub has been �cdeleted �7with the chunk!");
                sendClanMessage(ChatColor.GRAY + "Your clan may now claim " + (m.getClan().getMaxChunks() - m.getClan().getClaimedChunks().size()) + " chunks.");
            } else {
                p.playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 1.0F, 1.0F);
                p.sendMessage(ChatColor.RED + "Your Clan has no claimed chunks!");
            }
        } else {
            p.playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 1.0F, 1.0F);
            p.sendMessage(ChatColor.RED + "Only Co-Leaders and up may use this command!");
        }
    }
    
    public boolean attemptJoin(OfflinePlayer joiner) {
        if (RegionHandler.members.containsKey(joiner.getUniqueId())) {
            if (joiner.isOnline()) {
                joiner.getPlayer().sendMessage(ChatColor.GRAY + "You are already in a clan. Use �c/clan leave�4 to leave your current one.");
                joiner.getPlayer().playSound(joiner.getPlayer().getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 1.0F, 1.0F);
            }
            return false;
        }
        if (joiner.isOnline() && !Main.getInviteHandler().getInviteClan(joiner.getPlayer()).equals(this)) {
            Main.getInviteHandler().handleInviteResponse(joiner.getPlayer(), InviteAction.JOINED_ANOTHER_CLAN);
            return false;
        }
        new Member(joiner.getUniqueId(), name, Role.RECRUIT);
        return true;
    }
    
    public void attemptPromote(Member promoter, Member m) {
        int currentlevel = m.getRole().getID();
        int promoterlevel = promoter.getRole().getID();
        Player p = promoter.getPlayer();
        Player p2 = m.getPlayer();
        if (m.getRole().equals(Role.CO_LEADER)) {
            p.sendMessage(ChatColor.GRAY + "You cannot promote to Leader.");
            p.playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 1.0F, 1.0F);
            return;
        }
        if (promoterlevel < currentlevel - 1) {
            Role newrole = Role.getRole(currentlevel - 1);
            m.setRole(newrole);
            sendClanMessage(ChatColor.GREEN + p2.getName() + " " + ChatColor.DARK_GREEN + "has been promoted to " + ChatColor.GREEN + newrole.getName() + ChatColor.DARK_GREEN + " by " + promoter.getChatName() + "!");
            p2.sendMessage(ChatColor.BLUE + "Clan > �7You've been promoted to �a" + newrole.getName() + "�7!");
        } else {
            p.sendMessage(ChatColor.GRAY + "You are not ranked high enough to promote that player.");
            p.playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 1.0F, 1.0F);
        }
    }
    
    public void attemptDemote(Member demoter, Member m) {
        int currentlevel = m.getRole().getID();
        int demoterlevel = demoter.getRole().getID();
        Player p = demoter.getPlayer();
        Player p2 = m.getPlayer();
        if (m.getRole().equals(Role.RECRUIT)) {
            p.sendMessage(ChatColor.GRAY + "You cannot demote that player any further, but you can kick him/her with �c/clan kick [name]�7.");
            p.playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 1.0F, 1.0F);
            return;
        }
        if (demoterlevel < currentlevel && demoterlevel <= 2) {
            Role newrole = Role.getRole(currentlevel + 1);
            m.setRole(newrole);
            sendClanMessage(ChatColor.RED + p2.getName() + " " + ChatColor.DARK_RED + "has been demoted to " + ChatColor.RED + newrole.getName() + ChatColor.DARK_RED + " by " + demoter.getChatName() + "!");
            p2.sendMessage(ChatColor.BLUE + "Clan > �7You've been demoted to �c" + newrole.getName() + "�7!");
        } else {
            p.sendMessage(ChatColor.GRAY + "Only Captain Rank and up can promote or demote.");
            p.playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 1.0F, 1.0F);
        }
    }
    
    public void attemptKick(Member kicker, Member kicked) {
        int currentlevel = kicked.getRole().getID();
        int promoterlevel = kicker.getRole().getID();
        Player p = kicker.getPlayer();
        Player p2 = kicked.getPlayer();
        if (promoterlevel < currentlevel && promoterlevel <= 2) {
            members.remove(kicked.getID());
            RegionHandler.members.remove(kicked.getID());
            kicked.setRole(null);
            kicked.setClan(null);
            sendClanMessage(ChatColor.RED + p2.getName() + " " + ChatColor.DARK_RED + "has been kicked by " + kicker.getChatName() + "!");
            p2.sendMessage(ChatColor.BLUE + "Clan > �7You've been kicked out of the clan!");
        } else {
            p.sendMessage(ChatColor.RED + "You are not ranked high enough to kick this player.");
            p.playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 1.0F, 1.0F);
        }
    }
    
    public void attemptLeave(Member leaver) {
        sendClanMessage(ChatColor.RED + leaver.getChatName() + " " + ChatColor.DARK_RED + "has left the clan!");
        leaver.getPlayer().sendMessage(ChatColor.BLUE + "Clan > �7You've left the clan!");
        if (leaver.getRole().equals(Role.LEADER)) {
            boolean coLeader = false;
            if (members.size() > 0) {
                for (UUID u : members.keySet()) {
                    if (members.get(u) == Role.CO_LEADER) {
                        Member m = RegionHandler.getMember(u);
                        setLeader(m);
                        m.setRole(Role.LEADER);
                        setRole(m, Role.LEADER);
                        coLeader = true;
                        sendClanMessage(ChatColor.YELLOW + m.getChatName() + " " + ChatColor.GREEN + "is now the new Leader!");
                        break;
                    }
                }
            }
            if (!coLeader) destroy();
        }
        members.remove(leaver.getID());
        RegionHandler.members.remove(leaver.getID());
        leaver.setRole(null);
        leaver.setClan(null);
    }
    
    public void sendJoinRequest(Player p) {
        UUID id = p.getUniqueId();
        if (requests.contains(id)) {
            p.sendMessage(ChatColor.RED + "You have already sent a Join Request. Be patient!");
            p.playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 1.0F, 1.0F);
            return;
        }
        if (RegionHandler.members.containsKey(p.getUniqueId()) && members.containsKey(p.getUniqueId())) {
            p.sendMessage(ChatColor.GRAY + "You are already in this clan. Applying again would be pointless.");
            p.playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 1.0F, 1.0F);
            return;
        }
        requests.add(id);
        p.sendMessage(ChatColor.GREEN + "Successfully sent a Join Request to " + name + " Clan.");
        p.playSound(p.getLocation(), Sound.BLOCK_NOTE_PLING, 1.0F, 1.4F);
        this.sendClanMessage(ChatColor.WHITE + p.getName() + ChatColor.GRAY + " wants to join your Clan! Use �f/clan accept/deny " + p.getName() + " �7to respond.");
    }
    
    public void handleJoinRequest(Member handler, OfflinePlayer requester, boolean accepted) {
        if (handler.getRole().getID() > whoCanRecruit) {
            handler.getPlayer().sendMessage(ChatColor.BLUE + "Clan > �7Only Rank " + Main.capitalizeFirst(Role.getRole(whoCanRecruit).toString().replace("_", "-")) + " and above can handle Join Requests.");
            handler.getPlayer().playSound(handler.getPlayer().getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 1.0F, 1.0F);
            return;
        }
        if (!requests.contains(requester.getUniqueId())) {
            handler.getPlayer().sendMessage(ChatColor.GRAY + requester.getName() + " has not sent a Join Request to your clan.");
            handler.getPlayer().playSound(handler.getPlayer().getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 1.0F, 1.0F);
            return;
        }
        if (accepted) {
            if (attemptJoin(requester))
                this.sendClanMessage(ChatColor.GREEN + requester.getName() + " " + ChatColor.GRAY + "been accepted into the clan by " + handler.getChatName() + "!");
            else
                this.sendClanMessage(ChatColor.RED + requester.getName() + " " + ChatColor.GRAY + "is already in a Clan and cannot join yours.");
        } else {
            this.sendClanMessage(ChatColor.GRAY + requester.getName() + " has been denied entry into the Clan by " + handler.getChatName() + ".");
            if (requester.isOnline())
                requester.getPlayer().sendMessage(ChatColor.RED + "Your Join Request to " + name + " Clan has been rejected.");
        }
        requests.remove(requester.getUniqueId());
    }
    
    public void sendClanMessage(String msg) {
        for (UUID u : members.keySet()) {
            if (Bukkit.getOfflinePlayer(u).isOnline()) {
                Player p = Bukkit.getPlayer(u);
                p.sendMessage(ChatColor.BLUE + "" + ChatColor.BOLD + "Clan >> " + ChatColor.RESET + msg);
                p.playSound(p.getLocation(), Sound.BLOCK_NOTE_PLING, 1.0F, 1.6F);
            }
        }
    }    
    
    public void sendClanMessageNoPrefix(String msg) {
        for (UUID u : members.keySet()) {
            if (Bukkit.getOfflinePlayer(u).isOnline()) {
                Player p = Bukkit.getPlayer(u);
                p.sendMessage(msg);
            }
        }
    }
    
    public void sendClanMessage(Member sender, String msg) {
        for (UUID u : members.keySet()) {
            if (Bukkit.getOfflinePlayer(u).isOnline()) {
                Player p = Bukkit.getPlayer(u);
                p.sendMessage(ChatColor.BLUE + "" + ChatColor.BOLD + "Clan > �r�b" + sender.getChatName() + " " + ChatColor.GRAY + msg);
                p.playSound(p.getLocation(), Sound.BLOCK_NOTE_PLING, 1.0F, 1.8F);
            }
        }
    }
    
    public void playClanSound(Sound sound, float volume, float pitch) {
        for (UUID u : members.keySet()) {
            if (Bukkit.getOfflinePlayer(u).isOnline()) {
                Player p = Bukkit.getPlayer(u);
                p.playSound(p.getLocation(), sound, volume, pitch);
            }
        }
    }
    
    public void displayJoinRequests(Player viewer) {
        int i = 0;
        viewer.sendMessage(ChatColor.WHITE + "" + ChatColor.STRIKETHROUGH + "=================================");
        viewer.sendMessage(ChatColor.GREEN + "" + ChatColor.UNDERLINE + "Pending Clan Requests:");
        for (Iterator<UUID> it = requests.iterator(); it.hasNext();) {
            i++;
            viewer.sendMessage(ChatColor.GREEN + Integer.toString(i) + " " + ChatColor.DARK_GREEN + Bukkit.getOfflinePlayer(it.next()).getName());
            if (i == 12) {
                viewer.sendMessage(ChatColor.WHITE + "And " + (requests.size() - 12) + " more...");
                break;
            }
        }
        viewer.sendMessage(ChatColor.GRAY + "Accept or deny requests with �f/clan accept/deny [name]�7.");
    }
    
    public void displayInvites(Player viewer) {
        int i = 0;
        viewer.sendMessage(ChatColor.WHITE + "" + ChatColor.STRIKETHROUGH + "=================================");
        viewer.sendMessage(ChatColor.GREEN + "" + ChatColor.UNDERLINE + "Sent Clan Invites:");
        for (Iterator<Player> it = Main.getInviteHandler().getClanInvites(this).iterator(); it.hasNext();) {
            i++;
            viewer.sendMessage(ChatColor.GREEN + Integer.toString(i) + " " + ChatColor.DARK_GREEN + it.next().getName());
            if (i == 12) {
                viewer.sendMessage(ChatColor.WHITE + "And " + (requests.size() - 12) + " more...");
                break;
            }
        }
        viewer.sendMessage(ChatColor.GRAY + "Cancel invites with �f/clan cancel [name]�7.");
    }
    
    public void displayClanInfo(Player viewer) {
        viewer.sendMessage(ChatColor.WHITE + "" + ChatColor.STRIKETHROUGH + "=================================");
        viewer.sendMessage(ChatColor.WHITE + "�lInfo for �b�l" + name + "�f�l Clan �9[Lvl " + getLevel() + "]�f�l:");
        viewer.sendMessage("�9Clan Tag: �7" + getTag());
        viewer.sendMessage("�9Clan Leader: �7" + Leader.getOfflinePlayer().getName());
        viewer.sendMessage("�9Clan Members: �7" + (members.size() - 1));
        viewer.sendMessage("�9Recruitment Rank: �7" + Main.capitalizeFirst(Role.getRole(whoCanRecruit).toString()).replace("_", "-"));
        viewer.sendMessage("�9Land Claim Rank: �7" + Main.capitalizeFirst(Role.getRole(whoCanClaim).toString()).replace("_", "-"));
        viewer.sendMessage("�9Resource Rank: �7" + Main.capitalizeFirst(Role.getRole(whoCanWithdraw).toString()).replace("_", "-"));
    }
    
    public void displayClanMembers(Player viewer, int page) {
        int i = (page - 1) * 12;
        int ii = i;
        viewer.sendMessage(ChatColor.WHITE + "" + ChatColor.STRIKETHROUGH + "=================================");
        viewer.sendMessage(ChatColor.AQUA + "�l" + name + " �f�lClan Members:");
        for (Iterator<UUID> it = members.keySet().iterator(); it.hasNext();) {
            i++;
            OfflinePlayer op = Bukkit.getOfflinePlayer(it.next());
            if (i >= ii) viewer.sendMessage(ChatColor.BLUE + Integer.toString(i) + ") " + (op.isOnline() ? ChatColor.GREEN : ChatColor.GRAY) + RegionHandler.getMember(op).getChatName());
            if (i >= page + 12) {
                viewer.sendMessage(ChatColor.GRAY + "(Page " + page + "/" + (int) (Math.ceil((members.size() + 1) / 12)));
                break;
            }
        };
    }
    
    public void displayChunksLeft(Player viewer) {
        viewer.sendMessage(ChatColor.GRAY + "Your Clan can claim " + (getMaxChunks() - chunks.size()) + " more chunks.");
        viewer.playSound(viewer.getLocation(), Sound.BLOCK_NOTE_PLING, 1.0F, 0.8F);
    }
    
    public void attemptLevelUp(Member user) {
        Player p = user.getPlayer();
        if (user.getRole().getID() > whoCanClaim) {
            p.sendMessage("�7Only those who can claim land can attempt a Clan Upgrade!");
            p.playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 1.0F, 1.0F);
            return;
        }
        if (Lvl >= 15) {
            p.sendMessage("�7Your Clan is already at the Maximum Level!");
            p.playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 1.0F, 1.0F);
            return;
        }
        if (resources.isEmpty()) {
            p.sendMessage("�7Your Clan has no resources! Use �f/clan resources�7 to check.");
            p.playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 1.0F, 1.0F);
            return;
        }
        
        if (ClanPerks.attemptUpgradeClan(this, Lvl + 1, resources)) {
            Lvl += 1;
            playClanSound(Sound.ENTITY_ZOMBIE_VILLAGER_CONVERTED, 1.0F, 1.4F);
            sendClanMessage("The Clan has been upgraded to �aLevel " + Lvl + "�f!");
            sendClanMessageNoPrefix("�6�l NEW �e+1 Extra Chunk Claim");
            sendClanMessageNoPrefix("�6�l NEW �e" + ClanPerks.getPerkString(Lvl));
            updateResourceInterface();
            
            for (UUID id : members.keySet()) {
                if (Bukkit.getServer().getPlayer(id) != null)
                    Utilities.launchFirework(Bukkit.getServer().getPlayer(id));
            }
            
        } else {
            p.sendMessage(ChatColor.GRAY + "Not enough resources! Use �f/clan resources�7 to check.");
            p.playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 1.0F, 1.0F);
        }
    }
    
    public void updateResourceInterface() {
        Set<Material> nextLevel = ClanPerks.nextLevelRequirements(Lvl);
        for (ItemStack i : ResourceInterface.getContents()) {
            if (i == null || i.getType() == Material.AIR)
                continue;
            int amt = getResourceAmount(i.getType());
            ItemMeta meta = i.getItemMeta();
            String name = meta.getDisplayName() != null ? ChatColor.stripColor(meta.getDisplayName()) : "penis:penis";
            String[] nameSplit = name.split(":");
            meta.setDisplayName(ChatColor.RED + nameSplit[0] + ": �7" + amt + (nextLevel.contains(i.getType()) ? " �4/" + ClanPerks.getUpgradeItemAmount(Lvl, i.getType()) : ""));
            i.setItemMeta(meta);
            i.setAmount(Math.min(64, Math.max(1, amt)));
        }
    }
    
    public void updateResourceInterface(int slot) {
        ItemStack i = ResourceInterface.getItem(slot);
        int amt = getResourceAmount(i.getType());
        ItemMeta meta = i.getItemMeta();
        String name = meta.getDisplayName() != null ? ChatColor.stripColor(meta.getDisplayName()) : "penis:penis";
        String[] nameSplit = name.split(":");
        String[] slashSplit = nameSplit[1].split("/");
        meta.setDisplayName(ChatColor.RED + nameSplit[0] + ": �7" + amt + (slashSplit.length > 1 ? " �4/" + slashSplit[1] : ""));
        i.setItemMeta(meta);
        i.setAmount(Math.min(64, Math.max(1, amt)));
    }
    
    public void withdrawResource(Player p, int clickslot, Material mat, int amount) {
        if (amount <= 0) return;
        if (getRole(p).getID() > whoCanWithdraw) {
            p.sendMessage("�7You do not have permission to withdraw resources!");
            p.playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 0.4F, 1.0F);
            return;
        }
        if (removeResourceAmount(mat, amount)) {
            Utils.giveItem(p, new ItemStack(mat, amount));
            p.sendMessage("�7Withdrew �e" + amount + " �7" + mat.toString().replace("_", " ") + ".");
            p.playSound(p.getLocation(), Sound.BLOCK_NOTE_PLING, 0.4F, 1.2F);
            updateResourceInterface(clickslot);
        } else {
            if (amount == 1) {
                p.sendMessage("�cERROR: �7Your Clan does not have " + amount + " �7" + mat.toString().replace("_", " ") + "!");
                p.playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 0.4F, 1.0F);
            } else {
                withdrawResource(p, clickslot, mat, getResourceAmount(mat));
            }
        }
    }
    
    public void depositResource(Player p, int clickslot, Material mat, int amount) {
        if (amount <= 0) return;
        if (p.getInventory().containsAtLeast(new ItemStack(mat), amount)) {
            p.getInventory().removeItem(new ItemStack(mat, amount));
            int have = getResourceAmount(mat);
            resources.remove(mat);
            resources.put(mat, have + amount);
            p.sendMessage("�7Deposited �e" + amount + " �7" + mat.toString().replace("_", " ") + ".");
            p.playSound(p.getLocation(), Sound.BLOCK_NOTE_PLING, 0.4F, 1.2F);
            updateResourceInterface(clickslot);
        } else {
            if (amount == 1) {
                p.sendMessage("�cERROR: �7You do not have " + amount + " �7" + mat.toString().replace("_", " ") + "!");
                p.playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 0.4F, 1.0F);
            } else {
                depositResource(p, clickslot, mat, Utilities.getItemAmount(p, mat, (short) 0));
            }
        }
    }
    
    public void destroy() {
        sendClanMessage(ChatColor.DARK_RED + "The clan has been disbanded because there was no Leaders or Co-Leaders!");
        for (UUID u : members.keySet()) {
            Member m = RegionHandler.getMember(u);
            m.setClan(null);
            m.setRole(null);
        }
        for (String s : chunks) {
            RegionHandler.removeClaimedChunk(s);
        }
        RegionHandler.claimNames.remove(name.toLowerCase());
        RegionHandler.existingClans.remove(this);
        new File("plugins" + File.separator + "ProjectZ" + File.separator + name + ".yml").delete();
    }
    
}
