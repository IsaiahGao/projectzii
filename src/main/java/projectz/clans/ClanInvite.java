package projectz.clans;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.WeakHashMap;

import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import projectz.Main;

public class ClanInvite {
    
    private Map<Player, Clan> InviteMap = new WeakHashMap<Player, Clan>();
    private Map<UUID, BukkitTask> InviteTimers = new HashMap<UUID, BukkitTask>();
    
    public static enum InviteAction {
        ACCEPTED,
        DENIED,
        DENIED_WITH_PASSION,
        DISCONNECTED,
        CLAN_NULLED,
        JOINED_ANOTHER_CLAN,
        ALREADY_IN_CLAN,
        NOT_INVITED,
        CANCELLED
    }
    
    public Set<Player> getClanInvites(Clan c) {
        Set<Player> set = new HashSet<Player>();
        for (Player p : InviteMap.keySet()) {
            if (InviteMap.get(p).equals(c))
                set.add(p);
        }
        return set;
    }
    
    public boolean hasPendingInvitation(Player p) {
        return InviteMap.containsKey(p);
    }
    
    public Clan getInviteClan(Player p) {
        return InviteMap.get(p);
    }
    
    public boolean sendClanInvite(final OfflinePlayer receiver, final Member sender) {
        if (!receiver.isOnline()) {
            sender.sendMessage("�9�lClan > �fThat player is not online!");
            return false;
        }
        if (RegionHandler.getClan(receiver) != null) {
            sender.sendMessage("�9�lClan > �fThat player is already in a Clan!");
            return false;
        }
        if (InviteMap.containsKey(receiver)) {
            sender.sendMessage("�9�lClan > �fThat player already has a pending Invitation!");
            return false;
        }
        final Clan c = sender.getClan();
        final Player Preceiver = receiver.getPlayer();
        c.sendClanMessage("�f" + sender.getRole().getTag() + " " + sender.getPlayer().getName() + " �7has sent a Clan Invitation to �f" + receiver.getName());
        Preceiver.sendMessage("�9�lClan > �fYou have been invited to join �e" + c.getName() + " Clan�f!");
        Preceiver.sendMessage("�9�lClan > �fUse �e/invite accept/deny�f to answer. Request will expire after 3 minutes.");
        Preceiver.playSound(Preceiver.getLocation(), Sound.BLOCK_NOTE_PLING, 0.6F, 1.2F);
        InviteMap.put(Preceiver, c);
        InviteTimers.put(Preceiver.getUniqueId(),
        new BukkitRunnable() {
            public void run() {
                if (Preceiver.isOnline()) {
                    Preceiver.sendMessage("�9�lClan > �7Invitation from �f" + c.getName() + " Clan �7has expired.");
                    Preceiver.playSound(Preceiver.getLocation(), Sound.BLOCK_NOTE_PLING, 0.6F, 0.7F);
                }
                if (c != null) {
                    c.sendClanMessage("�7Invitation to �f" + receiver.getName() + "�7 has expired!");
                }
                InviteMap.remove(Preceiver);
                InviteTimers.remove(Preceiver.getUniqueId());
            }
        }.runTaskLater(Main.getPlugin(), 3600L));
        
        return true;
    }
    
    public void handleInviteResponse(Player p, InviteAction action) {
        Clan c = InviteMap.get(p);
        
        switch (action) {
        case ACCEPTED:
            c.attemptJoin(p);
            c.sendClanMessage("�f" + p.getName() + "�a joined the Clan!");
            break;
        case DENIED:
            c.sendClanMessage("�f" + p.getName() + "�c rejected your Clan's Invitation!");
            p.sendMessage("�7You denied �c" + c.getName() + " Clan's �7Invitation.");
            break;
        case DENIED_WITH_PASSION:
            c.sendClanMessage("�f" + p.getName() + "�c forcefully rejected your Clan's Invitation!");
            c.sendClanMessage("�fHe/She must be super passionate about NOT joining your Clan.");
            p.sendMessage("�7You said a big �lFUCK NO �rto �c" + c.getName() + " Clan's �7Invitation.");
            break;
        case DISCONNECTED:
            c.sendClanMessage("�f" + p.getName() + "�7 logged out, so its Invite was cancelled!");
            break;
        case CLAN_NULLED:
            p.sendMessage("�7That Clan did not exist! Perhaps it disbanded?");
            break;
        case CANCELLED:
            c.sendClanMessage("�f" + p.getName() + "�7's Invitation was revoked!");
            p.sendMessage("�9�lClan > �7Your Invitation to �c" + c.getName() + " Clan �7was revoked!");
            break;
        case JOINED_ANOTHER_CLAN:
            c.sendClanMessage("�f" + p.getName() + "�7 joined another clan, so its Invite was cancelled!");
            break;
        case ALREADY_IN_CLAN:
            p.sendMessage("�7You are already in a Clan.");
            break;
        case NOT_INVITED:
            p.sendMessage("�7You have no pending Clan Invite.");
            return;
        }
        
        if (InviteTimers.containsKey(p.getUniqueId())) {
            InviteTimers.get(p.getUniqueId()).cancel();
            InviteTimers.remove(p.getUniqueId());
        }
        InviteMap.remove(p);
    }

}
