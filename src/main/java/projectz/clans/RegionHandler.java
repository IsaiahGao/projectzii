package projectz.clans;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import projectz.Main;

public class RegionHandler {
    
    public static int START_CLAIM_CHUNKS, MAX_CLAIM_CHUNKS, INCREASE_PER_MEMBER;
    
    public static Set<Clan> existingClans = new HashSet<Clan>();
    public static Set<String> takenTags = new HashSet<String>();
    public static Map<String, Clan> claimNames = new HashMap<String, Clan>();
    public static Map<Chunk, Clan> claimedChunks = new HashMap<Chunk, Clan>();
    public static Map<String, Clan> claimedUnloadedChunks = new HashMap<String, Clan>();
    public static Map<UUID, Member> members = new HashMap<UUID, Member>();
    
    public static void createRegionClaim(Player claimer, String name) {
        if (name.length() > 15) {
            claimer.sendMessage(ChatColor.RED + "Name too long! Can be 15 characters at most.");
            return;
        }
            Location loc = claimer.getLocation();
            if (!members.containsKey(claimer.getUniqueId())) {
                claimer.sendMessage(ChatColor.RED + "You must first create a Clan before you may claim.");
                claimer.playSound(claimer.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 1.0F, 1.0F);
                return;
            }
            if (claimedChunks.containsKey(loc.getChunk())) {
                if (claimedChunks.get(loc.getChunk()).getName().equals(members.get(claimer.getUniqueId()).getClan().getName()))
                    claimer.sendMessage(ChatColor.YELLOW + "Your clan has already claimed this chunk.");
                else
                    claimer.sendMessage(ChatColor.RED + "This chunk is already claimed.");
                claimer.playSound(claimer.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 1.0F, 1.0F);
                return;
            }
            if (!loc.getWorld().getName().equals(Main.getOverworld().getName())) {
                claimer.sendMessage(ChatColor.RED + "You may not claim outside the Overworld.");
                claimer.playSound(claimer.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 1.0F, 1.0F);
                return;
            }
            Member m = members.get(claimer.getUniqueId());
            Clan clan = m.getClan();
            clan.addChunk(m, loc.getChunk());
    }
    
    public static Clan getClan(Chunk c) {
        return claimedChunks.containsKey(c) ? claimedChunks.get(c) : null;
    }
    
    public static void unloadChunk(Chunk c) {
        claimedUnloadedChunks.put(c.getX() + "," + c.getZ(), claimedChunks.get(c));
    }
    
    public static void reloadChunk(Chunk c) {
        String coords = c.getX() + "," +  c.getZ();
        Clan clan = claimedUnloadedChunks.remove(coords);
        if (clan != null) claimedChunks.put(c, clan);
    }
    
    public static Clan getClan(String name) {
        return claimNames.containsKey(name.toLowerCase()) ? claimNames.get(name.toLowerCase()) : null;
    }
    
    public static Clan getClan(OfflinePlayer p) {
        return getMember(p.getUniqueId()) == null ? null : RegionHandler.getMember(p.getUniqueId()).getClan();
    }
    
    public static Member getMember(OfflinePlayer p) {
        UUID id = p.getUniqueId();
        return members.containsKey(id) ? members.get(id) : null;
    }

    public static Member getMember(UUID id) {
        return members.containsKey(id) ? members.get(id) : null;
    }
    
    public static boolean isClaimed(Chunk c) {
        return claimedChunks.containsKey(c);
    }
    
    public static Chunk getChunkFromString(String s, boolean load) {
        String[] ss = s.split(",");
        if (load) Main.getOverworld().loadChunk(Integer.parseInt(ss[0]), Integer.parseInt(ss[1]));
        return Main.getOverworld().getChunkAt(Integer.parseInt(ss[0]), Integer.parseInt(ss[1]));
    }
    
    public static void removeClaimedChunk(String s) {
        Chunk c = getChunkFromString(s, false);
        if (c.isLoaded())
            claimedChunks.remove(c);
        else
            claimedUnloadedChunks.remove(s);
    }
    
    public static String getStringFromChunk(Chunk c) {
        return c.getX() + "," + c.getZ();
    }
}
