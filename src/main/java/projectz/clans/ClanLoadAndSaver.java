package projectz.clans;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

import projectz.clans.Clan.Role;

import com.google.common.io.Files;

public class ClanLoadAndSaver {
    
    //private static File f = new File("plugins" + File.separator + "ProjectZ" + File.separator + "members.yml");
    //private static YamlConfiguration cf = YamlConfiguration.loadConfiguration(f);

    
    public static void loadClans() {
        File clanDirectory = new File("plugins" + File.separator + "ProjectZ" + File.separator + "clans");
        File clanBackupDirectory = new File("plugins" + File.separator + "ProjectZ" + File.separator + "clan_backup");
        if (!clanDirectory.exists()) {
            clanDirectory.mkdirs();
        }
        if (!clanBackupDirectory.exists()) {
            clanBackupDirectory.mkdirs();
        }
        
        for (File f : clanDirectory.listFiles()) {
            if (f.getName().endsWith(".yml")) {
                try {
                    YamlConfiguration cf = YamlConfiguration.loadConfiguration(f);
                    int ID = cf.getInt("Level"), CP = cf.getInt("Claim-Permission"), WP = cf.getInt("Withdraw-Permission"), RP = cf.getInt("Recruit-Permission");
                    String name = f.getName().replace(".yml", ""), tag = cf.getString("Tag");
                    
                    Set<String> CC = new HashSet<String>();
                    CC.addAll(cf.getStringList("Claimed-Chunks"));
                    
                    String hub = cf.getString("Clan-Hub");
                    int x, y, z;
                    if (hub.equals("none")) {
                        x = 0;
                        y = 1337;
                        z = 0;
                    } else {
                        String[] hc = hub.replace(".0", "").split(",");
                        x = Integer.parseInt(hc[0]);
                        y = Integer.parseInt(hc[1]);
                        z = Integer.parseInt(hc[2]);
                    }
                    
                    Map<Material, Integer> RS = new HashMap<Material, Integer>();
                    //ConfigurationSection Resources = cf.getConfigurationSection("Resources");
                    for (String s : cf.getStringList("Resources")) {
                        String[] resource = s.split(":");
                        RS.put(Material.valueOf(resource[0].toUpperCase()), Integer.parseInt(resource[1]));
                    }
                    
                    Map<UUID, Role> MR = new HashMap<UUID, Role>();
                    ConfigurationSection Members = cf.getConfigurationSection("Members");
                    for (String s : Members.getKeys(false))
                        MR.put(UUID.fromString(s), Role.getRole(Members.getInt(s)));
                    
                    new Clan(ID, name, tag, CP, WP, RP, CC, MR, RS, x, y, z);
                    
                    Bukkit.getLogger().info("Loaded clan: " + name);
                    Bukkit.getLogger().info("Backed up clan: " + name);
                    Files.copy(f, new File("plugins" + File.separator + "ProjectZ" + File.separator + "clan_backup" + File.separator + f.getName()));
                } catch (Exception e) {
                    //f.delete();
                    e.printStackTrace();
                    Bukkit.getLogger().info(f.getName() + " is invalid!");
                }
            }
        }
        //loadMembers();
    }
    
    /*public static void loadMembers() {
        Set<String> members = cf.getKeys(false);
        for (String s : members) {
            String clanname = cf.getString(s);
                if (clanname.equals("none")) {
                    Bukkit.getLogger().info("Deleting Member #" + s + " as it is clanless.");
                    f.delete();
                } else {
                    new Member(UUID.fromString(s), clanname, false);
                    Bukkit.getLogger().info("Successfully loaded Member #" + s + "!");
                }
        }
    }*/
    
    public static void saveClans() {
        for (Clan c : RegionHandler.existingClans) {
            if ( c == null || c.getName() == null) return;
            File clanfile = new File("plugins" + File.separator + "ProjectZ" + File.separator + "clans" + File.separator + c.getName() + ".yml");
            if (!clanfile.exists()) {
                if (c.getMembers().isEmpty()) {
                    Bukkit.getLogger().info("Clan " + c.getName() + " was not saved because it is empty.");
                    return;
                }
                try {
                    clanfile.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                if (c.getMembers().isEmpty()) {
                    clanfile.delete();
                    Bukkit.getLogger().info("Clan " + c.getName() + " was deleted because it is empty.");
                    return;
                }
            }
                YamlConfiguration cf = YamlConfiguration.loadConfiguration(clanfile);
                cf.set("Level", c.getLevel());
                cf.set("Tag", c.getTag().replace("[","").replace("]",""));
                cf.set("Claim-Permission", c.getWhoCanClaim());
                cf.set("Withdraw-Permission", c.getWhoCanWithdraw());
                cf.set("Recruit-Permission", c.getWhoCanRecruit());
                
                String hub = "";
                if (c.getHubY() == 1337)
                    hub = "none";
                else
                    hub += c.getHubX() + "," + c.getHubY() + "," + c.getHubZ();
                cf.set("Clan-Hub", hub);
                
                List<String> chunkz = new ArrayList<String>();
                for (String s : c.getClaimedChunks())
                    chunkz.add(s);
                cf.set("Claimed-Chunks", chunkz);
                
                List<String> resources = new ArrayList<String>();
                for (Material resource : c.getResourceSet()) {
                    if (c.getResourceAmount(resource) > 0)
                        resources.add(resource.toString() + ":" + c.getResourceAmount(resource));
                }
                cf.set("Resources", resources);
                
                for (UUID u : c.getMembers())
                    cf.set("Members." + u, RegionHandler.getMember(u).getRole().getID());
                
                clanfile.delete();
                try {
                    cf.save("plugins" + File.separator + "ProjectZ" + File.separator + "clans" + File.separator + c.getName() + ".yml");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Bukkit.getLogger().info("Saving clan: " + c.getName());
        }
    }
    
    public static void saveMembers() {
        /*for (Member m : RegionHandler.members.values()) {
            String id = m.getID().toString();
            String clanname = m.getClan() == null ? "none" : m.getClan().getName();
            if (clanname.equals("none")) {
                Bukkit.getLogger().info("Member #" + m.getID() + " was not saved because it has no clan.");
                return;
            }
            cf.set(id, clanname);
        }
        try {
            cf.save("plugins" + File.separator + "ProjectZ" + File.separator + "members.yml");
        } catch (IOException e) {
            e.printStackTrace();
        }*/
    }

}
