package projectz.clans;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import projectz.Main;

public class ClanPerks {
    
    private static Material[] DoubleableBlockArray = {
        Material.LAPIS_ORE,
        Material.REDSTONE_ORE,
        Material.EMERALD_ORE,
        Material.DIAMOND_ORE,
        Material.COAL_ORE,
        Material.MELON_BLOCK,
        Material.PUMPKIN
    };    
    
    private static Material[] DoubleableSmeltArray = {
        Material.COBBLESTONE,
        Material.LOG,
        Material.LOG_2,
        Material.IRON_ORE,
        Material.GOLD_ORE,
        Material.RAW_CHICKEN,
        Material.RAW_FISH,
        Material.RAW_BEEF,
        Material.PORK,
        Material.MUTTON,
        Material.RABBIT,
        Material.POTATO_ITEM
    };
    
    private static Set<Material> DoubleableBlocks = new HashSet<Material>();
    private static Set<Material> DoubleableSmelts = new HashSet<Material>();
    private static List<Set<ItemStack>> LevelUpCosts = new ArrayList<Set<ItemStack>>();
    
    public static void load() {
        for (Material m : DoubleableBlockArray)
            DoubleableBlocks.add(m);
        for (Material m : DoubleableSmeltArray)
            DoubleableSmelts.add(m);
        List<String> costList = Main.getConfiguration().getStringList("clan-perk-costs");
        for (int i = 0; i < costList.size(); i++) {
            String[] c = costList.get(i).split(" ");
                LevelUpCosts.add(translateToItems(StringArrayToIntArray(c)));
        }
    }
    
    private static Set<ItemStack> translateToItems(Integer[] c) {
        Set<ItemStack> i = new HashSet<ItemStack>();
        i.add(new ItemStack(Material.LOG, c[1]));
        i.add(new ItemStack(Material.COBBLESTONE, c[2]));
        if (c[3] > 0) i.add(new ItemStack(Material.STONE, c[3]));
        if (c[4] > 0) i.add(new ItemStack(Material.COAL, c[4]));
        if (c[5] > 0) i.add(new ItemStack(Material.IRON_INGOT, c[5]));
        if (c[6] > 0) i.add(new ItemStack(Material.GOLD_INGOT, c[6]));
        if (c[7] > 0) i.add(new ItemStack(Material.REDSTONE, c[7]));
        if (c[8] > 0) i.add(new ItemStack(Material.DIAMOND, c[8]));
        if (c[9] > 0) i.add(new ItemStack(Material.OBSIDIAN, c[9]));
        if (c[10] > 0) i.add(new ItemStack(Material.EMERALD, c[10]));
        if (c[11] > 0) i.add(new ItemStack(Material.NETHERRACK, c[11]));
        if (c[12] > 0) i.add(new ItemStack(Material.NETHER_BRICK, c[12]));
        if (c[13] > 0) i.add(new ItemStack(Material.QUARTZ_BLOCK, c[13]));
        if (c[14] > 0) i.add(new ItemStack(Material.GLOWSTONE, c[14]));
        return i;
    }
    
    private static Integer[] StringArrayToIntArray(String[] c) {
        List<Integer> ints = new ArrayList<Integer>();
        for (String s : c)
            ints.add(Integer.parseInt(s));
        Integer[] base = new Integer[ints.size()];
        return ints.toArray(base);
    }
    
    @SuppressWarnings("deprecation")
    public static boolean isDoubleBlock(Block b) {
        Material m = b.getType();
        switch (m) {
            case CROPS:
            case CARROT:
            case POTATO:
                return b.getData() == 7;
            default:
                return DoubleableBlocks.contains(m);
        }
    }
    
    public static boolean isDoubleSmelt(Material m) {
        return DoubleableSmelts.contains(m);
    }
    
    public static boolean attemptUpgradeClan(Clan c, int upgradeTo, Map<Material, Integer> resources) {
        int cc = upgradeTo - 1;
        for (ItemStack need : LevelUpCosts.get(cc)) {
            if (resources.containsKey(need.getType()) && resources.get(need.getType()) >= need.getAmount())
                continue;
            return false;
        }

        for (ItemStack need : LevelUpCosts.get(cc))
            c.removeResourceAmount(need.getType(), need.getAmount());
        return true;
    }
    
    public static Set<ItemStack> nextLevelCost(int currentLevel) {
        return LevelUpCosts.get(currentLevel);
    }
    
    public static Set<Material> nextLevelRequirements(int currentLevel) {
        Set<Material> mats = new HashSet<Material>();
        for (ItemStack i : LevelUpCosts.get(currentLevel))
            if (i.getAmount() > 0) mats.add(i.getType());
        return mats;
    }
    
    public static int getUpgradeItemAmount(int currentLevel, Material mat) {
        for (ItemStack i : LevelUpCosts.get(currentLevel)) {
            if (i.getType() == mat)
                return i.getAmount();
        }
        return 0;
    }
    
    public static void displayClanPerks(Player p) {
        int lvl = RegionHandler.getClan(p) == null ? 0 : RegionHandler.getClan(p).getLevel();
        for (int i = 4; i < 20; i++) {
            if (i > lvl + 4)
                p.sendMessage(Main.clanLevelsHelp.get(i).replace("�f", "�6").replace("�a", "�7"));
            else
                p.sendMessage(Main.clanLevelsHelp.get(i).replace("�f", "�e"));
        }
    }
    
    public static String getPerkString(int lvl) {
        switch (lvl) {
        case 1:
            return "6% Chance of Double Yield on Claims";
        case 2:
            return "10% Damage Reduction on Claims";
        case 3:
            return "+25% XP on Claims";
        case 4:
            return "12% Chance of Double Yield on Claims";
        case 5:
            return "25% Damage Reduction on Claims";
        case 6:
            return "+1 Extra Food Value when eating on Claims";
        case 7:
            return "+1 Extra Damage on Claims";
        case 8:
            return "+50% Natural Regeneration on Claims";
        case 9:
            return "25% Chance of Double Yield on Claims";
        case 10:
            return "+75% XP on Claims";
        case 11:
            return "40% Damage Reduction on Claims";
        case 12:
            return "+2 Extra Food Value when eating on Claims";
        case 13:
            return "+2 Extra Damage on Claims";
        case 14:
            return "+100% Natural Regeneration on Claims";
        case 15:
            return "50% Chance of Double Yield on Claims";
        }
        return "penis";
    }

}
