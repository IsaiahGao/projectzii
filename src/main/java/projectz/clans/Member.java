package projectz.clans;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import projectz.clans.Clan.Role;

public class Member {
    
    private UUID ID;
    private Role role;
    private OfflinePlayer player;
    private Clan clan;
    private String name;
    
    public Member(UUID id, String clanname, Role role) {
        this.ID = id;
        player = Bukkit.getOfflinePlayer(id);
        name = player.getName();
        clan = RegionHandler.getClan(clanname);
        this.role = role;
        this.clan.setRole(this, role);
        RegionHandler.members.put(ID, this);
    }
    
    public UUID getID() {
        return ID;
    }
    
    public Role getRole() {
        return role;
    }
    
    public void setRole(Role role) {
        this.role = role;
        clan.updateRole(ID);
    }
    
    public Player getPlayer() {
        return player.getPlayer();
    }
    
    public OfflinePlayer getOfflinePlayer() {
        return player;
    }
    
    public Clan getClan() {
        return clan;
    }
    
    public void setClan(Clan clan) {
        this.clan = clan;
    }
    
    public String getChatName() {
        return role.getTag() + " " + name;
    }
    
    public String getRawChatPrefix() {
        return role.getRawTag() + clan.getRawTag();
    }
    
    public void sendMessage(String message) {
        getPlayer().sendMessage(message);
    }

}
