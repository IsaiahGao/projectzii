package projectz.food;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scheduler.BukkitRunnable;

import projectz.Main;
import projectz.Utilities;
import utilities.particles.ParticleEffects;
import utilities.particles.ParticleEffects.ItemData;

public class CustomFoodHandler {
    
    public enum CustomFood {
        MELON_SEEDS("Roasted Seeds", Material.BAKED_POTATO, 1, 1.8F, null),
        NETHER_STALK("Berries", Material.APPLE, 1, 0.8F, null),
        SPONGE("Cheese", Material.BAKED_POTATO, 4, 9.6F, null);
        
        private CustomFood(String whatitis, Material eatparticles, int hunger, float saturation, PotionEffect[] effects) {
            this.hunger = hunger;
            this.eatparticles = eatparticles;
            this.saturation = saturation;
            this.effects = effects;
        }
        
        private Material eatparticles;
        private int hunger;
        private float saturation;
        private PotionEffect[] effects;
        
        public void eat(final Player p) {
            Utilities.removeItem(p);
            p.setFoodLevel(p.getFoodLevel() + hunger);
            p.setSaturation(saturation);
            if (effects != null)
                for (PotionEffect eff : effects)
                    p.addPotionEffect(eff, true);
                p.getWorld().playSound(p.getLocation(), Sound.ENTITY_GENERIC_EAT, 1.0F, 1.0F);
                //p.playEffect(p.getEyeLocation(), Effect.STEP_SOUND, eatparticles);
                ParticleEffects.ITEM_CRACK.display(new ItemData(eatparticles, (byte) 0), 0.1F, 0.1F, 0.1F, 0.07F, 12, p.getEyeLocation(), 256);
                new BukkitRunnable() {
                    public void run() {
                        if (p != null)
                            p.getWorld().playSound(p.getLocation(), Sound.ENTITY_PLAYER_BURP, 1.0F, 1.0F);
                    }
                }.runTaskLater(Main.getPlugin(), 4L);
        }
    }
    
    public static CustomFood getFood(Material mat) {
        return CustomFood.valueOf(mat.toString());
    }
}
