package projectz.food;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import projectz.ItemDef;
import projectz.Utilities;
import projectz.tasks.DepletionTaskCycler.StatType;
import utilities.Utils;

public class ModifiedFoodHandler {
    
    public static final Map<String, ItemStack> newFoods = new HashMap<String, ItemStack>();
    
    public static final ItemStack applePie = new ItemStack(Material.PUMPKIN_PIE);
    public static final ItemStack potatoStew = new ItemStack(Material.MUSHROOM_SOUP);
    public static final ItemStack carrotStew = new ItemStack(Material.MUSHROOM_SOUP);
    public static final ItemStack chickenPotPie = new ItemStack(Material.RABBIT_STEW);
    public static final ItemStack simpleSandwich = new ItemStack(Material.BREAD);
    public static final ItemStack advancedSandwich = new ItemStack(Material.BREAD);
    public static final ItemStack burger = new ItemStack(Material.BREAD, 1);
    public static final ItemStack tea = new ItemStack(Material.POTION, 1);
    public static final ItemStack herbalTea = new ItemStack(Material.POTION);
    public static final ItemStack sweetTea = new ItemStack(Material.POTION);
    public static final ItemStack icedTea = new ItemStack(Material.POTION);
    public static final ItemStack coffee = new ItemStack(Material.POTION);
    public static final ItemStack fruitPunch = new ItemStack(Material.POTION);
    public static final ItemStack fishSticks = new ItemStack(Material.GRILLED_PORK);
    public static final ItemStack phishTaco = new ItemStack(Material.BAKED_POTATO);
    
    private static final void setName(String s, ItemStack i) {
        ItemMeta meta = i.getItemMeta();
        meta.setDisplayName(ChatColor.RESET + s);
        i.setItemMeta(meta);
    }
    
    private static final void setLore(String s, ItemStack i) {
        List<String> lore = new ArrayList<String>();
        ItemMeta meta = i.getItemMeta();
        lore.add(s);
        meta.setLore(lore);
        i.setItemMeta(meta);
    }
    
    public static void setModifiedFoods() {
    setName("Apple Pie", applePie);
    setName("Potato Stew", potatoStew);
    setName("Carrot Stew", carrotStew);
    setName("Chicken Pot Pie", chickenPotPie);
    setName("Small Sandwich", simpleSandwich);
    simpleSandwich.setAmount(3);
    setName("Huge Sandwich", advancedSandwich);
    advancedSandwich.setAmount(3);
    setName("Burger", burger);
    burger.setAmount(2);
    setName(ChatColor.GREEN + "Herbal Tea", herbalTea);
    setPotionEffect(herbalTea, new PotionEffect(PotionEffectType.REGENERATION, 160, 0));
    ItemDef.spc(herbalTea, 204, 202, 96);
    setName(ChatColor.GREEN + "Green Tea", tea);
    setPotionEffect(tea, new PotionEffect(PotionEffectType.HEALTH_BOOST, 600, 0));
    ItemDef.spc(tea, 146, 204, 96);
    setName(ChatColor.GREEN + "Sweet Tea", sweetTea);
    setPotionEffect(sweetTea, new PotionEffect(PotionEffectType.SPEED, 240, 0));
    ItemDef.spc(sweetTea, 225, 140, 58);
    setName(ChatColor.GREEN + "Iced Tea", icedTea);
    setPotionEffect(icedTea, new PotionEffect(PotionEffectType.FIRE_RESISTANCE, 240, 0));
    ItemDef.spc(icedTea, 210, 140, 31);
    setName(ChatColor.GREEN + "Fruit Punch", fruitPunch);
    setPotionEffect(fruitPunch, new PotionEffect(PotionEffectType.REGENERATION, 100, 0));
    setPotionEffect(fruitPunch, new PotionEffect(PotionEffectType.SPEED, 100, 0));
    ItemDef.spc(fruitPunch, 225, 43, 72);
    setName(ChatColor.GREEN + "Coffee", coffee);
    setPotionEffect(coffee, new PotionEffect(PotionEffectType.FAST_DIGGING, 240, 0));
    ItemDef.spc(coffee, 144, 86, 22);
    setName("Fish Sticks", fishSticks);
    fishSticks.setAmount(4);
    setName("Fish Taco", phishTaco);
    phishTaco.setAmount(2);
    
    setLore(ChatColor.GRAY + "It's got apples in it.", applePie);
    setLore(ChatColor.GRAY + "Potato floating in water.", potatoStew);
    setLore(ChatColor.GRAY + "Carrot floating in water.", carrotStew);
    setLore(ChatColor.GRAY + "Chicken and veggies in a pie.", chickenPotPie);
    setLore(ChatColor.GRAY + "Bread with meat in it.", simpleSandwich);
    setLore(ChatColor.GRAY + "Bread with lots of filling.", advancedSandwich);
    setLore(ChatColor.GRAY + "Bread with animal flesh.", burger);
    setLore(ChatColor.GRAY + "Looks pretty grassy.", herbalTea);
    setLore(ChatColor.GRAY + "Looks pretty green.", tea);
    setLore(ChatColor.GRAY + "Looks pretty sugary.", sweetTea);
    setLore(ChatColor.GRAY + "Looks pretty cold.", icedTea);
    setLore(ChatColor.GRAY + "Looks refreshing.", fruitPunch);
    setLore(ChatColor.GRAY + "Milk and sugar added.", coffee);
    setLore(ChatColor.GRAY + "Sticks of fish.", fishSticks);
    setLore(ChatColor.GRAY + "Bread shell with fish and veggies.", phishTaco);
    
    newFoods.put(ChatColor.GRAY + "It's got apples in it.", applePie);
    newFoods.put(ChatColor.GRAY + "Potato floating in water.", potatoStew);
    newFoods.put(ChatColor.GRAY + "Carrot floating in water.", carrotStew);
    newFoods.put(ChatColor.GRAY + "Chicken and veggies in a pie.", chickenPotPie);
    newFoods.put(ChatColor.GRAY + "Bread with meat in it.", simpleSandwich);
    newFoods.put(ChatColor.GRAY + "Bread with lots of filling.", advancedSandwich);
    newFoods.put(ChatColor.GRAY + "Bread with cow flesh.", burger);
    newFoods.put(ChatColor.GRAY + "Looks pretty grassy.", herbalTea);
    newFoods.put(ChatColor.GRAY + "Looks pretty green.", tea);
    newFoods.put(ChatColor.GRAY + "Looks pretty sugary.", sweetTea);
    newFoods.put(ChatColor.GRAY + "Looks pretty cold.", icedTea);
    newFoods.put(ChatColor.GRAY + "Looks refreshing.", fruitPunch);
    newFoods.put(ChatColor.GRAY + "Milk and sugar added.", coffee);
    newFoods.put(ChatColor.GRAY + "Sticks of fish.", fishSticks);
    newFoods.put(ChatColor.GRAY + "Bread shell with fish and veggies.", phishTaco);
    
    for (ItemStack i : newFoods.values())
            i.addEnchantment(Utils.glow, 1);
    }
    
    private static void setPotionEffect(ItemStack i, PotionEffect eff) {
        if (i.getType().equals(Material.POTION)) {
            PotionMeta meta = (PotionMeta) i.getItemMeta();
            meta.addCustomEffect(eff, true);
            i.setItemMeta(meta);
        }
    }
    
    public enum ModifiedFood {
        APPLE_PIE(applePie, ChatColor.GRAY + "It's got apples in it.", 12, 7.2F, 0, 0, 0, 0.5F, null),
        POTATO_STEW(potatoStew, ChatColor.GRAY + "Potato floating in water.", 6, 3.8F, 5, 0, 0, 1, null),
        CARROT_STEW(carrotStew, ChatColor.GRAY + "Carrot floating in water.", 6, 4.6F, 5, 0, 0, 1, null),
        CHICKEN_POT_PIE(chickenPotPie, ChatColor.GRAY + "Chicken and veggies in a pie.", 12, 12.8F, 0, 0, 0, 2, null),
        SIMPLE_SANDWICH(simpleSandwich, ChatColor.GRAY + "Bread with meat in it.", 5, 8.2F, 0, 0, 0, 0, null),
        ADVANCED_SANDWICH(advancedSandwich, ChatColor.GRAY + "Bread with lots of filling.", 11, 10.2F, 0, 0, 0, 0, null),
        BURGER(burger, ChatColor.GRAY + "Bread with cow flesh.", 12, 12.8F, 0, 0, 0, 0, null),
        TEA(tea, ChatColor.GRAY + "Looks pretty green.", 1, 0, 27, 13, 1, 2, null),
        HERBAL_TEA(herbalTea, ChatColor.GRAY + "Looks pretty grassy.", 1, 0, 27, 13, 2, 2, null),
        SWEET_TEA(sweetTea, ChatColor.GRAY + "Looks pretty sugary.", 1, 0, 27, 13, 1, 0, null),
        ICED_TEA(icedTea, ChatColor.GRAY + "Looks pretty cold.", 1, 0, 27, 13, 1, -3, null),
        FRUIT_PUNCH(fruitPunch, ChatColor.GRAY + "Looks refreshing.", 1, 0, 27, 13, 5, 0, null),
        COFFEE(coffee, ChatColor.GRAY + "Milk and sugar added.", 1, 0, 25, 8, 5, 3, null),
        FISH_STICKS(fishSticks, ChatColor.GRAY + "Sticks of fish.", 4, 6.2F, 0, 0, 0, 0, null),
        FISH_TACO(phishTaco, ChatColor.GRAY + "Bread shell with fish and veggies.", 8, 7.2F, 0, 0, 0, 0, null);
        
        public static Map<String, ModifiedFood> loreToFood = new HashMap<String, ModifiedFood>();
        static {
            for (ModifiedFood m : ModifiedFood.values())
                loreToFood.put(m.getIdentifier(), m);
        }
        
        private ModifiedFood(ItemStack item, String identifier, int hunger, float saturation, float hydration, float hydroSat, float sanity, float warmth, PotionEffect[] effects) {
            this.hydration = hydration;
            this.hydroSat = hydroSat;
            this.sanity = sanity;
            this.warmth = warmth;
            this.identifier = identifier;
            this.hunger = hunger;
            this.saturation = saturation;
            this.effects = effects;
        }
        
        private String identifier;
        private int hunger;
        private float hydration;
        private float sanity;
        private float warmth;
        private float hydroSat;
        private float saturation;
        private PotionEffect[] effects;
        
        private String getIdentifier() {
            return identifier;
        }
        
        public void eat(final Player p) {
            Utilities.removeItem(p);
            p.setFoodLevel(p.getFoodLevel() + hunger);
            if (saturation > 0) p.setSaturation(saturation);
            if (effects != null)
                for (PotionEffect eff : effects)
                    p.addPotionEffect(eff, true);
                p.getWorld().playSound(p.getLocation(), Sound.ENTITY_PLAYER_BURP, 1.0F, 1.0F);
            if (hydration != 0) StatType.HYDRATION.change(p, hydration);
            if (hydroSat != 0) StatType.HYDRATION_SAT.change(p, hydroSat);
            if (sanity != 0) StatType.SANITY.change(p, sanity);
            if (warmth != 0) StatType.WARMTH.change(p, warmth);
        }
    }
    
    public static boolean isModifiedFood(ItemStack i) {
        if (i.getItemMeta().getLore() == null) return false;
        return ModifiedFood.loreToFood.containsKey(i.getItemMeta().getLore().get(0));
    }
    
    public static ModifiedFood getFood(ItemStack i) {
        return ModifiedFood.loreToFood.get(i.getItemMeta().getLore().get(0));
    }
}
