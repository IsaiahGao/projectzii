package projectz.listeners.events;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Cancellable;

import projectz.treasure.cluescrolls.ClueScroll;
 
public final class ClueScrollIncrementEvent extends Event implements Cancellable {
    private static final HandlerList handlers = new HandlerList();
    private Player player;
    private Entity entity;
    private int i;
    private ClueScroll clue;
    private boolean cancelled;
 
    public ClueScrollIncrementEvent(ClueScroll clue, Player player, Entity entity, int increment) {
        this.player = player;
        i = increment;
        this.entity = entity;
        this.clue = clue;
    }
    
    public ClueScroll getClueScroll() {
        return clue;
    }
 
    public Player getPlayer() {
        return player;
    }
    
    public int getIncrement() {
        return i;
    }
    
    public Entity getEntity() {
        return entity;
    }
 
    public boolean isCancelled() {
        return cancelled;
    }
 
    public void setCancelled(boolean cancel) {
        cancelled = cancel;
    }
 
    public HandlerList getHandlers() {
        return handlers;
    }
 
    public static HandlerList getHandlerList() {
        return handlers;
    }
}