package projectz.listeners.events;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

public final class BottleFillEvent extends PlayerEvent implements Cancellable {
    private static final HandlerList handlers = new HandlerList();
    private boolean cancelled;
    private Block block;
 
    public BottleFillEvent(Player player, Block block) {
        super(player);
        this.block = block;
    }
    
    @Override
    public void setCancelled(boolean cancel) {
        cancelled = cancel;
    }
 
    @Override
    public HandlerList getHandlers() {
        return handlers;
    }
    
    public Block getBlock() {
        return this.block;
    }
 
    public static HandlerList getHandlerList() {
        return handlers;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }
}