package projectz.listeners.events;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Cancellable;

import projectz.treasure.cluescrolls.ClueScroll;
 
public final class ClueScrollDigEvent extends Event implements Cancellable {
    private static final HandlerList handlers = new HandlerList();
    private Player player;
    private Block block;
    private ClueScroll clue;
    private boolean cancelled;
 
    public ClueScrollDigEvent(ClueScroll clue, Player player, Block digBlock) {
        this.player = player;
        this.block = digBlock;
        this.clue = clue;
    }
    
    public ClueScroll getClueScroll() {
        return clue;
    }
 
    public Player getPlayer() {
        return player;
    }
    
    public Block getBlock() {
        return block;
    }
 
    public boolean isCancelled() {
        return cancelled;
    }
 
    public void setCancelled(boolean cancel) {
        cancelled = cancel;
    }
 
    public HandlerList getHandlers() {
        return handlers;
    }
 
    public static HandlerList getHandlerList() {
        return handlers;
    }
}