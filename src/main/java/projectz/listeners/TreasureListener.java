package projectz.listeners;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.event.player.PlayerFishEvent.State;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import projectz.Main;
import projectz.listeners.events.ClueScrollDigEvent;
import projectz.listeners.events.ClueScrollIncrementEvent;
import projectz.treasure.ClueScrollHandler;
import projectz.treasure.cluescrolls.ClueScroll;
import shadowrealm.augments.AugmentEnums.Augment;

public class TreasureListener implements Listener {
    
    @EventHandler
    public void onClueScrollDig(ClueScrollDigEvent e) {
        Block b = e.getBlock();
        ClueScroll clue = e.getClueScroll();
        switch (clue.getType()) {
            case DIG_AT_NETHER_COORDS:
                String s = ChatColor.stripColor(clue.getMessage().get(2));
                String[] s2 = s.replace("X: ", "").replace(" Y: ", "").replace(" Z: ", "").split(",");
                Location coord = new Location(Main.getNether(), Double.parseDouble(s2[0]), Double.parseDouble(s2[1]), Double.parseDouble(s2[2]));
                if (b.getLocation().distanceSquared(coord) <= 4)
                    clue.complete(e.getPlayer());
                break;
            case DIG_AT_COORDS:
                String a = ChatColor.stripColor(clue.getMessage().get(2));
                String[] a2 = a.replace("X: ", "").replace(" Y: ", "").replace(" Z: ", "").split(",");
                Location coords = new Location(Main.getOverworld(), Double.parseDouble(a2[0]), Double.parseDouble(a2[1]), Double.parseDouble(a2[2]));
                if (b.getLocation().distanceSquared(coords) <= 4)
                    clue.complete(e.getPlayer());
                break;
            case MINE_BLOCK:
                e.getPlayer().sendMessage("block");
                Material target = Material.valueOf(ChatColor.stripColor(clue.getMessage().get(2).toUpperCase()));
                if (b.getType().equals(target))
                    clue.complete(e.getPlayer());
                break;
            default:
                break;
        }
    }
    
    @EventHandler
    public void onClueIncrement(ClueScrollIncrementEvent e) {
        ClueScroll clue = e.getClueScroll();
        switch (clue.getType()) {
            case CATCH_FISH:
                short data = ClueScrollHandler.getFishData(ChatColor.stripColor(clue.getMessage().get(2)).split(" more ")[1]);
                int has = Integer.parseInt(ChatColor.stripColor(clue.getMessage().get(2)).split(" more ")[0]);
                if (((Item) e.getEntity()).getItemStack().getDurability() == data) {
                    has -= 1;
                    if (has <= 0) {
                        clue.complete(e.getPlayer());
                        return;
                    }
                    Player p = e.getPlayer();
                    ItemStack i = clue.toItem();
                        p.getInventory().remove(i);
                        ItemMeta meta = i.getItemMeta();
                        List<String> newlore = meta.getLore();
                        newlore.set(2, ChatColor.WHITE + "" + has + ChatColor.GRAY + " more " + ChatColor.WHITE + ClueScrollHandler.getFishName(data));
                        meta.setLore(newlore);
                        i.setItemMeta(meta);
                    p.getInventory().addItem(i);
                }
                break;
            case KILL_X_ENEMY:
                EntityType data2 = EntityType.valueOf(ChatColor.stripColor(clue.getMessage().get(2).toUpperCase()));
                int has2 = Integer.parseInt(ChatColor.stripColor(clue.getMessage().get(3)).split(" more times")[0]);
                if (e.getEntity().getType().equals(data2)) {
                    has2 -= 1;
                    if (has2 == 0) {
                        clue.complete(e.getPlayer());
                        return;
                    }
                    Player p = e.getPlayer();
                    ItemStack i = clue.toItem();
                        p.getInventory().remove(i);
                        ItemMeta meta = i.getItemMeta();
                        List<String> newlore = meta.getLore();
                        newlore.set(3, ChatColor.WHITE + "" + has2 + ChatColor.GRAY + " more times");
                        meta.setLore(newlore);
                        i.setItemMeta(meta);
                    p.getInventory().addItem(i);
                }
                break;
            default:
                break;
        }
    }
    
    @EventHandler
    public void onFishingRod(PlayerFishEvent e) {
        Player p = e.getPlayer();
        if (e.getState() == State.CAUGHT_FISH) {
            //int a = p.getItemInHand().containsEnchantment(Augment.MASTER_BAITER.getAugment()) ? 2 : 1;
            //if (!p.getItemInHand().containsEnchantment(Augment.BAITLESS.getAugment())) {
                if (!p.getInventory().containsAtLeast(new ItemStack(Material.ROTTEN_FLESH), 1) && !p.getInventory().containsAtLeast(new ItemStack(Material.SPIDER_EYE), 1)) {
                    p.sendMessage(ChatColor.GRAY + "It appears I will need bait to catch fish (Rotten Flesh or Spider Eyes).");
                    e.getCaught().remove();
                    e.getHook().remove();
                    return;
                }
                /*Item caught = (Item) e.getCaught();
                boolean fish = caught.getItemStack().getType().equals(Material.RAW_FISH);
                if (!fish) {
                    ItemStack treasure = FishingTreasure.FISHING_ROD.getRandomTreasure(SkillAssistant.getPlayerClass(p).equals(Skill.GATHERING) ? 0.1F : 0F);
                    caught.setItemStack(treasure);
                    e.setExpToDrop(e.getExpToDrop() * 2);
                }
                if (SkillAssistant.getPlayerClass(p).equals(Skill.GATHERING) && Main.random(999) < Main.gatheringlevel.getScore(p.getName()).getScore()) {
                    p.getWorld().playSound(e.getHook().getLocation(), Sound.SUCCESSFUL_HIT, 0.6F, 0.8F);
                    ParticleEffects.VILLAGER_HAPPY.display(1.0F, 1.0F, 1.0F, 0F, 18, e.getHook().getLocation(), 36);
                    p.sendMessage(ChatColor.GRAY + "Your incredible Artisan-ness doubles your catch!");
                    ItemStack i = caught.getItemStack();
                    i.setAmount(i.getAmount() * 2);
                    caught.setItemStack(i);
                }*/
                for (int i : p.getInventory().removeItem(new ItemStack(Material.ROTTEN_FLESH)).keySet()) {
                    p.getInventory().removeItem(new ItemStack(Material.SPIDER_EYE, i));
                    break;
                }
                /*if (a == 2) {
                    ItemStack item = ((Item) e.getCaught()).getItemStack();
                    item.setAmount(item.getAmount() * 2);
                }*/
            }
            
            if (e.getPlayer().getInventory().contains(Material.EMPTY_MAP)) {
                ClueScroll clue = ClueScroll.containsClueScoll(e.getPlayer().getInventory());
                if (clue != null) {
                    Bukkit.getServer().getPluginManager().callEvent(new ClueScrollIncrementEvent(clue, e.getPlayer(), e.getCaught(), 1));
                }
            }
        //}
    }

}
