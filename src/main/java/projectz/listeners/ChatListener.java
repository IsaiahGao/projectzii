package projectz.listeners;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import projectz.Main;
import projectz.clans.RegionHandler;

public class ChatListener /*implements Listener*/ {
    
    /*@EventHandler
    public void onPlayerChat(final AsyncPlayerChatEvent e) {
        e.setCancelled(true);
        new BukkitRunnable() {
            public void run() {
                Player p = e.getPlayer();
                ChatColor color = getRankColor(p);
                String prefix = color + "";
                if (RegionHandler.members.containsKey(p.getUniqueId())) {
                    prefix += "[" + RegionHandler.members.get(p.getUniqueId()).getRawChatPrefix() + "] ";
                }
                String msg = ChatColor.BOLD + prefix + ChatColor.RESET + color + p.getName() + ChatColor.RESET + ": " + e.getMessage();
                for (Player pl : Bukkit.getOnlinePlayers())
                    pl.sendMessage(msg);
            }
        }.runTask(Main.getPlugin());
    }*/
    
    public static String getPrefix(Player p) {
        ChatColor color = getRankColor(p);
        String prefix = color + "";
        if (RegionHandler.members.containsKey(p.getUniqueId())) {
            prefix += "[" + RegionHandler.members.get(p.getUniqueId()).getRawChatPrefix() + "] ";
        }
        return prefix;
    }
    
    public static ChatColor getRankColor(Player p) {
        switch (Main.staffRank.getScore(p.getName()).getScore()) {
        case 1: // Helper
            return ChatColor.GREEN;
        case 2: // Mod
            return ChatColor.BLUE;
        case 3: // Admin
            return ChatColor.DARK_RED;
            default:
                switch (Main.donationRank.getScore(p.getName()).getScore()) {
                case 1:
                    return ChatColor.GOLD;
                }
                return RegionHandler.getClan(p) != null ? ChatColor.GRAY : ChatColor.DARK_GRAY;
        }
    }

}
