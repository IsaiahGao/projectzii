package projectz.listeners;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.entity.EntityTargetEvent.TargetReason;
import org.bukkit.scheduler.BukkitRunnable;

import projectz.Main;
import projectz.bosses.Boss;
import projectz.bosses.BossHandler;
import projectz.bosses.BossProjectileAbility;
import projectz.bosses.BossHandler.BossMob;
import utilities.Utils;
import utilities.gui.ActionBar;

public class BossListener implements Listener {
    
    @EventHandler
    public void abilityActivate(EntityTargetEvent e) {
        if (e.getEntity() instanceof LivingEntity) {
            LivingEntity le = (LivingEntity) e.getEntity();
            Boss b = BossHandler.getBossByEntity(le);
            if (b == null) return;
            if (e.getReason().equals(TargetReason.FORGOT_TARGET) || e.getReason().equals(TargetReason.TARGET_DIED)) {
                BossHandler.setAbilityCycle(le, false);
            } else {
                BossHandler.setAbilityCycle(le, true);
            }
        }
    }
    
    @EventHandler
    public void bossHit(EntityDamageByEntityEvent e) {
        if (e.getEntity() instanceof LivingEntity) {
            if (e.getDamager() instanceof LivingEntity) {
                LivingEntity le = (LivingEntity) e.getDamager();
                Boss b = BossHandler.getBossByEntity(le);
                if (b != null) {
                    if (b.getContactAbilityChance() > -1 && Utils.random(b.getContactAbilityChance()) == 0) {
                        if (b.getContactAbilities() == null)
                            Bukkit.getLogger().info("NULL");
                        else
                        b.getContactAbilities().get(Utils.random(b.getContactAbilities().size() - 1)).activate(le, (LivingEntity) e.getEntity(), e);
                    }
                }
            }
            if (e.getDamager() instanceof Projectile) {
                Projectile proj = (Projectile) e.getDamager();
                if (proj.getShooter() instanceof LivingEntity) {
                    LivingEntity le = (LivingEntity) proj.getShooter();
                    Boss b = BossHandler.getBossByEntity(le);
                    if (b != null) {
                        if (b.getContactAbilityChance() != -1 && Utils.random(b.getContactAbilityChance()) == 0) {
                            b.getContactAbilities().get(Utils.random(b.getContactAbilities().size() - 1)).activate(le, (LivingEntity) e.getEntity(), e);
                        }
                        if (proj.hasMetadata("bossability")) {
                            BossProjectileAbility abil = (BossProjectileAbility) BossHandler.getAbility(proj.getMetadata("bossability").get(0).asString());
                            abil.impact(proj, (LivingEntity) e.getEntity(), e);
                        }
                    }
                    if (proj.getShooter() instanceof Player) {
                        Boss b2 = BossHandler.getBossByEntity((LivingEntity) e.getEntity());
                        if (b2 != null) {
                            if (((proj instanceof Arrow) && b2.getImmunities().contains(DamageCause.PROJECTILE)) || (proj.hasMetadata("ability") && b2.getImmunities().contains(DamageCause.MAGIC)))
                                e.setCancelled(true);
                        }
                    }
                }
            }
        }
    }
    
    @EventHandler
    public void bossDeath(EntityDeathEvent e) {
        if (e.getEntity() instanceof LivingEntity) {
            LivingEntity le = (LivingEntity) e.getEntity();
            Boss b = BossHandler.getBossByEntity(le);
            if (b != null) {
                e.setDroppedExp(b.getXPDrop());
                b.die(le);
            } else {
                if (le.getKiller() != null && Math.random() < 0.01 && !le.getLocation().getBlock().getType().isOccluding() && le.getLocation().getBlock().getLightFromSky() > 13 && (le.getWorld().getName().endsWith("_nether") || Math.abs(le.getWorld().getTime() - 18075) <= 5775)) {
                    final BossMob bm = BossMob.get(le.getType());
                    if (bm != null && bm.isEnabled()) {
                        final Player p = le.getKiller();
                        final Location loc = le.getLocation();
                        new BukkitRunnable() {
                            public void run() {
                                if (loc.getChunk().isLoaded()) {
                                    bm.spawn(loc);
                                    loc.getWorld().strikeLightningEffect(loc);
                                    new ActionBar().sendMessage(p, "�cA powerful monster spawns nearby!");
                                }
                            }
                        }.runTaskLater(Main.getPlugin(), 60L);
                    }
                }
            }
        }
    }
    
    @EventHandler(priority = EventPriority.LOW)
    public void bossDamage(EntityDamageEvent e) {
        if (e.getEntity() instanceof LivingEntity) {
            LivingEntity le = (LivingEntity) e.getEntity();
            Boss b = BossHandler.getBossByEntity(le);
            if (b != null && !e.getCause().equals(DamageCause.PROJECTILE) && !e.getCause().equals(DamageCause.MAGIC)) {
                if (b.getImmunities().contains(e.getCause()))
                    e.setCancelled(true);
            }
        }
    }

}
