package projectz.listeners;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent.RegainReason;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.inventory.FurnaceSmeltEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerExpChangeEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

import projectz.Main;
import projectz.clans.Clan;
import projectz.clans.ClanPerks;
import projectz.clans.RegionHandler;
import projectz.clans.ClanInvite.InviteAction;
import utilities.particles.ParticleEffects;

public class ClanListener implements Listener {
    
    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerLeave(PlayerQuitEvent e) {
        if (Main.getInviteHandler().hasPendingInvitation(e.getPlayer()))
            Main.getInviteHandler().handleInviteResponse(e.getPlayer(), InviteAction.DISCONNECTED);
    }
    
    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerKickLeave(PlayerKickEvent e) {
        if (Main.getInviteHandler().hasPendingInvitation(e.getPlayer()))
            Main.getInviteHandler().handleInviteResponse(e.getPlayer(), InviteAction.DISCONNECTED);
    }
    
    @EventHandler(priority = EventPriority.MONITOR)
    public void clanDoubleDrops(BlockBreakEvent e) {
        Player p = e.getPlayer();
        Block b = e.getBlock();
        Clan clan = RegionHandler.getClan(p);
        if (clan != null && !p.getItemInHand().containsEnchantment(Enchantment.SILK_TOUCH) && clan.getLevel() > 0 && ClanPerks.isDoubleBlock(b) && clan.inRegion(p)) {
            int L = clan.getLevel();
            boolean doubled = false;
                if (L == 15) doubled = Math.random() < .5;
                else if (L >= 9) doubled = Math.random() < .25;
                else if (L >= 3) doubled = Math.random() < .1;
                else doubled = Math.random() < .05;
            if (doubled) {
                ParticleEffects.FIREWORKS_SPARK.display(0.5F, 0.5F, 0.5F, 0.1F, 16, b.getLocation().add(0.5, 0.5, 0.5), 257);
                for (ItemStack drop : b.getDrops(p.getItemInHand()))
                    b.getWorld().dropItem(b.getLocation().add(0.5, 0.5, 0.5), drop);
            }
        }
    }
    
    @EventHandler(priority = EventPriority.HIGH)
    public void clanXPIncrease(PlayerExpChangeEvent e) {
        if (e.getAmount() > 0) {
            Player p = e.getPlayer();
            Clan clan = RegionHandler.getClan(p);
            if (clan != null && clan.getLevel() > 2 /*&& clan.inRegion(p)*/) {
                float mult = 1.25F;
                if (clan.getLevel() >= 10) mult = 1.75F;
                e.setAmount((int) Math.ceil(e.getAmount() * mult));
            }
        }
    }
    
    @EventHandler(priority = EventPriority.HIGH)
    public void clanDamageEntityEvents(EntityDamageByEntityEvent e) {
        if (e.isCancelled()) return;
        
        if (e.getDamager() instanceof Player && !(e.getEntity() instanceof Player)) {
            Player p = (Player) e.getDamager();
            Clan clan = RegionHandler.getClan(p);
            if (clan != null && clan.getLevel() > 6 /*&& clan.inRegion(p)*/) {
                int bonus = 1;
                if (clan.getLevel() >= 13) bonus = 2;
                e.setDamage(e.getDamage() + bonus);
            }
        }
    }
    
    @EventHandler(priority = EventPriority.HIGH)
    public void clanDamageEvents(EntityDamageEvent e) {
        if (e.isCancelled()) return;
        
        if (e.getEntity() instanceof Player) {
            Player p = (Player) e.getEntity();
            Clan clan = RegionHandler.getClan(p);
            if (clan != null && clan.getLevel() > 1 && clan.inRegion(p)) {
                float reduction = 0.9F;
                int L = clan.getLevel();
                    if (L >= 11) reduction = 0.6F;
                    else if (L >= 5) reduction = 0.75F;
                e.setDamage(e.getDamage() * reduction);
            }
        }
    }
    
    @EventHandler(priority = EventPriority.HIGH)
    public void clanFoodEatEvent(FoodLevelChangeEvent e) {
        Player p = (Player) e.getEntity();
        Clan clan = RegionHandler.getClan(p);
        if (clan != null && clan.getLevel() > 5 /*&& clan.inRegion(p)*/) {
            int bonus = 1;
            if (clan.getLevel() >= 12) bonus = 2;
            e.setFoodLevel(e.getFoodLevel() + bonus);
        }
    }
    
    @EventHandler(priority = EventPriority.HIGH)
    public void clanHealEvent(EntityRegainHealthEvent e) {
        if (e.getEntity() instanceof Player && e.getRegainReason() == RegainReason.SATIATED) {
            Player p = (Player) e.getEntity();
            Clan clan = RegionHandler.getClan(p);
            if (clan != null && clan.getLevel() > 7 && clan.inRegion(p)) {
                float bonus = (clan.getLevel() >= 14) ? 2.0F : 1.5F;
                e.setAmount(e.getAmount() * bonus);
            }
        }
    }
    
    @EventHandler(priority = EventPriority.HIGH)
    public void clanFurnaceEvent(FurnaceSmeltEvent e) {
        Block b = e.getBlock();
        Clan clan = RegionHandler.getClan(b.getLocation().getChunk());
        if (clan != null && clan.getLevel() > 0 && ClanPerks.isDoubleSmelt(e.getSource().getType())) {
            int L = clan.getLevel();
            boolean doubled = false;
                if (L == 15) doubled = Math.random() < .5;
                else if (L >= 9) doubled = Math.random() < .25;
                else if (L >= 3) doubled = Math.random() < .12;
                else doubled = Math.random() < .06;
            if (doubled) {
                e.getResult().setAmount(Math.min(e.getResult().getAmount() + 1, 64));
                ParticleEffects.FLAME.display(0.5F, 0.5F, 0.5F, 0.07F, 18, b.getLocation().add(0.5, 0.5, 0.5), 257);
            }
        }
    }
    
    @EventHandler
    public void clanResourceInterface(InventoryClickEvent e) {
        if (e.getInventory().getName().equals("Clan Resources"))
            e.setCancelled(true);
        if (e.getCurrentItem() == null || e.getCurrentItem().getType() == Material.AIR)
            return;

        if (e.getClickedInventory().getName().equals("Clan Resources")) {
            Player p = (Player) e.getWhoClicked();
            Clan c = RegionHandler.getClan(p);
            if (c != null) {
                if (e.isRightClick()) {
                    c.withdrawResource(p, e.getSlot(), e.getCurrentItem().getType(), e.isShiftClick() ? 64 : 1);
                } else if (e.isLeftClick()) {
                    c.depositResource(p, e.getSlot(), e.getCurrentItem().getType(), e.isShiftClick() ? 64 : 1);
                }
            } else {
                p.closeInventory();
            }
        }
    }

}
