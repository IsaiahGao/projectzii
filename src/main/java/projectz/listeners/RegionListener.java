package projectz.listeners;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Tameable;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerBedLeaveEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.event.world.ChunkUnloadEvent;

import projectz.Main;
import projectz.Utilities;
import projectz.clans.Clan;
import projectz.clans.RegionHandler;
import projectz.tasks.DepletionTaskCycler.StatType;
import utilities.gui.ActionBar;

public class RegionListener implements Listener {
    
    public static long TIME_REFERENCE;
    
    static {
        Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(Main.getPlugin(), new Runnable() {
            public void run() {
                TIME_REFERENCE = Main.getOverworld().getTime();
            }
        }, 100L, 100L);
    }
    
    @EventHandler(priority = EventPriority.MONITOR)
    public void checkRegionChange(PlayerMoveEvent e) {
        if (e.getFrom().getChunk().equals(e.getTo().getChunk()))
            return;
        Chunk c = e.getTo().getChunk();
        Chunk cf = e.getFrom().getChunk();
        if (RegionHandler.claimedChunks.containsKey(c)) {
            Clan clan = RegionHandler.claimedChunks.get(c);
            Clan clanb4 = RegionHandler.claimedChunks.get(cf);
            if (clanb4 == null || !clan.equals(clanb4)) {
                if (clan.isMember(e.getPlayer())) {
                    new ActionBar().sendMessage(e.getPlayer(), ChatColor.DARK_BLUE + "�lClan > �r�7Entering Home Turf!");
                } else
                    new ActionBar().sendMessage(e.getPlayer(),ChatColor.DARK_BLUE + "�lClan > �r�7Entering �b" + clan.getName() + "�7 Clan's territory!");
            }
        } else {
            if (RegionHandler.claimedChunks.containsKey(cf)) {
                new ActionBar().sendMessage(e.getPlayer(),ChatColor.DARK_BLUE + "�lClan > �r�7Entering Wilderness.");
            }
        }
    }
    
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void disableFriendlyFire(EntityDamageByEntityEvent e) {
        if (e.getEntity() instanceof Player) {
            Player p = (Player) e.getEntity();
            if (RegionHandler.members.containsKey(p.getUniqueId())) {
                Clan c = RegionHandler.members.get(p.getUniqueId()).getClan();
                LivingEntity damager = Utilities.getActualDamager(e.getDamager());
                if (damager instanceof Player && c.isMember((Player) damager))
                    e.setCancelled(true);
            }
        } else if (e.getEntity() instanceof Tameable && Utilities.getActualDamager(e.getDamager()) instanceof Player) {
            Tameable tameable = (Tameable) e.getEntity();
            if (tameable.getOwner() != null) {
                Player p = (Player) Utilities.getActualDamager(e.getDamager());
                Clan wolfClan = RegionHandler.getClan(Bukkit.getOfflinePlayer(tameable.getOwner().getUniqueId()));
                if (wolfClan != null && RegionHandler.getClan(p) != null && wolfClan.equals(RegionHandler.getClan(p))) {
                    p.sendMessage(ChatColor.BLUE + "Clan > �4This is a clanmate's animal. Don't attack it!");
                    e.setCancelled(true);
                }
            }
        }
    }
    
    @EventHandler(priority = EventPriority.MONITOR)
    public void chunkUnload(ChunkUnloadEvent e) {
        RegionHandler.unloadChunk(e.getChunk());
    }
    
    @EventHandler(priority = EventPriority.MONITOR)
    public void chunkLoad(ChunkLoadEvent e) {
        RegionHandler.reloadChunk(e.getChunk());
    }
    
    @EventHandler
    public void checkNightSkip(PlayerBedLeaveEvent e) {
        if (e.getBed().getWorld().getTime() - TIME_REFERENCE < 0) {
            StatType.SANITY.change(e.getPlayer(), 1000);
            if (StatType.WARMTH.get(e.getPlayer()) < 1000)
                StatType.WARMTH.set(e.getPlayer(), 1000, true);
        }
    }

}
