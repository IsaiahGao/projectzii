package projectz.listeners;

import java.util.HashSet;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.block.Dispenser;
import org.bukkit.block.Sign;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.PigZombie;
import org.bukkit.entity.Player;
import org.bukkit.entity.Skeleton;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.block.BlockSpreadEvent;
import org.bukkit.event.block.CauldronLevelChangeEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.event.entity.EntityCombustEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.EntityInteractEvent;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.entity.EntityTeleportEvent;
import org.bukkit.event.entity.ItemMergeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.inventory.FurnaceSmeltEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.inventory.AnvilInventory;
import org.bukkit.inventory.BrewerInventory;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.HorseInventory;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import cwhitelist.listeners.AnimalPetEvent;
import cwhitelist.soup.SoupHandler;
import projectz.ItemDef;
import projectz.Main;
import projectz.TeleTabs.TeleTab;
import projectz.Utilities;
import projectz.Utilities.CoolDownReason;
import projectz.abilities.AbilityUtils;
import projectz.chests.ChestHandler;
import projectz.clans.Clan;
import projectz.clans.Member;
import projectz.clans.RegionHandler;
import projectz.food.CustomFoodHandler;
import projectz.food.CustomFoodHandler.CustomFood;
import projectz.food.ModifiedFoodHandler;
import projectz.handlers.CauldronHandler;
import projectz.handlers.VitalsHandler;
import projectz.listeners.events.BottleFillEvent;
import projectz.listeners.events.ClueScrollDigEvent;
import projectz.listeners.events.ClueScrollIncrementEvent;
import projectz.spells.SpellCreationHandler.SpellObject;
import projectz.spells.SpellInterface;
import projectz.tasks.BoardHandler;
import projectz.tasks.DepletionTaskCycler;
import projectz.tasks.DepletionTaskCycler.StatType;
import projectz.tasks.ParticleTasks;
import projectz.tasks.PersonalBoard;
import projectz.treasure.CasketHandler;
import projectz.treasure.ClueScrollHandler.ClueScrollType;
import projectz.treasure.cluescrolls.ClueScroll;
import shadowrealm.augments.AugmentEnums.Augment;
import utilities.Utils;
import utilities.cooldowns.CooldownHandler;
import utilities.particles.ParticleEffects;
import utilities.particles.ParticleEffects.ItemData;
import utilities.particles.ParticleEffects.OrdinaryColor;

public class InteractListener implements Listener {
    
    @EventHandler
    public void blockCheezProtect(PlayerCommandPreprocessEvent e) {
        if (e.getMessage().startsWith("/claim"))
            e.setCancelled(true);
    }
    
    @EventHandler
    public void petAnimal(AnimalPetEvent e) {
        if (CooldownHandler.cooledDown(e.getPlayer(), "petAnimal", 5000)) {
            StatType.SANITY.change(e.getPlayer(), 1.5F);
        }
    }
    
    @EventHandler
    public void onPlayerLogin(PlayerJoinEvent e) {
        final Player p = e.getPlayer();
        if (Main.joined.getScore(p.getName()).getScore() == 0) {
            Main.joined.getScore(p.getName()).setScore(1);
            p.sendMessage("�fUse �e�l/commands �ffor commands and �e�l/help �ffor help pages!");
        }
        VitalsHandler.loadVitals(p);
        
        new BukkitRunnable() {
            @Override
            public void run() {
                new PersonalBoard(p);
            }
        }.runTaskLater(Main.getPlugin(), 60L);
    }
    
    @EventHandler
    public void blockBurn(BlockBurnEvent e) {
        e.setCancelled(true);
    }
    
    @EventHandler
    public void noFireSpread(BlockSpreadEvent e) {
        if(e.getNewState().getType() == Material.FIRE) {
            e.getNewState().setType(Material.AIR);
        }
    }
    
    @EventHandler
    public void onPlayerLogout(PlayerQuitEvent e) {
        Player p = e.getPlayer();
        //e.setQuitMessage(ChatColor.DARK_GRAY + p.getName() + " left.");
        VitalsHandler.save(p);
        BoardHandler.removeScoreboard(p);
        Main.getCycler().removePlayerTask(p);
        if (p.hasMetadata("bleed")) {
            p.removeMetadata("bleed", Main.getPlugin());
        }
    }
    
    @EventHandler
    public void onItemPickup(EntityPickupItemEvent e) {
        if (e.getItem().hasMetadata("unpickupable") || e.getEntity().hasMetadata("nopickup"))
            e.setCancelled(true);
        if (e.getItem().hasMetadata("owner") && !e.getItem().getMetadata("owner").get(0).asString().equals(e.getEntity().getName()))
            e.setCancelled(true);
    }
    
    @EventHandler
    public void onItemMerge(ItemMergeEvent e) {
        if (e.getEntity().hasMetadata("unpickupable"))
            e.setCancelled(true);
    }
    
    @EventHandler
    public void itemClick(InventoryClickEvent e) {
    Inventory inv = e.getInventory();
    if (e.getCurrentItem() == null || e.getCurrentItem().getType() == Material.AIR) {
        return;
    }
        if (inv instanceof HorseInventory) {
            ItemStack i = e.getCurrentItem();
            if (i.containsEnchantment(Utils.glow))
                e.setCancelled(true);
        } else if (inv instanceof AnvilInventory) {
            if (ClueScroll.isClueScroll(e.getCurrentItem())) {
                e.getWhoClicked().sendMessage(ChatColor.RED + "You can't rename your clue scroll.");
                e.setCancelled(true);
                return;
            }
            if (e.getCurrentItem().getType() == Material.BANNER)
                e.setCancelled(true);
        } else if (inv.getHolder() instanceof Dispenser) {
            if (e.getCurrentItem().getType() == Material.MONSTER_EGG && e.getWhoClicked().getGameMode() != GameMode.CREATIVE)
                e.setCancelled(true);
        } else if (inv instanceof BrewerInventory) {
            if (e.getCurrentItem().getType() == Material.NETHER_WARTS && ChatColor.stripColor(Utilities.getLore(e.getCurrentItem(), 0)).equals("Edible"))
                e.setCancelled(true);
        } else if (inv.getName().equals("Spells (" + e.getWhoClicked().getEntityId() + ")")) {
            if (e.getCurrentItem().getType() == Material.EXP_BOTTLE) {
                e.setCancelled(true);
                return;
            }
            e.setCancelled(true);
            SpellObject tomake = SpellObject.getSpell(e.getCurrentItem().getType());
            Player clicker = (Player) e.getWhoClicked();
                tomake.createSpell(clicker);
        } else if (inv.getName().equals("Stats (" + e.getWhoClicked().getEntityId() + ")")) {
            e.setCancelled(true);
        }
    }
    
    @EventHandler
    public void onPlayerClickBlock(PlayerInteractEvent e) {
        if (e.getAction().toString().contains("_CLICK_BLOCK")) {
            Player p = e.getPlayer();
            Block b = e.getClickedBlock();
            if (b.hasMetadata("casket.holder")) {
                String name = b.getMetadata("casket.holder").get(0).asString();
                if (!p.getName().equals(name)) {
                    p.sendMessage(ChatColor.RED + "That is " + name + "'s treasure chest. You can't just steal it!");
                    p.playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 1.0F, 1.6F);
                    e.setCancelled(true);
                    return;
                }
                b.removeMetadata("casket.holder", Main.getPlugin());
                CasketHandler.getCasketType(b.getType()).openChest(b, p);
                e.setCancelled(true);
            }
        }
        if (e.getAction().toString().contains("RIGHT_CLICK_")) {
            Player p = e.getPlayer();
            ItemStack i = p.getItemInHand();
            if (i.getType().toString().contains("CHEST")) {
                if (i.getItemMeta().getLore() != null && i.getItemMeta().getLore().size() > 1) {
                    if (i.getItemMeta().getLore().get(1).equals(ChatColor.GRAY + "I wonder what's inside?")) {
                        if (CasketHandler.getCasketType(i.getType()).dropChest(p)) 
                            Utilities.removeItem(p);
                        e.setCancelled(true);
                    }
                }
            }
            if (i != null) {
                if (i.getType() == Material.FIREWORK && i.containsEnchantment(Utils.glow))
                    e.setCancelled(true);
                else if (i.getType() == Material.BANNER && i.containsEnchantment(Utils.glow))
                    e.setCancelled(true);
            }
        }
        if (e.getAction().equals(Action.PHYSICAL) && e.getClickedBlock().getType() == Material.SOIL) {
            e.setCancelled(true);
        }
    }
    
    @EventHandler
    public void disableCropTrample(EntityInteractEvent e) {
        if ((e.getEntity() instanceof Creature) && e.getBlock().getType() == Material.SOIL) {
            
            e.setCancelled(true);
        }
    }
    
    @EventHandler
    public void cauldronFill(CauldronLevelChangeEvent e) {
        switch (e.getReason()) {
        case BOTTLE_EMPTY:
            break;
        case BUCKET_EMPTY:
            CauldronHandler.contaminate(e.getBlock());
            break;
        case EXTINGUISH:
        case ARMOR_WASH:
        case BANNER_WASH:
            CauldronHandler.contaminate(e.getBlock());
        case UNKNOWN:
        case EVAPORATE:
        case BUCKET_FILL:
        default:
            if (e.getNewLevel() == 0)
                CauldronHandler.decontaminate(e.getBlock());
            break;
        }
    }
    
    @EventHandler
    public void bottleFill(BottleFillEvent e) {
        Block b = e.getBlock();
        Player p = e.getPlayer();
        
        e.setCancelled(true);
        if (b.getData() != 0)
            return;
        Utilities.removeItem(p);
        if (Main.random(Utilities.isDesert(p.getLocation().getBlock()) ? 1 : 3) == 0) b.setType(Material.AIR);
        if (Utilities.isDesert(b))
            Utils.giveItem(p, ItemDef.hotWater.clone());
        else if (Utilities.isCold(b))
            Utils.giveItem(p, ItemDef.coldWater.clone());
        else {
            switch (b.getBiome()) {
            case SWAMPLAND:
            case MUTATED_SWAMPLAND:
                Utils.giveItem(p, ItemDef.filthyWater.clone());
                break;
            case OCEAN:
            case DEEP_OCEAN:
            case BEACHES:
            case STONE_BEACH:
            case COLD_BEACH:
                Utils.giveItem(p, ItemDef.saltWater.clone());
                break;
            default:
                // TODO dirty water?
                Utils.giveItem(p, ItemDef.dirtyWater.clone());
                break;
            }
        }
        p.updateInventory();
    }
    
    @SuppressWarnings("deprecation")
    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e) {
        if (e.getClickedBlock() != null && e.getClickedBlock().getType() == Material.BED_BLOCK) {
            if (ParticleTasks.isSweaty(e.getPlayer())) {
                e.setCancelled(true);
                e.getPlayer().sendMessage("�7You're too sweaty to sleep.");
                e.getPlayer().playSound(e.getPlayer().getLocation(), Sound.BLOCK_CHEST_LOCKED, 0.6F, 0.5F);
            }
        }
        
        if (e.getItem() == null)
            return;
        final Player p = e.getPlayer();
        switch (e.getItem().getType()) {
            case WOOL:
                if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK) || e.getAction().equals(Action.RIGHT_CLICK_AIR)) {
                    if (Utils.compareLore(e.getItem(), ItemDef.towel_dry)) {
                        e.setCancelled(true);
                        if (Utilities.cooledDown(p, CoolDownReason.DRINK, 2))
                            if (ParticleTasks.getSoak(p) > 0) {
                                Utils.removeItem(p);
                                Utils.giveItem(p, ItemDef.towel_wet.clone());
                                p.sendMessage("�7You dry yourself with the towel.");
                                ParticleTasks.dryOff(p);
                                p.playSound(p.getLocation(), Sound.BLOCK_PISTON_EXTEND, 0.4F, 0.4F);
                            } else {
                                p.sendMessage("�7You do not need drying off right now.");
                            }
                    } else if (Utils.compareLore(e.getItem(), ItemDef.towel_wet)) {
                        e.setCancelled(true);
                    }
                }
                break;
            case BOWL:
                if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK) || e.getAction().equals(Action.RIGHT_CLICK_AIR)) {
                    for (final Block b : p.getLineOfSight(null, 4)) {
                        if (!b.getType().equals(Material.STATIONARY_WATER) && !Utils.passable(b)) break;
                        if (b.getType().equals(Material.STATIONARY_WATER)) {
                            if (Utilities.cooledDown(p, CoolDownReason.DRINK, 3)) {
                                p.getWorld().playSound(p.getLocation(), Main.random(1) == 0 ? Sound.ENTITY_GENERIC_SPLASH : Sound.ENTITY_PLAYER_SPLASH, 0.5F, 1.0F);
                                new BukkitRunnable() {
                                    @Override
                                    public void run() {
                                        if (p != null) {
                                            p.getWorld().playSound(p.getLocation(), Sound.ENTITY_GENERIC_DRINK, 0.5F, 1.0F);
                                            
                                            if (Utilities.isDesert(b)) StatType.WARMTH.change(p, 0.5F);
                                            else if (Utilities.isCold(b)) StatType.WARMTH.change(p, -0.5F);
                                            
                                            StatType.HYDRATION.change(p, 8);
                                            if (Math.random() < 0.5) {
                                                p.addPotionEffect(new PotionEffect(PotionEffectType.HUNGER, 400, 0));
                                            }
                                            if (Main.random(Utilities.isDesert(p.getLocation().getBlock()) ? 2 : 6) == 0) b.setType(Material.AIR);
                                        }
                                    }
                                }.runTaskLater(Main.getPlugin(), 10L);
                                p.sendMessage(ChatColor.GRAY + "You scoop up some water with your bowl and drink it.");
                            }
                        break;
                        }
                    }
                }
                break;
            case FIREWORK_CHARGE:
                if (!e.getAction().equals(Action.PHYSICAL)) {
                    SpellObject spell = SpellObject.getSpell(e.getItem());
                    if (spell != null) {
                        spell.getSpell().activate(p, p.getItemInHand().containsEnchantment(Enchantment.ARROW_DAMAGE));
                        Utilities.removeItem(p);
                    }
                }
                break;
            /*case ENCHANTED_BOOK:
                if (e.getItem().getItemMeta().getLore().get(0).equals(ChatColor.RESET + "Right click to gain XP!")) {
                    List<String> lore = e.getItem().getItemMeta().getLore();
                    Utilities.removeItem(p);
                    Skill skill = Skill.valueOf(ChatColor.stripColor(lore.get(1).split(": ")[1].toUpperCase()));
                    int amount = Integer.parseInt(ChatColor.stripColor(lore.get(2).split(": ")[1]));
                    skill.addXP(p, amount);
                    p.getWorld().playSound(p.getLocation(), Sound.LEVEL_UP, 1.0F, 0.6F);
                    p.sendMessage(ChatColor.YELLOW + "Gained " + amount + " experience in " + Main.capitalizeFirst(skill.toString()) + "!");
                }
                break;*/
            case BLAZE_ROD:
                if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
                    if (e.getClickedBlock().getType().equals(Material.CAULDRON)) {
                        if (SoupHandler.getSoup(e.getClickedBlock()) != null) {
                            p.sendMessage(ChatColor.GRAY + "This cauldron has soup in it. Go elsewhere to make spells.");
                            return;
                        }
                            if (e.getClickedBlock().getData() == 3) {
                                SpellInterface.openSpellInventory(p);
                                p.getWorld().playSound(p.getLocation(), Sound.BLOCK_CHEST_OPEN, 1.0F, 1.6F);
                            } else {
                                p.sendMessage(ChatColor.GRAY + "I must fill this cauldron with water before I perform witchcraft.");
                            }
                    }
                }
                break;
            case GLASS_BOTTLE:
                if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
                    if (e.getClickedBlock().getType().equals(Material.CAULDRON) && e.getClickedBlock().getData() > 0) {
                        if (SoupHandler.getSoup(e.getClickedBlock()) == null) {
                            e.setCancelled(true);
                            Utils.removeItem(p, e.getHand());
                            Utils.giveItem(p, CauldronHandler.isContaminated(e.getClickedBlock()) ? ItemDef.dirtyWater.clone() : new ItemStack(Material.POTION));
                            e.getClickedBlock().setData((byte) (e.getClickedBlock().getData() - 1));
                            
                            if (e.getClickedBlock().getData() == 0)
                                CauldronHandler.decontaminate(e.getClickedBlock());
                            return;
                        }
                    }
                }
                
                // TODO replaced with bottle fill event
                /*if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK) || e.getAction().equals(Action.RIGHT_CLICK_AIR))
                    for (Block b : p.getLineOfSight(null, 5)) {
                        switch (b.getType()) {
                        case STATIONARY_WATER:
                        case WATER:
                            e.setCancelled(true);
                            if (b.getData() != 0)
                                return;
                            Utilities.removeItem(p);
                            if (Main.random(Utilities.isDesert(p.getLocation().getBlock()) ? 1 : 3) == 0) b.setType(Material.AIR);
                            if (Utilities.isDesert(b))
                                Utils.giveItem(p, ItemDef.hotWater.clone());
                            else if (Utilities.isCold(b))
                                Utils.giveItem(p, ItemDef.coldWater.clone());
                            else {
                                switch (b.getBiome()) {
                                case SWAMPLAND:
                                case MUTATED_SWAMPLAND:
                                    Utils.giveItem(p, ItemDef.filthyWater.clone());
                                    break;
                                case OCEAN:
                                case DEEP_OCEAN:
                                case BEACHES:
                                case STONE_BEACH:
                                case COLD_BEACH:
                                    Utils.giveItem(p, ItemDef.saltWater.clone());
                                    break;
                                default:
                                    // TODO dirty water?
                                    Utils.giveItem(p, ItemDef.dirtyWater.clone());
                                    break;
                                }
                            }
                            p.updateInventory();
                            break;
                            default:
                                if (!Utils.passable(b))
                                    return;
                                break;
                        }
                    }*/
                break;
            case SPONGE:
            case NETHER_STALK:
            case MELON_SEEDS:
                if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK) || e.getAction().equals(Action.RIGHT_CLICK_AIR)) {
                    if (e.getItem().getItemMeta().getLore() != null && e.getItem().getItemMeta().getLore().get(0).equals(ChatColor.GRAY + "Edible")) {
                    e.setCancelled(true);
                        if (p.getFoodLevel() == 20) return;
                        CustomFood food = CustomFoodHandler.getFood(e.getItem().getType());
                        food.eat(p);
                    }
                }
                break;
            case STEP:
                if (!e.getAction().equals(Action.PHYSICAL)) {
                    TeleTab tab = TeleTab.getTab(e.getItem());
                    if (tab != null) {
                        Location to = null;
                        switch (tab) {
                        case HOME:
                            to = Utilities.getSafeBedSpawnLocation(p);
                            break;
                        case CLAN:
                            if (!RegionHandler.members.containsKey(p.getUniqueId())) {
                                p.sendMessage(ChatColor.RED + "You are not in a Clan and therefore have no Clan Hub.");
                                p.playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 1.0F, 1.6F);
                                return;
                            }
                            Clan c = RegionHandler.members.get(p.getUniqueId()).getClan();
                            to = c.getClanSpawn();
                            break;
                        case SPAWN:
                            to = Main.getOverworld().getHighestBlockAt(Main.getOverworld().getSpawnLocation()).getLocation().add(0,1,0);
                            break;
                        }
                        e.setCancelled(true);
                        if (p.hasPotionEffect(PotionEffectType.SLOW)) {
                            p.sendMessage(ChatColor.RED + "You cannot teleport while slowed!");
                            p.playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 1.0F, 1.6F);
                            return;
                        }
                        final Location tpto = to;
                        if (tpto != null) {
                            Utilities.removeItem(p);
                            p.getWorld().playEffect(p.getLocation(), Effect.STEP_SOUND, Material.GLASS);
                            if (!tpto.getChunk().isLoaded()) tpto.getChunk().load();
                            new BukkitRunnable() {
                                @Override
                                public void run() {
                                    if (p != null) {
                                        p.teleport(tpto);
                                        p.getWorld().playSound(tpto, Sound.BLOCK_PORTAL_TRAVEL, 0.6F, 1.4F);
                                        p.setFallDistance(0F);
                                        p.setFireTicks(0);
                                    }
                                }
                            }.runTaskLater(Main.getPlugin(), 10L);
                        } else {
                            p.sendMessage(ChatColor.RED + "Your Clan does not currently have a Hub.");
                            p.playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 1.0F, 1.6F);
                        }
                    }
                }
                break;
            /*case CARROT_ITEM:
            case POTATO_ITEM:
            case APPLE:
            case MELON:
            case PUMPKIN:
            case SUGAR:
            case BROWN_MUSHROOM:
            case RED_MUSHROOM:
            case LONG_GRASS:
            case LEAVES:
            case LEAVES_2:
            case RAW_BEEF:
            case PORK:
            case RABBIT:
            case RAW_CHICKEN:
            case MUTTON:
            case RAW_FISH:
            case EGG:
            case BONE:
            case WHEAT:
            case ROTTEN_FLESH:
            case SULPHUR:
            case SPIDER_EYE:
            case COBBLESTONE:
                if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
                    Block b = e.getClickedBlock();
                    Clan clan = RegionHandler.getClan(b.getChunk());
                    if (clan != null) {
                        Member mem = RegionHandler.members.get(p.getUniqueId());
                        if (mem == null || !clan.isMember(p)) {
                            p.sendMessage(ChatColor.BLUE + "Clan > �4You cannot screw with �c" + clan.getName() + "�4 Clan's soup bowl!");
                            e.setCancelled(true);
                            return;
                        }
                    }
                    if (!b.getType().equals(Material.CAULDRON) || b.getData() != 3) return;
                        if (!b.getRelative(0, -1, 0).getType().equals(Material.FIRE)) {
                            p.sendMessage(ChatColor.GRAY + "Light a fire under the cauldron first.");
                            e.setCancelled(true);
                            return;
                        }
                            Material mat = e.getItem().getType();
                            SoupBowl bowl = Main.getSoupHandler().getSoupBowl(b);
                            e.setCancelled(true);
                            if (bowl.addIngredient(mat)) {
                                Utilities.removeItem(p);
                                p.sendMessage(ChatColor.GRAY + "You add the " + mat.toString().toLowerCase().replace("_item", "").replace("_", " ") + " to the stew pot.");
                                p.getWorld().playSound(b.getLocation(), Sound.SPLASH, 1.0F, 1.0F);
                                ParticleEffects.ITEM_CRACK.display(new ItemData(mat, (byte) 0), 0, 0, 0, 0.01F, 8, b.getLocation().add(0.5, 1.2, 0.5), 256);
                            } else {
                                p.sendMessage(ChatColor.GRAY + "The stew pot already has that ingredient.");
                            }
                }
                break;*/
            case IRON_BARDING:
            case GOLD_BARDING:
            case DIAMOND_BARDING:
                ItemStack i = e.getItem();
                if (i.getItemMeta().getLore() != null) {
                    switch (e.getAction()) {
                    case RIGHT_CLICK_BLOCK:
                    case RIGHT_CLICK_AIR:
                        if (!Utilities.cooledDown(p, CoolDownReason.DART, 1)) return;
                        if (!p.getInventory().containsAtLeast(ItemDef.dart, 1)) {
                            return;
                        }
                        p.getInventory().removeItem(Utils.setItemAmount(ItemDef.dart, 1));
                        p.updateInventory();
                        Arrow dart = p.launchProjectile(Arrow.class);
                        dart.setVelocity(p.getLocation().getDirection().normalize().multiply(2.0));
                        dart.setMetadata("dart", new FixedMetadataValue(Main.getPlugin(), i.getType().equals(Material.DIAMOND_BARDING) ? 1 : 0));
                        if (i.getType().equals(Material.GOLD_BARDING)) dart.setFireTicks(120);
                        p.getWorld().playSound(p.getLocation(), Sound.ENTITY_CAT_HISS, 0.2F, 2.0F);
                        break;
                    /*case LEFT_CLICK_BLOCK:
                    case LEFT_CLICK_AIR:
                        if (i.getType().toString().contains("_BARDING") && p.isSneaking()) {
                            int darts = Main.getDartAmount(p.getInventory());
                            if (darts <= 0) { p.sendMessage(ChatColor.GRAY + "You have no darts to load!"); return; }
                            int totalAmmo = Integer.parseInt(ChatColor.stripColor(i.getItemMeta().getLore().get(0)).split(" ")[1]);
                            p.getInventory().removeItem(Utilities.setItemAmount(ItemDef.dart, darts));
                                ItemMeta meta = i.getItemMeta();
                                List<String> lores = meta.getLore();
                                lores.set(0, ChatColor.GRAY + "Ammo: " + ChatColor.YELLOW + (totalAmmo + darts));
                                meta.setLore(lores);
                                i.setItemMeta(meta);
                                    p.updateInventory();
                                    p.sendMessage(ChatColor.GRAY + "You load " + darts + " darts into the blowpipe.");
                                    p.playSound(p.getLocation(), Sound.CLICK, 1.0F, 0.5F);
                        }
                        break;*/
                    default:
                        break;
                    }
                }
                break;
            case EMPTY_MAP:
                if (!e.getAction().equals(Action.PHYSICAL)) {
                    e.setCancelled(true);
                    e.getItem().setType(Material.EMPTY_MAP);
                    p.sendMessage(ChatColor.GRAY + "Maps are for panzies. You don't need maps.");
                    p.playSound(p.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0F, 1.0F);
                }
                break;
            default:
                break;
        }
    }
    
    @EventHandler
    public void interactEntity(PlayerInteractEntityEvent e) {
        Player p = e.getPlayer();
        Entity ent = e.getRightClicked();
        if (Utilities.protectedMobs.contains(ent.getType())) {
            Clan clan = RegionHandler.getClan(ent.getLocation().getChunk());
            if (clan != null) {
                Member mem = RegionHandler.members.get(p.getUniqueId());
                if (mem == null || !clan.isMember(p)) {
                    p.sendMessage(ChatColor.BLUE + "Clan > �4You cannot screw with �c" + clan.getName() + "�4 Clan's animals!");
                    e.setCancelled(true);
                    return;
                }
            }
        }
        /*if (e.getRightClicked().getType().equals(EntityType.SHEEP) && item != null) {
            if (!item.containsEnchantment(Utils.glow) || !item.getType().equals(Material.INK_SACK)) return;
                p.sendMessage(ChatColor.GRAY + "You poison the sheep. Douche.");
                new PotionEffect(PotionEffectType.WITHER, 500, 20, false).apply((LivingEntity) e.getRightClicked());
        }*/
    }
    
    @EventHandler
    public void onProjectileLand(final ProjectileHitEvent e) {
        if (e.getEntity().hasMetadata("dart")) {
            ParticleEffects.ITEM_CRACK.display(new ItemData(Material.ARROW, (byte) 0), 0, 0, 0, 0.08F, 8, e.getEntity().getLocation(), 256);
            e.getEntity().remove();
        }
    }
    
    @EventHandler
    public void fixTeleports(PlayerTeleportEvent e) {
        Player p = e.getPlayer();
        if (p.hasPotionEffect(PotionEffectType.SLOW) && !e.getCause().equals(TeleportCause.UNKNOWN)) {
            p.sendMessage(ChatColor.RED + "You cannot teleport while slowed!");
            p.playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_IRON_DOOR, 1.0F, 1.6F);
            e.setCancelled(true);
            if (e.getCause().equals(TeleportCause.ENDER_PEARL)) {
                p.getInventory().addItem(new ItemStack(Material.ENDER_PEARL, 1));
                p.updateInventory();
            }
            return;
        }
        if (!e.getTo().getChunk().isLoaded()) e.getTo().getChunk().load();
    }
    
    @EventHandler
    public void onMobTarget(EntityTargetEvent e) {
        if (e.getTarget() instanceof Player) {
            if (e.getTarget().hasMetadata("untargetable")) {
                e.setCancelled(true);
                return;
            }
            
            if (e.getEntity() instanceof Skeleton && e.getTarget() != null) {
                final Skeleton skele = ((Skeleton) e.getEntity());
                skele.launchProjectile(Arrow.class, Utilities.toTarget(skele, skele.getTarget()).normalize().multiply(2.7));
            }
        }
    }
    
    @EventHandler
    public void onChestOpen(InventoryOpenEvent e) {
    if (!Utilities.isChestInventory(e.getInventory())) return;
    if (e.getInventory().getHolder() instanceof Chest) {
        Chest chest = (Chest) e.getInventory().getHolder();
            if (ChestHandler.isLootable(chest)) {
                ChestHandler.claimLoot(chest);
            }
        }
    }
    
    @SuppressWarnings("deprecation")
    @EventHandler(ignoreCancelled = true)
    public void onBlockBreak(BlockBreakEvent e) {
        Clan clan = RegionHandler.getClan(e.getBlock().getChunk());
        Player p = e.getPlayer();
        Block b = e.getBlock();
        if (clan != null) {
            Member mem = RegionHandler.members.get(p.getUniqueId());
            if (mem == null || !clan.isMember(p)) {
                p.sendMessage(ChatColor.BLUE + "Clan > �4You cannot screw with �c" + clan.getName() + "�4 Clan's land!");
                e.setCancelled(true);
                return;
            }
        }
        switch (b.getType()) {
        case WALL_SIGN:
            if (ChestHandler.isChestSign((Sign) b.getState())) {
                p.sendMessage(ChatColor.RED + "You may not break that.");
                e.setCancelled(true);
            }
        break;
        case CHEST:
        case TRAPPED_CHEST:
            if (!b.getRelative(0, -2, 0).getType().equals(Material.WALL_SIGN)) return;
            if (ChestHandler.isChestSign((Sign) b.getRelative(0, -2, 0).getState())) {
                p.sendMessage(ChatColor.RED + "You may not break that.");
                e.setCancelled(true);
            }
        break;
        case LONG_GRASS:
        case DOUBLE_PLANT:
            if (Main.random(9) == 0)
                b.getWorld().dropItemNaturally(b.getLocation().add(0.5, 0.5, 0.5), ItemDef.berries.clone());
            break;
        case ICE:
            if (e.getPlayer().getInventory().getItemInMainHand().getType().toString().endsWith("_PICKAXE")) {
                b.setType(Material.AIR);
                b.getWorld().dropItem(b.getLocation().add(0.5, 0.5, 0.5), new ItemStack(Material.ICE));
            }
            break;
        case BREWING_STAND:
            if (b.getData() > 0)
                e.setCancelled(true);
            break;
        default:
            break;
        }
        if (!e.isCancelled() && p.getInventory().contains(Material.EMPTY_MAP)) {
            ClueScroll clue = ClueScroll.containsClueScoll(p.getInventory());
            if (clue != null) {
                Bukkit.getServer().getPluginManager().callEvent(new ClueScrollDigEvent(clue, p, b));
            }
        }
    }
    
    @EventHandler
    public void onPlayerTakeDamage(final EntityDamageEvent e) {
    if (e.getEntity().hasMetadata("invincibility")) {
        e.setCancelled(true);
    }
    if (e.getEntity().hasMetadata("noknockback")) {
        new BukkitRunnable() {
            @Override
            public void run() {
                e.getEntity().setVelocity(new Vector());
            }
        }.runTaskLater(Main.getPlugin(), 1L);
    }
    if (e.getEntity() instanceof Player) {
        Player p = (Player) e.getEntity();
        if (p.getHealth() - e.getFinalDamage() <= 0 && Utilities.checkResurrectionTalisman(p)) {
            e.setDamage(0);
            p.playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_DOOR_WOOD, 0.8F, 1.0F);
            p.playSound(p.getLocation(), Sound.BLOCK_PORTAL_TRAVEL, 0.5F, 2.0F);
            p.sendMessage(ChatColor.LIGHT_PURPLE + "Your Insignia of Resurrection was consumed, returning you to life!");
            p.sendMessage(ChatColor.GRAY + "You will not be able to use another one for 30 minutes.");
            p.setHealth(12);
            StatType.HYDRATION.change(p, 30);
            StatType.SANITY.change(p, 30);
            DepletionTaskCycler.homeostasis(p, 30);
            Utilities.doStreakEffect(p, 18, new OrdinaryColor(1, 110, 255));
            p.removeMetadata("bleed", Main.getPlugin());
            for (PotionEffectType pe : AbilityUtils.badeffects)
                if (p.hasPotionEffect(pe))
                    p.removePotionEffect(pe);
            return;
        }
        if (p.hasMetadata("delayed.teleporting")) {
            Bukkit.getServer().getScheduler().cancelTask(p.getMetadata("delayed.teleporting").get(0).asInt());
            p.sendMessage(ChatColor.RED + "Your teleport was cancelled because you took damage!");
            p.playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_DOOR_WOOD, 0.4F, 1.7F);
            p.removeMetadata("delayed.teleporting", Main.getPlugin());
        }
    }
    if (e.getCause().equals(DamageCause.FALL) && e.getEntity().hasMetadata("nofall")) {
        e.setCancelled(true);
    }
    if (!(e.getEntity() instanceof LivingEntity)) return;
        LivingEntity ent = (LivingEntity) e.getEntity();
        if (ent instanceof Player) {
            if (e.getCause().equals(DamageCause.FIRE) || e.getCause().equals(DamageCause.FIRE_TICK)) {
                StatType.WARMTH.change((Player) ent, 0.8F);
            } else if (e.getCause().equals(DamageCause.LAVA)) {
                StatType.WARMTH.change((Player) ent, 4.0F);
            }
        }
        if (ent.hasMetadata("vulnerability")) {
            ent.getWorld().playSound(ent.getLocation(), Sound.ENTITY_WITHER_SHOOT, 0.2F, 0.5F);
            e.setDamage(e.getDamage() + ent.getMetadata("vulnerability").get(0).asInt());
        }
    }
    
    @EventHandler
    public void onPlayerDrinkAndEat(PlayerItemConsumeEvent e) {
    final Player p = e.getPlayer();
    ItemStack item = e.getItem();
        switch (item.getType()) {
            case COOKED_BEEF: // COOKED_BEEF
            case COOKED_MUTTON: // COOKED_MUTTON
            case COOKED_RABBIT:
            case COOKED_CHICKEN:
                StatType.WARMTH.change(p, 0.5F);
                break;
            case POTION:
                p.setFireTicks(0);
                if (!ItemDef.drinkWater(p, item)) {
                    if (((PotionMeta) item.getItemMeta()).getBasePotionData().getType() == PotionType.WATER) {
                        StatType.HYDRATION.change(p, 25);
                        StatType.HYDRATION_SAT.change(p, 12);
                        StatType.WARMTH.change(p, -0.5F);
                    } else {
                        StatType.HYDRATION.change(p, 25);
                        StatType.HYDRATION_SAT.change(p, 12);
                    }
                }
                
                if (ModifiedFoodHandler.isModifiedFood(item)) {
                    ModifiedFoodHandler.getFood(item).eat(p);
                }
                break;
            case MELON:
                StatType.HYDRATION.change(p, 1.5F);
                break;
            case MILK_BUCKET:
                StatType.HYDRATION.change(p, 25);
                StatType.HYDRATION_SAT.change(p, 7);
                break;
            case MUSHROOM_SOUP:
            case GRILLED_PORK:
            case BAKED_POTATO:
                StatType.WARMTH.change(p, 0.6F);
            case PUMPKIN_PIE:
            case RABBIT_STEW:
            case BREAD:
                if (ModifiedFoodHandler.isModifiedFood(item)) {
                    e.setCancelled(true);
                    ModifiedFoodHandler.getFood(item).eat(p);
                }
                break;
        default:
            break;
        }
    }
    
    @EventHandler
    public void onEntitySpawn(CreatureSpawnEvent e) {
    LivingEntity mob = e.getEntity();
        switch (mob.getType()) {
        case SPIDER:
            applyEffectLater(mob, Utilities.spiderEffects[Main.random(Utilities.spiderEffects.length - 1)], 0);
            break;
        case ZOMBIE:
        case ZOMBIE_VILLAGER:
        case HUSK:
            applyEffectLater(mob, PotionEffectType.SPEED, Main.random(2));
            break;
        case CREEPER:
            if (Math.random() < 0.2)
                ((Creeper) mob).setPowered(true);
            break;
        case ENDERMITE:
            if (e.getSpawnReason() != SpawnReason.EGG && e.getSpawnReason() != SpawnReason.DISPENSE_EGG) e.setCancelled(true);
            break;
        case PIG_ZOMBIE:
            if (Math.random() > 0.5)
                applyEffectLater(mob, PotionEffectType.SPEED, 0);
            ((PigZombie) mob).setAngry(true);
            break;
        case SQUID:
            if (Math.random() < 0.1 && !RegionHandler.isClaimed(mob.getLocation().getChunk())) {
                mob.getWorld().spawnEntity(mob.getLocation(), EntityType.GUARDIAN);
                mob.remove();
            }
            break;
        default:
            break;
        }
    }
    
    @EventHandler
    public void onPlayerDamage(EntityDamageByEntityEvent e) {
    if (e.getEntity().hasMetadata("invincibility")) {
        e.setCancelled(true);
    }
    if (!e.isCancelled()) {
        if (e.getDamager() instanceof Arrow && e.getEntity() instanceof LivingEntity) {
            Arrow arrow = (Arrow) e.getDamager();
            if (arrow.hasMetadata("dart")) {
                switch (arrow.getMetadata("dart").get(0).asInt()) {
                case 1:
                    if (Math.random() <= 0.33)
                        new PotionEffect(PotionEffectType.POISON, 160, 0, false).apply((LivingEntity) e.getEntity());
                    e.setDamage(4 + 2 * Math.random());
                    break;
                case 2:
                    if (Math.random() <= 0.33)
                        e.getEntity().setFireTicks(60);
                    e.setDamage(4 + 2 * Math.random());
                    break;
                default:
                    e.setDamage(3 + 2 * Math.random());
                    break;
                }
            }
        }
    }
    if (e.getEntity() instanceof Player) {
    if (e.isCancelled()) return;
    Player p = (Player) e.getEntity();
    
        switch (e.getDamager().getType()) {
        case CREEPER:
            p.sendMessage(ChatColor.GRAY + "You are stunned by the explosion!");
            p.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 200, 3));
            break;
        case SPIDER:
        case CAVE_SPIDER:
            if (Math.random() < 0.5) {
                p.playSound(p.getLocation(), Sound.ENTITY_SILVERFISH_AMBIENT, 0.5F, 1.5F);
                p.sendMessage(ChatColor.GRAY + "You've been poisoned!");
                p.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 200, 0));
            }
            if (Math.random() < 0.1) {
                cwhitelist.Utilities.applyBleed(p, 3);
            }
            break;
        case ZOMBIE:
        case ZOMBIE_VILLAGER:
        case HUSK:
        case WITHER_SKELETON:
        case PIG_ZOMBIE:
            if (Math.random() < 0.25) {
                p.playSound(p.getLocation(), Sound.ENTITY_HUSK_AMBIENT, 0.5F, 1.5F);
                p.sendMessage(ChatColor.GRAY + "You've been afflicted!");
                p.addPotionEffect(new PotionEffect(PotionEffectType.HUNGER, 100, 1));
            }
            if (Math.random() < 0.15) {
                cwhitelist.Utilities.applyBleed(p, 4);
            }
            break;
        case ARROW:
            Arrow a = (Arrow) e.getDamager();
            if (!(a.getShooter() instanceof Skeleton)) return;
                int armorcount = 0;
                for (ItemStack i : ((LivingEntity) a.getShooter()).getEquipment().getArmorContents()) {
                    if (i == null || i.getType().equals(Material.AIR))
                        continue;
                armorcount++;
                }
                if (Main.random(armorcount) >= 3 ) {
                    p.playSound(p.getLocation(), Sound.ENTITY_WITHER_SHOOT, 0.5F, 2.0F);
                    p.sendMessage(ChatColor.GRAY + "You've been blinded!");
                    p.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 100, 1));
                }
                if (Main.random(3) == 0) {
                    p.playSound(p.getLocation(), Sound.ENTITY_SILVERFISH_AMBIENT, 0.5F, 1.5F);
                    p.sendMessage(ChatColor.GRAY + "That was a poison arrow!");
                    p.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 100, 0));
                }
                if (Math.random() < 0.1) {
                    cwhitelist.Utilities.applyBleed(p, 3);
                }
            break;
        case ENDERMAN:
            StatType.SANITY.change(p, -3);
            if (Math.random() < 0.15) {
                cwhitelist.Utilities.applyBleed(p, 4);
            }
            break;
        default:
            break;
        }
    }
    if (e.getDamager() instanceof Player && e.getEntity() instanceof LivingEntity) {
        Player p = (Player) e.getDamager();
        LivingEntity le = (LivingEntity) e.getEntity();
        switch (le.getType()) {
        case ENDERMAN:
            StatType.SANITY.change(p, -1);
            break;
        case SPIDER:
        case CAVE_SPIDER:
            if (!e.isCancelled() && le.getHealth() - e.getFinalDamage() <= 0) {
                ItemStack i = p.getInventory().getItemInMainHand();
                if (Main.random(19) == 0 && i.getType() == Material.IRON_BARDING && i.getItemMeta().getLore() != null) {
                    i.setType(Material.DIAMOND_BARDING);
                    ItemMeta meta = i.getItemMeta();
                    if (meta.getDisplayName().equals(ItemDef.blowpipe.getItemMeta().getDisplayName())) {
                        meta.setDisplayName(ChatColor.DARK_AQUA + "Toxic Blowpipe");
                        i.setItemMeta(meta);
                        i.addUnsafeEnchantment(Utils.glow, 1);
                    }
                    p.sendMessage(ChatColor.GRAY + "Your Blowpipe absorbs some venom from the spider!");
                    p.getWorld().playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_VILLAGER_CURE, 0.4F, 1.6F);
                    ParticleEffects.VILLAGER_HAPPY.display(0.5F, 0.5F, 0.5F, 1.0F, 24, p.getEyeLocation(), 257);
                }
            }
            break;
        case BLAZE:
            if (!e.isCancelled() && le.getHealth() - e.getFinalDamage() <= 0) {
                ItemStack i = p.getInventory().getItemInMainHand();
                if (Main.random(19) == 0 && i.getType() == Material.IRON_BARDING && i.getItemMeta().getLore() != null) {
                    i.setType(Material.GOLD_BARDING);
                    ItemMeta meta = i.getItemMeta();
                    if (meta.getDisplayName().equals(ItemDef.blowpipe.getItemMeta().getDisplayName())) {
                        meta.setDisplayName(ChatColor.DARK_RED + "Incendary Blowpipe");
                        i.setItemMeta(meta);
                        i.addUnsafeEnchantment(Utils.glow, 1);
                    }
                    p.sendMessage(ChatColor.GRAY + "Your Blowpipe absorbs the inferno from the Blaze!");
                    p.getWorld().playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_VILLAGER_CURE, 0.4F, 1.6F);
                    ParticleEffects.FLAME.display(0.5F, 0.5F, 0.5F, 0.1F, 24, p.getEyeLocation(), 257);
                }
            }
            break;
        case ZOMBIE:
        case ZOMBIE_VILLAGER:
        case HUSK:
        case PIG_ZOMBIE:
            if (p.getInventory().getItemInMainHand().getType().toString().endsWith("_SPADE")) {
                e.setDamage(e.getDamage() * 1.5);
                le.getWorld().playSound(le.getLocation(), Sound.ENTITY_PLAYER_HURT, 0.4F, 0.5F);
            }
            break;
            default:
                break;
        }
    }
        
    }
    
    @EventHandler
    public void onEntityKill(EntityDeathEvent e) {
        LivingEntity killed = e.getEntity();
        if (killed.getKiller() == null)
            return;
        
        Player killer = killed.getKiller();
            switch (killed.getType()) {
                case PLAYER:
                    killer.sendMessage(ChatColor.DARK_PURPLE + "You've killed someone! Your sanity slips away...");
                    killer.playSound(killer.getLocation(), Sound.ENTITY_ZOMBIE_VILLAGER_CURE, 1.0F, 1.2F);
                    StatType.SANITY.change(killer, -30);
                    break;
                case VILLAGER:
                    killer.sendMessage(ChatColor.GRAY + "Killing the villager bugs your conscience.");
                    StatType.SANITY.change(killer, -5);
                    break;
                case COW:
                case CHICKEN:
                case PIG:
                case SHEEP:
                case RABBIT:
                case WOLF:
                case OCELOT:
                case HORSE:
                    killer.sendMessage(ChatColor.GRAY + "Killing the cute animal slightly bugs your conscience.");
                    StatType.SANITY.change(killer, -5);
                    break;
                case ZOMBIE:
                case ZOMBIE_VILLAGER:
                case HUSK:
                case STRAY:
                case SPIDER:
                case CAVE_SPIDER:
                case SKELETON:
                    killer.sendMessage(ChatColor.DARK_GREEN + "Facing your fears restores some of your sanity.");
                    StatType.SANITY.change(killer, 1);
                    break;
                case WITCH:
                case PIG_ZOMBIE:
                case WITHER:
                case GHAST:
                case BLAZE:
                case GUARDIAN:
                    killer.sendMessage(ChatColor.DARK_GREEN + "Facing your nightmares restores much of your sanity.");
                    StatType.SANITY.change(killer, 3);
                    break;
                default:
                    break;
            }
            //Skill.KNOWLEDGE.addXP(killer, (int) (killed.getMaxHealth() * 2));
            if (killer.getInventory().contains(Material.EMPTY_MAP)) {
                ClueScroll clue = ClueScroll.containsClueScoll(killer.getInventory());
                if (clue != null) {
                    Bukkit.getServer().getPluginManager().callEvent(new ClueScrollIncrementEvent(clue, killer, killed, 1));
                }
            }
    }
    
    @EventHandler
    public void endermanTeleport(EntityTeleportEvent e) {
        if (e.getEntityType() == EntityType.ENDERMAN) {
            LivingEntity entity = (LivingEntity) e.getEntity();
            if (entity.hasPotionEffect(PotionEffectType.SLOW) && entity.hasPotionEffect(PotionEffectType.JUMP))
                e.setCancelled(true);
        }
    }
    
    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent e) {
        Player p = e.getEntity();
        e.setKeepInventory(true);
        e.getDrops().clear();
        
        ItemStack[] contents = p.getInventory().getContents();
        for (int i = 0; i < contents.length; i++) {
            ItemStack next = contents[i];
            if (next != null) {
                boolean flag = false;
                for (Enchantment ench : next.getEnchantments().keySet()) {
                    if (Augment.BOUND.getAugment().equals(ench)) {
                        flag = true;
                        break;
                    }
                }
                if (!flag) {
                    contents[i] = null;
                }
            }
        }
        p.getInventory().setContents(contents);
        
        p.getWorld().strikeLightning(p.getLocation());
        e.setDeathMessage(ChatColor.RED + e.getDeathMessage());
        p.removeMetadata("bleed", Main.getPlugin());
        ParticleTasks.makeNotSweaty(p);
        ParticleTasks.dryOff(p);
    }
    
    @EventHandler
    public void onPlayerRespawn(PlayerRespawnEvent e) {
    final Player p = e.getPlayer();
        new BukkitRunnable() {
            @Override
            public void run() {
            if (p == null) return;
                for (StatType ST : StatType.values())
                    ST.set(p, 75, true);
                VitalsHandler.save(p);
                p.sendMessage(ChatColor.STRIKETHROUGH + "" + ChatColor.BOLD + "===========================================");
                p.sendMessage(ChatColor.RED + "Oh dear, you have died!");
                if (p.getInventory().getContents().length == 0) p.sendMessage(ChatColor.GRAY + "All your corporeal belongings have been erased.");
                p.sendMessage("");
                p.sendMessage(ChatColor.GRAY + "You wake up somewhere in the world...");
            }
        }.runTaskLater(Main.getPlugin(), 2L);
    }
    
    @EventHandler
    public void inventoryClose(InventoryCloseEvent e) {
        final Player p = (Player) e.getPlayer();
        if (e.getInventory().getName().contains(" Casket")) {
            Inventory inv = e.getInventory();
            if (inv.getContents().length > 0) {
                for (ItemStack i : inv.getContents()) {
                    if (i != null && !i.getType().equals(Material.AIR))
                        p.getWorld().dropItemNaturally(p.getLocation(), i);
                }
            }
        }
    }
    
    @EventHandler
    public void itemCraftEvent(CraftItemEvent e) {
        Player p = (Player) e.getWhoClicked();
        if (p.getInventory().contains(Material.EMPTY_MAP)) {
            ClueScroll clue = ClueScroll.containsClueScoll(p.getInventory());
            if (clue != null && clue.getType().equals(ClueScrollType.CRAFT_ITEM)) {
                Material target = Material.valueOf(ChatColor.stripColor(clue.getMessage().get(2).toUpperCase()));
                if (e.getCurrentItem().getType().equals(target))
                    clue.complete(p);
            }
        }
        if (e.getRecipe().getResult().getType() == Material.EMPTY_MAP && !p.isOp()) {
            p.sendMessage(ChatColor.GRAY + "Maps are for panzies. You don't need maps.");
            p.closeInventory();
            e.setCancelled(true);
        }
    }
    
    @EventHandler
    public void onEntityExplode(EntityExplodeEvent e) {
        
        if (e.getEntity() != null && !e.isCancelled()) {
            if (e.getEntityType() == EntityType.CREEPER) {
                e.blockList().clear();
            } else {
                Set<Block> toRemove = new HashSet<Block>();
                
                for (Block b : e.blockList()) {
                    if (RegionHandler.isClaimed(b.getChunk()))
                        toRemove.add(b);
                }
                
                for (Block b : toRemove)
                    e.blockList().remove(b);
            }
            
            if (e.getEntityType() == EntityType.PRIMED_TNT && e.getEntity().hasMetadata("noBlockDamage"))
                e.blockList().clear();
        }
    }
    
    @EventHandler
    public void onEntityCombust(EntityCombustEvent e) {
        Entity en = e.getEntity();
        if (en.getType() == EntityType.ZOMBIE || en.getType() == EntityType.SKELETON) {
            e.setCancelled(true);
        }
    }
    
    @EventHandler
    public void onPlayerMoveTeleport(PlayerMoveEvent e) {
        if (e.getTo().getBlockX() == e.getFrom().getBlockX() && e.getTo().getBlockY() == e.getFrom().getBlockY() && e.getTo().getBlockZ() == e.getFrom().getBlockZ())
            return;
        
        if (e.getPlayer().hasMetadata("delayed.teleporting")) {
            Player p = e.getPlayer();
            Bukkit.getServer().getScheduler().cancelTask(p.getMetadata("delayed.teleporting").get(0).asInt());
            p.sendMessage(ChatColor.RED + "Your teleport was cancelled because you moved!");
            p.playSound(p.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_DOOR_WOOD, 0.4F, 1.7F);
            p.removeMetadata("delayed.teleporting", Main.getPlugin());
        }
        
        if (Utilities.isWater(e.getTo().getBlock()) && !Utilities.isWater(e.getFrom().getBlock())) {
            final Player p = e.getPlayer();
            new BukkitRunnable() {
                @Override
                public void run() {
                    if (!p.isValid())
                        return;
                    if (p.getVehicle() == null || p.getVehicle().getType() != EntityType.BOAT)
                        ParticleTasks.Soak(p, 8);
                }
            }.runTaskLater(Main.getPlugin(), 2L);
        }
    }
    
    @EventHandler
    public void onFurnaceSmelt(FurnaceSmeltEvent e) {
        if (e.getSource().getType() == Material.WOOL && !Utils.compareLore(e.getSource(), ItemDef.towel_wet)) {
            e.setCancelled(true);
        }
    }
    
    private static void applyEffectLater(final LivingEntity e, final PotionEffectType type, final int amplitude) {
        new BukkitRunnable() {
            @Override
            public void run() {
                new PotionEffect(type, 1000000, amplitude).apply(e);
            }
        }.runTaskLater(Main.getPlugin(), 2L);
    }

}
