package projectz;

import org.bukkit.Material;
import org.bukkit.block.Block;

public class MatDat {
    
    public MatDat(Material m) {
        this.m = m;
        this.data = (byte) 0;
    }
    
    public MatDat(Material m, int data) {
        this.m = m;
        this.data = (byte) data;
    }
    
    public MatDat(Material m, byte data) {
        this.m = m;
        this.data = data;
    }
    
    private Material m;
    private byte data;
    
    public Material a() {
        return m;
    }
    
    public byte b() {
        return data;
    }
    
    public boolean compare(Block b) {
        return b.getType().equals(m) && b.getData() == data;
    }
    
    public void set(Block b) {
        b.setType(m);
        b.setData(data);
    }

}
