package projectz;

import java.io.File;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.FurnaceRecipe;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.ShapelessRecipe;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;

import net.minecraft.server.v1_12_R1.Item;
import net.minecraft.server.v1_12_R1.RecipesFurnace;
import projectz.bosses.BossHandler;
import projectz.chests.ChestHandler;
import projectz.clans.ClanInvite;
import projectz.clans.ClanLoadAndSaver;
import projectz.clans.ClanPerks;
import projectz.clans.RegionHandler;
import projectz.food.ModifiedFoodHandler;
import projectz.handlers.VitalsHandler;
import projectz.listeners.BossListener;
import projectz.listeners.ClanListener;
import projectz.listeners.InteractListener;
import projectz.listeners.RegionListener;
import projectz.listeners.TreasureListener;
import projectz.tasks.DepletionTaskCycler;
import projectz.tasks.ParticleTasks;
import projectz.treasure.CasketHandler;
import projectz.treasure.FishingTreasureHandler;
import utilities.Utils;

public class Main extends JavaPlugin {
    
    public static final String ERR = "�4Error > �r", INFO = "�9Info > �r";
        
    private static Main plugin;
    private static World overWorld, netherWorld;
    private static FileConfiguration config;

    public static FileConfiguration stats;
    
    private static ClanInvite inviteHandler;
    
    private static Scoreboard scoreboard;
    private static DepletionTaskCycler cycler;
    public static ScoreboardManager manager;
    public static Objective joined, playerclass;
    public static Objective donationRank, staffRank;
    
    public static YamlConfiguration clueList, memberList, clanList;
    
    public static int exhaustDelay;
    
    public static int chestLootMin, chestLootMax;
    public static long lootDelay;
    
    public static final Map<String, String> loadedPlayers = new HashMap<String, String>();
    
    public static final List<String> hydrationHelp = new ArrayList<String>(),
                                    sanityHelp = new ArrayList<String>(),
                                    warmthHelp = new ArrayList<String>(),
                                    worldHelp = new ArrayList<String>(),
                                    monstersHelp = new ArrayList<String>(),
                                    spellsHelp = new ArrayList<String>(),
                                    abilitiesHelp = new ArrayList<String>(),
                                    skillsHelp = new ArrayList<String>(),
                                    clanHelp = new ArrayList<String>(),
                                    clancommandHelp2 = new ArrayList<String>(),
                                    clancommandHelp3 = new ArrayList<String>(),
                                    clancommandHelp = new ArrayList<String>(),
                                    clanLevelsHelp = new ArrayList<String>(),
                                    clanDoubleYieldHelp = new ArrayList<String>(),
                                    commandsHelp = new ArrayList<String>();
        
    private static String[] commands = {
        "pzhelp", "zspawn", "projectz", "pz", "clan", "clans", "c", "invite", "pzcommands", "pzcmds", "map"
    };
    
    @Override
    public void onEnable() {
        if (!new File("plugins" + File.separator + "ProjectZ" + File.separator + "config.yml").exists())
            this.saveDefaultConfig();
        
        if (!new File("plugins" + File.separator + "ProjectZ" + File.separator + "clues.yml").exists())
            this.saveResource("clues.yml", false);

        if (!new File("plugins" + File.separator + "ProjectZ" + File.separator + "members.yml").exists())
            this.saveResource("members.yml", false);
        
        plugin = this;
        config = this.getConfig();
        stats = Utils.saveAndGetResource(this, "stats.yml");
        exhaustDelay = config.getInt("exhaustion-delay");
        //ItemDef.setStuff();
        inviteHandler = new ClanInvite();
        registerListeners();
        overWorld = Bukkit.getServer().getWorld(config.getString("world-name"));
        netherWorld = Bukkit.getServer().getWorld(config.getString("world-name") + "_nether");
        handleScoreboard();
        setHelpText();
        cycler = new DepletionTaskCycler(exhaustDelay);
        CommandHandler cmdh = new CommandHandler();
        for (String s : commands)
            getCommand(s).setExecutor(cmdh);
        ModifiedFoodHandler.setModifiedFoods();
        setChestItems();
        addNewRecipes();
        FishingTreasureHandler.load(config);
        CasketHandler.load(config);
        clueList = YamlConfiguration.loadConfiguration(new File(getDataFolder(), "clues.yml"));
        ClanPerks.load();
        ClanLoadAndSaver.loadClans();
        RegionHandler.MAX_CLAIM_CHUNKS = config.getInt("Max-Claim-Chunks");
        RegionHandler.START_CLAIM_CHUNKS = config.getInt("Start-Claim-Chunks");
        RegionHandler.INCREASE_PER_MEMBER = config.getInt("Members-Per-Chunk");
        BossHandler.load();
        ChangeStackSizes();
        new ParticleTasks();
    }
     
    @Override
    public void onDisable() {
        ClanLoadAndSaver.saveClans();
        ClanLoadAndSaver.saveMembers();
        VitalsHandler.save();
    }

    private void registerListeners() {
        Bukkit.getPluginManager().registerEvents(new InteractListener(), this);
        Bukkit.getPluginManager().registerEvents(new TreasureListener(), this);
        Bukkit.getPluginManager().registerEvents(new RegionListener(), this);
        //Bukkit.getPluginManager().registerEvents(new ChatListener(), this);
        Bukkit.getPluginManager().registerEvents(new BossListener(), this);
        Bukkit.getPluginManager().registerEvents(new ClanListener(), this);
    }
        
    public static boolean hasPermission(Player player, String permission)
    {
        Permission p = new Permission(permission.toLowerCase(), PermissionDefault.FALSE);
        return player.hasPermission(p);
    }
        
    public static int random(int range) {
        return (int) (Math.random() * (range + 1));
    }
    
    public static FileConfiguration getConfiguration() {
        return config;
    }
        
    public static Main getPlugin() {
        return plugin;
    }
    
    public static Main instance() {
        return plugin;
    }
        
    public static World getOverworld() {
        return overWorld;
    }
    
    public static World getNether() {
        return netherWorld;
    }
    
    public static Scoreboard getMainScoreboard() {
        return scoreboard;
    }
    
    public static DepletionTaskCycler getCycler() {
        return cycler;
    }
    
    public static ClanInvite getInviteHandler() {
        return inviteHandler;
    }
    
    private static void handleScoreboard() {
        manager = Bukkit.getScoreboardManager();
        scoreboard = manager.getMainScoreboard();
        joined = scoreboardValue("Joined");
        staffRank = scoreboardValue("staffRank");
        donationRank = scoreboardValue("donationRank");
    }
    
    private static Objective scoreboardValue(String s) {
        return scoreboard.getObjective(s) == null ? scoreboard.registerNewObjective(s, "dummy") : scoreboard.getObjective(s);
    }
    
    private static void setHelpText() {
        ConfigurationSection sec = config.getConfigurationSection("help");
            for (String s : sec.getStringList("hydration"))
                hydrationHelp.add(s.replace("&", "�"));
            for (String s : sec.getStringList("sanity"))
                sanityHelp.add(s.replace("&", "�"));
            for (String s : sec.getStringList("warmth"))
                warmthHelp.add(s.replace("&", "�"));
            for (String s : sec.getStringList("world"))
                worldHelp.add(s.replace("&", "�"));
            for (String s : sec.getStringList("monsters"))
                monstersHelp.add(s.replace("&", "�"));
            for (String s : sec.getStringList("spells"))
                spellsHelp.add(s.replace("&", "�"));
            for (String s : sec.getStringList("skills"))
                skillsHelp.add(s.replace("&", "�"));
            for (String s : sec.getStringList("abilities"))
                abilitiesHelp.add(s.replace("&", "�"));
            for (String s : sec.getStringList("clans"))
                clanHelp.add(s.replace("&", "�"));
            for (String s : sec.getStringList("clancommands"))
                clancommandHelp.add(s.replace("&", "�"));
            for (String s : sec.getStringList("clancommands2"))
                clancommandHelp2.add(s.replace("&", "�"));
            for (String s : sec.getStringList("clancommands3"))
                clancommandHelp3.add(s.replace("&", "�"));
            for (String s : sec.getStringList("clanlevels"))
                clanLevelsHelp.add(s.replace("&", "�"));
            for (String s : sec.getStringList("clandoubleyield"))
                clanDoubleYieldHelp.add(s.replace("&", "�"));
            for (String s : sec.getStringList("commands"))
                commandsHelp.add(s.replace("&", "�"));
    }
    
    private static void setChestItems() {
        boolean log = config.getBoolean("debug");
        chestLootMin = config.getInt("loot-chest-min-amount");
        chestLootMax = config.getInt("loot-chest-max-amount");
        ConfigurationSection sec = config.getConfigurationSection("loot-chests");
        for (String s : sec.getKeys(false)) {
            List<String> loots = config.getStringList("loot-chests." + s);
            for (String lootvalue : loots) {
                String[] lootinfo = lootvalue.split(":");
                if (log) Bukkit.getLogger().info(lootinfo[0]);
                for (int i = 0; i < Integer.parseInt(lootinfo[2]); i++) {
                    ItemStack item = new ItemStack(Material.getMaterial(lootinfo[0]), Integer.parseInt(lootinfo[1]));
                    ChestHandler.getLootTable(s).add(item);
                }
            }
        }
    }
    
    public static int getDartAmount(Inventory inv) {
    int count = 0;
        for (ItemStack i : inv.getContents()) {
            if (i != null)
                if (i.getType().equals(Material.FIREWORK) && i.getItemMeta().getLore() != null)
                    if (i.getItemMeta().getLore().get(0).contains("Left click with a Blowpipe"))
                        count += i.getAmount();
        }
    return count;
    }
    
    public static String capitalizeFirst(String s) {
        return s.substring(0, 1).toUpperCase() + s.toLowerCase().substring(1);
    }
    
    @SuppressWarnings("deprecation")
    private static void addNewRecipes() {
        Set<ShapedRecipe> SR = new HashSet<ShapedRecipe>();
        Set<ShapelessRecipe> SlR = new HashSet<ShapelessRecipe>();
        Set<FurnaceRecipe> FR = new HashSet<FurnaceRecipe>();
        SR.add(new ShapedRecipe(new ItemStack(Material.SADDLE))
        .shape("A  ","ABA","ABA")
        .setIngredient('A', Material.LEATHER)
        .setIngredient('B', Material.IRON_INGOT));
        
        SlR.add(new ShapelessRecipe(new ItemStack(ModifiedFoodHandler.applePie))
        .addIngredient(Material.APPLE)
        .addIngredient(Material.WHEAT)
        .addIngredient(Material.SUGAR)
        .addIngredient(Material.EGG));
        
        SlR.add(new ShapelessRecipe(new ItemStack(ModifiedFoodHandler.potatoStew))
        .addIngredient(Material.BOWL)
        .addIngredient(2, Material.POTATO_ITEM));
        
        SlR.add(new ShapelessRecipe(new ItemStack(ModifiedFoodHandler.carrotStew))
        .addIngredient(Material.BOWL)
        .addIngredient(2, Material.CARROT_ITEM));
        
        SlR.add(new ShapelessRecipe(new ItemStack(ModifiedFoodHandler.chickenPotPie))
        .addIngredient(Material.COOKED_CHICKEN)
        .addIngredient(Material.POTATO_ITEM)
        .addIngredient(Material.CARROT_ITEM)
        .addIngredient(Material.BOWL)
        .addIngredient(Material.WHEAT));
        
        SR.add(new ShapedRecipe(new ItemStack(ModifiedFoodHandler.simpleSandwich))
        .shape("   ", "ABA", "   ")
        .setIngredient('A', Material.BREAD)
        .setIngredient('B', Material.COOKED_CHICKEN));
        SR.add(new ShapedRecipe(new ItemStack(ModifiedFoodHandler.simpleSandwich))
        .shape("   ", "ABA", "   ")
        .setIngredient('A', Material.BREAD)
        .setIngredient('B', Material.COOKED_MUTTON));
        SR.add(new ShapedRecipe(new ItemStack(ModifiedFoodHandler.simpleSandwich))
        .shape("   ", "ABA", "   ")
        .setIngredient('A', Material.BREAD)
        .setIngredient('B', Material.COOKED_RABBIT));

        SR.add(new ShapedRecipe(new ItemStack(ModifiedFoodHandler.burger))
        .shape("   ", "ABA", "   ")
        .setIngredient('A', Material.BREAD)
        .setIngredient('B', Material.GRILLED_PORK));
        SR.add(new ShapedRecipe(new ItemStack(ModifiedFoodHandler.burger))
        .shape("   ", "ABA", "   ")
        .setIngredient('A', Material.BREAD)
        .setIngredient('B', Material.COOKED_BEEF));
        
        SR.add(new ShapedRecipe(new ItemStack(ModifiedFoodHandler.advancedSandwich))
        .shape("AAA", "BCD", "AAA")
        .setIngredient('A', Material.BREAD)
        .setIngredient('C', Material.COOKED_CHICKEN)
        .setIngredient('B', Material.POTATO_ITEM)
        .setIngredient('D', Material.CARROT_ITEM));
        SR.add(new ShapedRecipe(new ItemStack(ModifiedFoodHandler.advancedSandwich))
        .shape("AAA", "BCD", "AAA")
        .setIngredient('A', Material.BREAD)
        .setIngredient('C', Material.COOKED_MUTTON)
        .setIngredient('B', Material.POTATO_ITEM)
        .setIngredient('D', Material.CARROT_ITEM));
        SR.add(new ShapedRecipe(new ItemStack(ModifiedFoodHandler.advancedSandwich))
        .shape("AAA", "BCD", "AAA")
        .setIngredient('A', Material.BREAD)
        .setIngredient('C', Material.COOKED_RABBIT)
        .setIngredient('B', Material.POTATO_ITEM)
        .setIngredient('D', Material.CARROT_ITEM));

        ItemStack bottle = new ItemStack(Material.GLASS_BOTTLE);
        ItemStack water = new ItemStack(Material.POTION);
        SlR.add(a("greentea1", ModifiedFoodHandler.tea, water, new ItemStack(Material.LEAVES, 1, (short) -1)));
        SlR.add(a("greentea2", ModifiedFoodHandler.tea, water, new ItemStack(Material.LEAVES_2, 1, (short) -1)));
        SlR.add(a("sweettea", ModifiedFoodHandler.sweetTea, water, new ItemStack(Material.LEAVES, 1, (short) -1), new ItemStack(Material.SUGAR, 2)));
        SlR.add(a("icedtea", ModifiedFoodHandler.icedTea, water, new ItemStack(Material.LEAVES, 1, (short) -1), new ItemStack(Material.SUGAR), new ItemStack(Material.ICE)));
        SlR.add(a("herbaltea1", ModifiedFoodHandler.herbalTea, water, new ItemStack(Material.LEAVES, 1, (short) -1), new ItemStack(Material.LONG_GRASS), new ItemStack(Material.RED_ROSE)));
        SlR.add(a("herbaltea2", ModifiedFoodHandler.herbalTea, water, new ItemStack(Material.LEAVES, 1, (short) -1), new ItemStack(Material.LONG_GRASS), new ItemStack(Material.YELLOW_FLOWER)));
        SlR.add(a("herbaltea3", ModifiedFoodHandler.herbalTea, water, new ItemStack(Material.LEAVES_2, 1, (short) -1), new ItemStack(Material.LONG_GRASS), new ItemStack(Material.RED_ROSE)));
        SlR.add(a("herbaltea4", ModifiedFoodHandler.herbalTea, water, new ItemStack(Material.LEAVES_2, 1, (short) -1), new ItemStack(Material.LONG_GRASS), new ItemStack(Material.YELLOW_FLOWER)));
        SlR.add(a("fruitpunch", ModifiedFoodHandler.fruitPunch, water, new ItemStack(Material.APPLE), new ItemStack(Material.MELON), new ItemStack(Material.SUGAR)));
        SlR.add(a("coffee", ModifiedFoodHandler.coffee, water, new ItemStack(Material.MILK_BUCKET), new ItemStack(Material.INK_SACK, 1, (short) 3), new ItemStack(Material.SUGAR)));
        
        /*SlR.add(new ShapelessRecipe(new ItemStack(ModifiedFoodHandler.tea))
        .addIngredient(Material.POTION, 0)
        .addIngredient(Material.LEAVES));
        SlR.add(new ShapelessRecipe(new ItemStack(ModifiedFoodHandler.sweetTea))
        .addIngredient(Material.POTION, 0)
        .addIngredient(Material.LEAVES)
        .addIngredient(2, Material.SUGAR));
        SlR.add(new ShapelessRecipe(new ItemStack(ModifiedFoodHandler.icedTea))
        .addIngredient(Material.POTION, 0)
        .addIngredient(Material.LEAVES)
        .addIngredient(Material.SUGAR)
        .addIngredient(Material.ICE));
        SlR.add(new ShapelessRecipe(new ItemStack(ModifiedFoodHandler.herbalTea))
        .addIngredient(Material.POTION, 0)
        .addIngredient(Material.LEAVES)
        .addIngredient(Material.LONG_GRASS, 1)
        .addIngredient(Material.RED_ROSE));
        SlR.add(new ShapelessRecipe(new ItemStack(ModifiedFoodHandler.herbalTea))
        .addIngredient(Material.POTION, 0)
        .addIngredient(Material.LEAVES)
        .addIngredient(Material.LONG_GRASS, 1)
        .addIngredient(Material.YELLOW_FLOWER));
        SlR.add(new ShapelessRecipe(new ItemStack(ModifiedFoodHandler.fruitPunch))
        .addIngredient(Material.POTION, 0)
        .addIngredient(Material.APPLE)
        .addIngredient(Material.MELON)
        .addIngredient(Material.SUGAR));
        SlR.add(new ShapelessRecipe(new ItemStack(ModifiedFoodHandler.coffee))
        .addIngredient(Material.GLASS_BOTTLE)
        .addIngredient(Material.MILK_BUCKET)
        .addIngredient(Material.INK_SACK, 3)
        .addIngredient(Material.SUGAR));*/

        SR.add(new ShapedRecipe(new ItemStack(ModifiedFoodHandler.fishSticks))
        .shape(" A ", " A ", " A ")
        .setIngredient('A', Material.COOKED_FISH));

        SR.add(new ShapedRecipe(new ItemStack(ModifiedFoodHandler.phishTaco))
        .shape("BBB", "AAA", "BBB")
        .setIngredient('A', Material.COOKED_FISH)
        .setIngredient('B', Material.BREAD));
        
        /*SlR.add(new ShapelessRecipe(new ItemStack(ItemDef.coldWater))
        .addIngredient(Material.GLASS_BOTTLE)
        .addIngredient(Material.ICE));
        SlR.add(new ShapelessRecipe(new ItemStack(ItemDef.coldWater))
        .addIngredient(Material.GLASS_BOTTLE)
        .addIngredient(2, Material.SNOW_BALL));
        SlR.add(new ShapelessRecipe(new ItemStack(ItemDef.freezingWater))
        .addIngredient(Material.POTION, 16335)
        .addIngredient(Material.ICE));
        SlR.add(new ShapelessRecipe(new ItemStack(ItemDef.freezingWater))
        .addIngredient(Material.POTION, 16335)
        .addIngredient(2, Material.SNOW_BALL));
        SlR.add(new ShapelessRecipe(new ItemStack(ItemDef.dirtyWater))
        .addIngredient(Material.GLASS_BOTTLE)
        .addIngredient(Material.DIRT));
        SlR.add(new ShapelessRecipe(new ItemStack(ItemDef.saltWater))
        .addIngredient(Material.GLASS_BOTTLE)
        .addIngredient(Material.SAND));
        SlR.add(new ShapelessRecipe(new ItemStack(ItemDef.cactusWater))
        .addIngredient(Material.GLASS_BOTTLE)
        .addIngredient(Material.CACTUS));*/
        SlR.add(a("coldwater1", ItemDef.coldWater, bottle, new ItemStack(Material.ICE)));
        SlR.add(a("coldwater2", ItemDef.coldWater, bottle, new ItemStack(Material.SNOW_BALL, 2)));
        SlR.add(a("freezewater1", ItemDef.freezingWater, ItemDef.coldWater, new ItemStack(Material.ICE)));
        SlR.add(a("freezewater2", ItemDef.freezingWater, ItemDef.coldWater, new ItemStack(Material.SNOW_BALL, 2)));

        SlR.add(a("cactuswater", ItemDef.cactusWater, bottle, new ItemStack(Material.CACTUS)));
        
        SR.add(new ShapedRecipe(new ItemStack(ItemDef.blowpipe))
        .shape("A A", "A A", "A A")
        .setIngredient('A', Material.SUGAR_CANE));
        
        SR.add(new ShapedRecipe(new ItemStack(ItemDef.dart))
        .shape(" A ", " A ", " B ")
        .setIngredient('A', Material.CACTUS)
        .setIngredient('B', Material.FEATHER));

        SR.add(new ShapedRecipe(new ItemStack(ItemDef.talismanOfRespite))
        .shape("BAB", "AAA", "BAB")
        .setIngredient('A', Material.CLAY_BALL)
        .setIngredient('B', Material.GHAST_TEAR));

        ItemStack paper = new ItemStack(Material.PAPER, 2);
        ItemStack charcoal = new ItemStack(Material.COAL, 1, (short) 1);
        SlR.add(a("filterdirtywater", ItemDef.filteredWater.clone(), ItemDef.dirtyWater.clone(), paper, charcoal));
        SlR.add(a("filterfilthywater", ItemDef.dirtyWater.clone(), ItemDef.filthyWater.clone(), paper, charcoal));
        SlR.add(a("filtercactuswater", new ItemStack(Material.POTION), ItemDef.cactusWater.clone(), paper, charcoal));
        SlR.add(a("filtersaltwater", ItemDef.filteredWater.clone(), ItemDef.saltWater.clone(), paper, charcoal));
        
        SlR.add(new ShapelessRecipe(ItemDef.towel_dry)
        .addIngredient(4, Material.WOOL));

        SlR.add(new ShapelessRecipe(new ItemStack(ItemDef.dart)).addIngredient(2, Material.ARROW));
        SlR.add(new ShapelessRecipe(new ItemStack(Material.GLASS_BOTTLE)).addIngredient(Material.POTION));

        FR.add(new FurnaceRecipe(new ItemStack(ItemDef.cookedSeeds), Material.SEEDS));
        FR.add(new FurnaceRecipe(new ItemStack(ItemDef.cookedSeeds), Material.MELON_SEEDS));
        FR.add(new FurnaceRecipe(new ItemStack(ItemDef.cookedSeeds), Material.PUMPKIN_SEEDS));

        SlR.add(new ShapelessRecipe(Utils.setItemAmount(ItemDef.cheese, 2)).addIngredient(4, Material.MILK_BUCKET));
        
        
        RecipesFurnace smelting = RecipesFurnace.getInstance();
        smelting.registerRecipe(a(ItemDef.coldWater.clone()), a(new ItemStack(Material.POTION)), 0.1F);
        smelting.registerRecipe(a(ItemDef.freezingWater.clone()), a(new ItemStack(Material.POTION)), 0.1F);
        smelting.registerRecipe(a(new ItemStack(Material.POTION)), a(ItemDef.hotWater.clone()), 0.1F);
        smelting.registerRecipe(a(ItemDef.hotWater.clone()), a(ItemDef.boilingWater.clone()), 0.1F);
        smelting.registerRecipe(a(ItemDef.towel_wet.clone()), a(ItemDef.towel_dry.clone()), 0.1F);
        smelting.registerRecipe(a(ItemDef.filteredWater.clone()), a(new ItemStack(Material.POTION)), 0.1F);
        
        for (ShapedRecipe s : SR)
            Bukkit.getServer().addRecipe(s);
        for (ShapelessRecipe s : SlR)
            Bukkit.getServer().addRecipe(s);
        for (FurnaceRecipe s : FR)
            Bukkit.getServer().addRecipe(s);
    }
    
    private static ShapelessRecipe a(String name, ItemStack... data) {
        ShapelessRecipe r = new ShapelessRecipe(new NamespacedKey(instance(), name), data[0]);
        try {
            List<ItemStack> ingredients;
            Field f = ShapelessRecipe.class.getDeclaredField("ingredients");
            f.setAccessible(true);
            ingredients = (List<ItemStack>) f.get(r);
            for (int i = 1; i < data.length; i++) {
                ItemStack it = data[i];
                ItemStack one = Utils.setItemAmount(it, 1);
                for (int j = 0; j < it.getAmount(); j++) {
                    ingredients.add(one);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return r;
    }
    
    private static net.minecraft.server.v1_12_R1.ItemStack a(ItemStack stack) {
        return CraftItemStack.asNMSCopy(stack);
    }
    
    private static void ChangeStackSizes() {
        TweakItemSize(Material.BANNER, 1);
        TweakItemSize(Material.TNT, 1);
        TweakItemSize(Material.COOKED_BEEF, 16);
        TweakItemSize(Material.COOKED_CHICKEN, 16);
        TweakItemSize(Material.COOKED_FISH, 16);
        TweakItemSize(Material.GRILLED_PORK, 16);
        TweakItemSize(Material.COOKED_RABBIT, 16);
        TweakItemSize(Material.COOKED_MUTTON, 16);
        TweakItemSize(Material.RAW_BEEF, 16);
        TweakItemSize(Material.RAW_CHICKEN, 16);
        TweakItemSize(Material.RAW_FISH, 16);
        TweakItemSize(Material.PORK, 16);
        TweakItemSize(Material.RABBIT, 16);
        TweakItemSize(Material.MUTTON, 16);
        TweakItemSize(Material.SPONGE, 16);
        TweakItemSize(Material.BREAD, 16);
        TweakItemSize(Material.APPLE, 16);
        TweakItemSize(Material.BAKED_POTATO, 16);
        TweakItemSize(Material.CARROT_ITEM, 16);
        TweakItemSize(Material.POTATO_ITEM, 16);
        TweakItemSize(Material.BEETROOT, 16);
        TweakItemSize(Material.RED_MUSHROOM, 16);
        TweakItemSize(Material.BROWN_MUSHROOM, 16);
        TweakItemSize(Material.GOLDEN_APPLE, 16);
        TweakItemSize(Material.PUMPKIN_PIE, 8);
        TweakItemSize(Material.CAKE, 8);
        TweakItemSize(Material.GLASS_BOTTLE, 16);
        TweakItemSize(Material.ICE, 16);
        TweakItemSize(Material.POTION, 3);
        TweakItemSize(Material.MONSTER_EGG, 16);
    }
    
    @SuppressWarnings("deprecation")
    private static void TweakItemSize(Material mat, int size) {
        Item item = Item.getById(mat.getId());
        try {
            Field field = Item.class.getDeclaredField("maxStackSize");
            field.setAccessible(true);
            field.setInt(item, size);
             
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
