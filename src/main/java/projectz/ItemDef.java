package projectz;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import projectz.tasks.DepletionTaskCycler;
import projectz.tasks.DepletionTaskCycler.StatType;
import utilities.Utils;

public class ItemDef {

    //private static final List<String> foodLore = new ArrayList<String>();
            
    public static final ItemStack
    healingSpell = Utils.ConstructItemStack(Material.FIREWORK_CHARGE, 1, (short) 0, "�e�lHealing Spell", new String[] {"�7Creates a Ring of Healing that heals", "�7all players within its radius."}, true),
    rageSpell = Utils.ConstructItemStack(Material.FIREWORK_CHARGE, 1, (short) 0, "�d�lRage Spell", new String[] {"�7Creates a Ring of Rage that boosts", "�7Strength and Speed for all players", "�7within its radius."}, true),
    hopeSpell = Utils.ConstructItemStack(Material.FIREWORK_CHARGE, 1, (short) 0, "�9�lHope Spell", new String[] {"�7Creates a Ring of Hope that restores", "�7Hydration, Sanity, and Warmth of all", "�7players within its radius."}, true),
    forgeSpell = Utils.ConstructItemStack(Material.FIREWORK_CHARGE, 1, (short) 0, "�6�lForge Spell", new String[] {"�7Creates a Ring of Forging that smelts", "�7ores, charcoals logs, and cooks food of", "�7all players within its radius."}, true),
    flashSpell = Utils.ConstructItemStack(Material.FIREWORK_CHARGE, 1, (short) 0, "�f�lFlash Spell", new String[] {"�7Allows the user to run faster than the", "�7winds, albeit with a heavy toll on hunger", "�7and hydration when the effect expires."}, true),
    sneakingSpell = Utils.ConstructItemStack(Material.FIREWORK_CHARGE, 1, (short) 0, "�8�lShadow Spell", new String[] {"�7Cloaks all players close to the user", "�7with shadow, causing all hostile mobs", "�7to ignore them for a while."}, true),
    freezeSpell = Utils.ConstructItemStack(Material.FIREWORK_CHARGE, 1, (short) 0, "�b�lFreeze Spell", new String[] {"�7Freezes EVERYTHING within the spell's", "�7radius except for the user, including", "�7entities, liquids, and blocks."}, true),
    lightningSpell = Utils.ConstructItemStack(Material.FIREWORK_CHARGE, 1, (short) 0, "�9�lLightning Spell", new String[] {"�7Throws a Lightning Orb that activates", "�7after 3 seconds, striking the area around", "�7it with several bolts of lightning."}, true),
    poisonSpell = Utils.ConstructItemStack(Material.FIREWORK_CHARGE, 1, (short) 0, "�c�lPoison Spell", new String[] {"�7Throws a Poison Orb that activates", "�7after 3 seconds, creating a cloud of", "�7poison that damages all living things."}, true),
    hasteSpell = Utils.ConstructItemStack(Material.FIREWORK_CHARGE, 1, (short) 0, "�5�lHaste Spell", new String[] {"�7Creates a Ring of Haste that drastically", "�7increases Speed and Mining Speed of all", "�7players within its radius."}, true),
    
    xpBook = new ItemStack(Material.ENCHANTED_BOOK, 1),
    
    hotWater = Utils.ConstructItemStack(Material.POTION, 1, (short) 0, "�rHot Water Bottle", new String[] {}, false),
    coldWater = Utils.ConstructItemStack(Material.POTION, 1, (short) 0, "�rCold Water Bottle", new String[] {}, false),
    filteredWater = Utils.ConstructItemStack(Material.POTION, 1, (short) 0, "�rFiltered Water Bottle", new String[] {}, false),
    dirtyWater = Utils.ConstructItemStack(Material.POTION, 1, (short) 0, "�rDirty Water Bottle", new String[] {}, false),
    filthyWater = Utils.ConstructItemStack(Material.POTION, 1, (short) 0, "�rFilthy Water Bottle", new String[] {}, false),
    saltWater = Utils.ConstructItemStack(Material.POTION, 1, (short) 0, "�rSaltwater Bottle", new String[] {}, false),
    cactusWater = Utils.ConstructItemStack(Material.POTION, 1, (short) 0, "�rCactus Juice", new String[] {}, false),
    boilingWater = Utils.ConstructEnchantedItemStack(Material.POTION, 1, (short) 0, "�rBoiling Water Bottle", new String[] {}, false),
    freezingWater = Utils.ConstructEnchantedItemStack(Material.POTION, 1, (short) 0, "�rFreezing Water Bottle", new String[] {}, false),
    
    houseTab = Utils.ConstructItemStack(Material.STEP, 1, (short) 4, "�rTeleport to House", new String[] {"�7Teleports user to his/her home bed.", "�7Does not work while user is slowed."}, true),
    spawnTab = Utils.ConstructItemStack(Material.STEP, 1, (short) 0, "�rTeleport to Spawn", new String[] {"�7Teleports user to the world spawn.", "�7Does not work while user is slowed."}, true),
    clanTab = Utils.ConstructItemStack(Material.STEP, 1, (short) 5, "�rTeleport to Clan", new String[] {"�7Teleports user to his/her Clan Hub.", "�7Does not work while user is slowed."}, true),
    
    cookedSeeds = Utils.ConstructItemStack(Material.MELON_SEEDS, 1, (short) 0, "�rRoasted Seeds", new String[] {"�7Edible"}, false),
    berries = Utils.ConstructItemStack(Material.NETHER_STALK, 1, (short) 0, "�rBerries", new String[] {"�7Edible"}, false),
    cheese = Utils.ConstructItemStack(Material.SPONGE, 1, (short) 0, "�rCheese", new String[] {"�7Edible"}, false),
    
    blowpipe = Utils.ConstructItemStack(Material.IRON_BARDING, 1, (short) 0, "�rBlowpipe", new String[] {"�7Ammo: �e0", "�7Left click to load darts."}, true),
    toxicblowpipe = Utils.ConstructEnchantedItemStack(Material.DIAMOND_BARDING, 1, (short) 0, "�3Toxic Blowpipe", new String[] {"�7Ammo: �e0", "�7Left click to load darts."}, true),
    incendaryblowpipe = Utils.ConstructEnchantedItemStack(Material.GOLD_BARDING, 1, (short) 0, "�4Incendary Blowpipe", new String[] {"�7Ammo: �e0", "�7Left click to load darts."}, true),
    dart = Utils.ConstructEnchantedItemStack(Material.FIREWORK, 4, (short) 0, "�rDart", new String[] {"�7Left click with a Blowpipe to load."}, true),
    
    bandage = Utils.ConstructItemStack(Material.PAINTING, 4, (short) 0, "�fBandages", new String[] {"�7This can heal bleeds."}, true),
    
    talismanOfResurrection = Utils.ConstructEnchantedItemStack(Material.BANNER, 1, (short) 0, "�dInsignia of Resurrection", new String[] {"�7If you die with this in your", "�7hotbar, you will be revived for", "�712 health, consuming this item.", "�7Has a 10-minute cooldown."}, true),
    talismanOfRespite = Utils.ConstructEnchantedItemStack(Material.BANNER, 1, (short) 0, "�5Insignia of Respite", new String[] {"�7If you die with this in your", "�7hotbar, you will be keep all", "�7items, consuming this item."}, true),
    
    towel_dry = Utils.ConstructItemStack(Material.WOOL, 1, (short) 0, "�rTowel", new String[] {"�7Gets wetter the more it dries."}, true),
    towel_wet = Utils.ConstructItemStack(Material.WOOL, 1, (short) 7, "�rWet Towel", new String[] {"�7I need to dry this in a furnace."}, true)
    ;
    
    private static Map<Color, WaterBottle> potionMap;
    
    static {
        spc(hotWater, 107, 138, 228);
        spc(coldWater, 56, 129, 193);
        spc(dirtyWater, 49, 77, 159);
        spc(filteredWater, 58, 90, 183);
        spc(filthyWater, 159, 118, 64);
        spc(saltWater, 72, 199, 207);
        spc(cactusWater, 72, 207, 162);
        spc(boilingWater, 133, 163, 249);
        spc(freezingWater, 133, 199, 249);
        
        potionMap = new HashMap<>();
        potionMap.put(getColor(filteredWater), new WaterBottle() {
            @Override
            public void drink(Player p) {
                StatType.HYDRATION.change(p, 20);
                StatType.HYDRATION_SAT.change(p, 6);
                StatType.WARMTH.change(p, -1);
                if (Math.random() < 0.2) p.addPotionEffect(new PotionEffect(PotionEffectType.HUNGER, 600, 0), true);
            }
        });
        potionMap.put(getColor(hotWater), new WaterBottle() {
            @Override
            public void drink(Player p) {
                StatType.HYDRATION.change(p, 17);
                StatType.HYDRATION_SAT.change(p, 6);
                StatType.WARMTH.change(p, 3);
            }
        });
        potionMap.put(getColor(coldWater), new WaterBottle() {
            @Override
            public void drink(Player p) {
                StatType.HYDRATION.change(p, 17);
                StatType.HYDRATION_SAT.change(p, 6);
                StatType.WARMTH.change(p, -4);
            }
        });
        potionMap.put(getColor(filthyWater), new WaterBottle() {
            @Override
            public void drink(Player p) {
                StatType.HYDRATION.change(p, 10);
                StatType.HYDRATION_SAT.change(p, 2);
                StatType.WARMTH.change(p, -1);
                if (Math.random() < 0.7) p.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 300, 1), true);
                if (Math.random() < 0.7) p.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 300, 0), true);
                if (Math.random() < 0.7) p.addPotionEffect(new PotionEffect(PotionEffectType.HUNGER, 600, 0), true);
            }
        });
        potionMap.put(getColor(dirtyWater), new WaterBottle() {
            @Override
            public void drink(Player p) {
                StatType.HYDRATION.change(p, 10);
                StatType.HYDRATION_SAT.change(p, 2);
                StatType.WARMTH.change(p, -1);
                if (Math.random() < 0.1) p.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 300, 0), true);
                if (Math.random() < 0.6) p.addPotionEffect(new PotionEffect(PotionEffectType.HUNGER, 600, 0), true);
            }
        });
        potionMap.put(getColor(saltWater), new WaterBottle() {
            @Override
            public void drink(Player p) {
                StatType.HYDRATION.change(p, 10);
                StatType.HYDRATION_SAT.change(p, 2);
                p.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 300, 2), true);
                p.addPotionEffect(new PotionEffect(PotionEffectType.HUNGER, 100, 3), true);
                DepletionTaskCycler.addDehydrationEffect(p, 8);
            }
        });
        potionMap.put(getColor(cactusWater), new WaterBottle() {
            @Override
            public void drink(Player p) {
                StatType.HYDRATION.change(p, Main.random(9) + 1);
                StatType.HYDRATION_SAT.change(p, 3);
                StatType.WARMTH.change(p, -1);
                if (Math.random() < 0.33) {
                    p.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 300, 1), true);
                    if (Math.random() < 0.5)
                        p.chat("Drink Cactus Juice! It'll quench ya! It's the quenchiest!");
                }
                if (Math.random() < 0.33) p.addPotionEffect(new PotionEffect(PotionEffectType.HUNGER, 300, 0), true);
            }
        });
        potionMap.put(getColor(boilingWater), new WaterBottle() {
            @Override
            public void drink(Player p) {
                StatType.HYDRATION.change(p, 15);
                StatType.HYDRATION_SAT.change(p, 5);
                StatType.WARMTH.change(p, 12);
                p.setFireTicks(20 * (2 + Main.random(2)));
            }
        });
        potionMap.put(getColor(freezingWater), new WaterBottle() {
            @Override
            public void drink(Player p) {
                StatType.HYDRATION.change(p, 15);
                StatType.HYDRATION_SAT.change(p, 5);
                StatType.WARMTH.change(p, -12);
                p.damage(Main.random(2) + 2);
                p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 200, 1));
            }
        });
        
    }
    
    public static boolean drinkWater(Player p, ItemStack item) {
        WaterBottle bottle = potionMap.get(getColor(item));
        if (bottle != null) {
            bottle.drink(p);
            return true;
        }
        return false;
    }
    
    private static Color getColor(ItemStack item) {
        try {
            return ((PotionMeta) item.getItemMeta()).getColor();
        } catch (Exception e) {
            return null;
        }
    }
    
    public static void spc(ItemStack item, int r, int g, int b) {
        PotionMeta meta = (PotionMeta) item.getItemMeta();
        Color color = Color.fromRGB(r, g, b);
        meta.setColor(color);
        item.setItemMeta(meta);
    }
    
    /*public static final void setStuff() {
        foodLore.add(ChatColor.GRAY + "Edible");
        setBlowpipe();
        setWater();
        setTeleTabs();
        setFood();
        setHealSpell();
        setRageSpell();
        setHopeSpell();
        setForgeSpell();
        setFlashSpell();
        setSneakingSpell();
        setFreezeSpell();
        setLightningSpell();
        spells.put(healingSpell.getItemMeta().getDisplayName(), healingSpell);
        spells.put(rageSpell.getItemMeta().getDisplayName(), rageSpell);
        spells.put(hopeSpell.getItemMeta().getDisplayName(), hopeSpell);
        spells.put(forgeSpell.getItemMeta().getDisplayName(), forgeSpell);
        spells.put(flashSpell.getItemMeta().getDisplayName(), flashSpell);
        spells.put(freezeSpell.getItemMeta().getDisplayName(), freezeSpell);
        spells.put(sneakingSpell.getItemMeta().getDisplayName(), sneakingSpell);
        spells.put(lightningSpell.getItemMeta().getDisplayName(), lightningSpell);
        teletabs.put(houseTab.getItemMeta().getLore().get(0), TeleTabDestination.HOME);
        teletabs.put(spawnTab.getItemMeta().getLore().get(0), TeleTabDestination.SPAWN);
        teletabs.put(clanTab.getItemMeta().getLore().get(0), TeleTabDestination.CLAN);
        
        for (ItemStack i : spells.values()) {
            unformatSpells.put(ChatColor.stripColor(i.getItemMeta().getDisplayName()), i);
        }
        
        ItemMeta meta = xpBook.getItemMeta();
        meta.setDisplayName(ChatColor.AQUA + "Tome of Experience");
        List<String> lore = new ArrayList<String>();
        lore.add(ChatColor.RESET + "Right click to gain XP!");
        meta.setLore(lore);
        xpBook.setItemMeta(meta);
    }
    
    public static boolean isSpell(ItemStack item) {
    if (item.getItemMeta().getDisplayName() == null) return false;
        return spells.containsKey(item.getItemMeta().getDisplayName());
    }
    
    private static final void setBlowpipe() {
        ItemMeta meta = blowpipe.getItemMeta();
        List<String> lore = new ArrayList<String>();
        
        meta.setDisplayName(ChatColor.RESET + "Blowpipe");
        lore.add(ChatColor.GRAY + "Ammo: " + ChatColor.YELLOW + "0");
        lore.add(ChatColor.GRAY + "Left click to load darts.");
        meta.setLore(lore);
        blowpipe.setItemMeta(meta);
        
        meta.setDisplayName(ChatColor.DARK_AQUA + "Toxic Blowpipe");
        meta.setLore(lore);
        toxicblowpipe.setItemMeta(meta);
        toxicblowpipe.addEnchantment(Utils.glow, 1);

        meta.setDisplayName(ChatColor.DARK_RED + "Incendary Blowpipe");
        meta.setLore(lore);
        incendaryblowpipe.setItemMeta(meta);
        incendaryblowpipe.addEnchantment(Utils.glow, 1);

        lore.clear();
        meta.setDisplayName(ChatColor.RESET + "Dart");
        lore.add(ChatColor.GRAY + "Left click with a Blowpipe to load.");
        meta.setLore(lore);
        dart.setItemMeta(meta);
        dart.addEnchantment(Utils.glow, 1);

        lore.clear();
        BannerMeta bmeta = (BannerMeta) talismanOfResurrection.getItemMeta();
        bmeta.setDisplayName(ChatColor.LIGHT_PURPLE + "Insignia of Resurrection");
        lore.add(ChatColor.GRAY + "If you die with this in your");
        lore.add(ChatColor.GRAY + "hotbar, you will be revived for");
        lore.add(ChatColor.GRAY + "12 health, consuming this item.");
        lore.add(ChatColor.GRAY + "Has a 10-minute cooldown.");
        bmeta.setLore(lore);
        bmeta.setBaseColor(DyeColor.BLACK);
        bmeta.addPattern(new Pattern(DyeColor.WHITE, PatternType.SKULL));
        bmeta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        talismanOfResurrection.setItemMeta(bmeta);
        talismanOfResurrection.addEnchantment(Utils.glow, 1);

        lore.clear();
        BannerMeta bmeta2 = (BannerMeta) talismanOfRespite.getItemMeta();
        bmeta2.setDisplayName(ChatColor.DARK_PURPLE + "Insignia of Respite");
        lore.add(ChatColor.GRAY + "If you die with this in your");
        lore.add(ChatColor.GRAY + "hotbar, you will be keep all");
        lore.add(ChatColor.GRAY + "items, consuming this item.");
        bmeta2.setLore(lore);
        bmeta2.setBaseColor(DyeColor.LIGHT_BLUE);
        bmeta2.addPattern(new Pattern(DyeColor.WHITE, PatternType.FLOWER));
        bmeta2.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        talismanOfRespite.setItemMeta(bmeta2);
        talismanOfRespite.addEnchantment(Utils.glow, 1);
    }
    
    private static final void setFood() {
    ItemMeta meta = cookedSeeds.getItemMeta();
    meta.setLore(foodLore);
        
        meta.setDisplayName(ChatColor.RESET + "Roasted Seeds");
        cookedSeeds.setItemMeta(meta);
        meta.setDisplayName(ChatColor.RESET + "Berries");
        berries.setItemMeta(meta);
        meta.setDisplayName(ChatColor.RESET + "Cheese");
        cheese.setItemMeta(meta);
    }
    
    private static final void setTeleTabs() {
        ItemMeta meta = houseTab.getItemMeta();
        meta.setDisplayName(ChatColor.RESET + "Teleport to House");
        List<String> lore = new ArrayList<String>();
        lore.add(ChatColor.GRAY + "Teleports user to his/her home bed.");
        lore.add(ChatColor.GRAY + "Does not work while user is slowed.");
        meta.setLore(lore);
        houseTab.setItemMeta(meta);
        
        meta.setDisplayName(ChatColor.RESET + "Teleport to Spawn");
        lore.clear();
        lore.add(ChatColor.GRAY + "Teleports user to the world spawn.");
        lore.add(ChatColor.GRAY + "Does not work while user is slowed.");
        meta.setLore(lore);
        spawnTab.setItemMeta(meta);

        meta.setDisplayName(ChatColor.RESET + "Teleport to Clan");
        lore.clear();
        lore.add(ChatColor.GRAY + "Teleports user to his/her Clan Hub.");
        lore.add(ChatColor.GRAY + "Does not work while user is slowed.");
        meta.setLore(lore);
        clanTab.setItemMeta(meta);
    }
    
    private static final void setWater() {
        ItemMeta meta = hotWater.getItemMeta();
        meta.setDisplayName(ChatColor.RESET + "Hot Water Bottle");
        hotWater.setItemMeta(meta);
        hotWater.setDurability((short) 16327);
        
        meta.setDisplayName(ChatColor.RESET + "Cold Water Bottle");
        coldWater.setItemMeta(meta);
        coldWater.setDurability((short) 16335);
        
        meta.setDisplayName(ChatColor.RESET + "Dirty Water Bottle");
        dirtyWater.setItemMeta(meta);
        dirtyWater.setDurability((short) 16343);
        
        meta.setDisplayName(ChatColor.RESET + "Saltwater Bottle");
        saltWater.setItemMeta(meta);
        saltWater.setDurability((short) 16351);
        
        meta.setDisplayName(ChatColor.RESET + "Cactus Juice");
        cactusWater.setItemMeta(meta);
        cactusWater.setDurability((short) 16359);

        boilingWater.setDurability((short) 10317);
        PotionMeta pmeta = (PotionMeta) boilingWater.getItemMeta();
        pmeta.setDisplayName(ChatColor.RESET + "Boiling Water Bottle");
        pmeta.addCustomEffect(new PotionEffect(PotionEffectType.WATER_BREATHING, 20, 0), true);
        pmeta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        boilingWater.setItemMeta(pmeta);

        pmeta.setDisplayName(ChatColor.RESET + "Freezing Water Bottle");
        freezingWater.setDurability((short) 10333);
        freezingWater.setItemMeta(pmeta);
    }
    
    private static final void setHealSpell() {
        FireworkEffectMeta meta = (FireworkEffectMeta) healingSpell.getItemMeta();
        meta.setEffect(FireworkEffect.builder().withColor(Color.ORANGE).withFade(Color.YELLOW).build());
        meta.setDisplayName(ChatColor.YELLOW + "" + ChatColor.BOLD + "Healing Spell");
        List<String> lore = new ArrayList<String>();
            lore.add(ChatColor.GRAY + "Creates a Ring of Healing that heals");
            lore.add(ChatColor.GRAY + "all players within its radius.");
        meta.setLore(lore);
        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        healingSpell.setItemMeta(meta);
        healingSpell.addEnchantment(Utils.glow, 1);
    }
    
    private static final void setRageSpell() {
        FireworkEffectMeta meta = (FireworkEffectMeta) rageSpell.getItemMeta();
        meta.setEffect(FireworkEffect.builder().withColor(Color.PURPLE).withFade(Color.FUCHSIA).build());
        meta.setDisplayName(ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "Rage Spell");
        List<String> lore = new ArrayList<String>();
            lore.add(ChatColor.GRAY + "Creates a Ring of Rage that boosts");
            lore.add(ChatColor.GRAY + "Strength and Speed for all players");
            lore.add(ChatColor.GRAY + "within its radius.");
        meta.setLore(lore);
        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        rageSpell.setItemMeta(meta);
        rageSpell.addEnchantment(Utils.glow, 1);
    }
    
    private static final void setHopeSpell() {
        FireworkEffectMeta meta = (FireworkEffectMeta) hopeSpell.getItemMeta();
        meta.setEffect(FireworkEffect.builder().withColor(Color.AQUA).withFade(Color.WHITE).build());
        meta.setDisplayName(ChatColor.BLUE + "" + ChatColor.BOLD + "Hope Spell");
        List<String> lore = new ArrayList<String>();
            lore.add(ChatColor.GRAY + "Creates a Ring of Hope that restores");
            lore.add(ChatColor.GRAY + "Hydration and Sanity of all players");
            lore.add(ChatColor.GRAY + "within its radius.");
        meta.setLore(lore);
        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        hopeSpell.setItemMeta(meta);
        hopeSpell.addEnchantment(Utils.glow, 1);
    }
    
    private static final void setForgeSpell() {
        FireworkEffectMeta meta = (FireworkEffectMeta) forgeSpell.getItemMeta();
        meta.setEffect(FireworkEffect.builder().withColor(Color.ORANGE).withFade(Color.MAROON).build());
        meta.setDisplayName(ChatColor.GOLD + "" + ChatColor.BOLD + "Forge Spell");
        List<String> lore = new ArrayList<String>();
            lore.add(ChatColor.GRAY + "Creates a Ring of Forging that smelts");
            lore.add(ChatColor.GRAY + "ores, charcoals logs, and cooks food of");
            lore.add(ChatColor.GRAY + "all players within its radius.");
        meta.setLore(lore);
        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        forgeSpell.setItemMeta(meta);
        forgeSpell.addEnchantment(Utils.glow, 1);
    }
    
    private static final void setFlashSpell() {
        FireworkEffectMeta meta = (FireworkEffectMeta) flashSpell.getItemMeta();
        meta.setEffect(FireworkEffect.builder().withColor(Color.WHITE).withFade(Color.YELLOW).build());
        meta.setDisplayName(ChatColor.WHITE + "" + ChatColor.BOLD + "Flash Spell");
        List<String> lore = new ArrayList<String>();
            lore.add(ChatColor.GRAY + "Allows the user to run faster than the");
            lore.add(ChatColor.GRAY + "winds, albeit with a heavy toll on hunger");
            lore.add(ChatColor.GRAY + "and hydration when the effect expires.");
        meta.setLore(lore);
        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        flashSpell.setItemMeta(meta);
        flashSpell.addEnchantment(Utils.glow, 1);
    }
    
    private static final void setSneakingSpell() {
        FireworkEffectMeta meta = (FireworkEffectMeta) sneakingSpell.getItemMeta();
        meta.setEffect(FireworkEffect.builder().withColor(Color.BLACK).withFade(Color.SILVER).build());
        meta.setDisplayName(ChatColor.GRAY + "" + ChatColor.BOLD + "Shadow Spell");
        List<String> lore = new ArrayList<String>();
            lore.add(ChatColor.GRAY + "Cloaks all players close to the user");
            lore.add(ChatColor.GRAY + "with shadow, causing all hostile mobs");
            lore.add(ChatColor.GRAY + "to ignore them for a while.");
        meta.setLore(lore);
        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        sneakingSpell.setItemMeta(meta);
        sneakingSpell.addEnchantment(Utils.glow, 1);
    }
    
    private static final void setFreezeSpell() {
        FireworkEffectMeta meta = (FireworkEffectMeta) freezeSpell.getItemMeta();
        meta.setEffect(FireworkEffect.builder().withColor(Color.fromBGR(255, 239, 191)).build());
        meta.setDisplayName(ChatColor.AQUA + "" + ChatColor.BOLD + "Freeze Spell");
        List<String> lore = new ArrayList<String>();
            lore.add(ChatColor.GRAY + "Freezes EVERYTHING within the spell's");
            lore.add(ChatColor.GRAY + "radius except for the user, including");
            lore.add(ChatColor.GRAY + "entities, liquids, and blocks.");
        meta.setLore(lore);
        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        freezeSpell.setItemMeta(meta);
        freezeSpell.addEnchantment(Utils.glow, 1);
    }
    
    private static final void setLightningSpell() {
        FireworkEffectMeta meta = (FireworkEffectMeta) lightningSpell.getItemMeta();
        meta.setEffect(FireworkEffect.builder().withColor(Color.BLUE).withFade(Color.NAVY).build());
        meta.setDisplayName(ChatColor.BLUE + "" + ChatColor.BOLD + "Lightning Spell");
        List<String> lore = new ArrayList<String>();
            lore.add(ChatColor.GRAY + "Throws a Lightning Orb that activates");
            lore.add(ChatColor.GRAY + "after 3 seconds, striking the area around");
            lore.add(ChatColor.GRAY + "it with several bolts of lightning.");
        meta.setLore(lore);
        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
        meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        lightningSpell.setItemMeta(meta);
        lightningSpell.addEnchantment(Utils.glow, 1);
    }
    
    /*public static final ItemStack getXPBook(Skill skill, int amount) {
        ItemStack i = xpBook.clone();
        ItemMeta meta = i.getItemMeta();
        List<String> lore = meta.getLore();
        lore.add(ChatColor.GOLD + "Skill: " + ChatColor.YELLOW + Main.capitalizeFirst(skill.toString()));
        lore.add(ChatColor.DARK_GREEN + "XP Amount: " + ChatColor.GREEN + amount);
        meta.setLore(lore);
        i.setItemMeta(meta);
        return i;
    }
    public static final ItemStack getXPBook(String skillname, int itemAmount, int XP) {
        ItemStack i = xpBook.clone();
        ItemMeta meta = i.getItemMeta();
        Skill skill = Skill.valueOf(skillname.toUpperCase());
        List<String> lore = meta.getLore();
        lore.add(ChatColor.GOLD + "Skill: " + ChatColor.YELLOW + Main.capitalizeFirst(skill.toString()));
        lore.add(ChatColor.DARK_GREEN + "XP Amount: " + ChatColor.GREEN + XP);
        meta.setLore(lore);
        i.setItemMeta(meta);
        i.setAmount(itemAmount);
        return i;
    }*/

}
