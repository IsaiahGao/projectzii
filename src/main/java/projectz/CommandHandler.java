package projectz;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.entity.Spellcaster;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.FixedMetadataValue;

import cwhitelist.DelayedTeleport;
import projectz.Utilities.CoolDownReason;
import projectz.bosses.BossHandler.BossMob;
import projectz.clans.Clan;
import projectz.clans.Clan.Role;
import projectz.clans.ClanInvite.InviteAction;
import projectz.clans.ClanPerks;
import projectz.clans.Member;
import projectz.clans.RegionHandler;
import projectz.listeners.ChatListener;
import projectz.spells.Spell;
import projectz.spells.SpellCreationHandler.SpellObject;
import projectz.treasure.CasketHandler;
import projectz.treasure.ClueScrollHandler;
import utilities.Utils;
import utilities.gui.ActionBar;

public class CommandHandler implements CommandExecutor {
    
    private static Set<UUID> debugging = new HashSet<UUID>();
    
    public static boolean debugging(Player p) {
        return debugging.contains(p.getUniqueId());
    }
    
    @Override
    @SuppressWarnings("deprecation")
    public boolean onCommand(CommandSender sender, Command commandLabel, String cmd, String[] args) {
        if (cmd.equals("pzhelp")) {
            if (args.length == 0) return false;
            String s = args[0];
                if (s.equalsIgnoreCase("hydration")) {
                    for (String str : Main.hydrationHelp)
                        sender.sendMessage(str);
                    return true;
                }
                if (s.equalsIgnoreCase("sanity")) {
                    for (String str : Main.sanityHelp)
                        sender.sendMessage(str);
                    return true;
                }
                /*if (s.equalsIgnoreCase("world")) {
                    for (String str : Main.worldHelp)
                        sender.sendMessage(str);
                    return true;
                }*/
                if (s.equalsIgnoreCase("monsters")) {
                    for (String str : Main.monstersHelp)
                        sender.sendMessage(str);
                    return true;
                }
                if (s.equalsIgnoreCase("spells")) {
                    for (String str : Main.spellsHelp)
                        sender.sendMessage(str);
                    return true;
                }
                /*if (s.equalsIgnoreCase("abilities")) {
                    for (String str : Main.abilitiesHelp)
                        sender.sendMessage(str);
                    return true;
                }
                if (s.equalsIgnoreCase("skills")) {
                    for (String str : Main.skillsHelp)
                        sender.sendMessage(str);
                    return true;
                }*/
                if (s.equalsIgnoreCase("clans")) {
                    for (String str : Main.clanHelp)
                        sender.sendMessage(str);
                    return true;
                }
                if (s.equalsIgnoreCase("clancommands")) {
                    if (args.length > 1 && Utilities.isInt(args[1])) {
                        int page = Integer.parseInt(args[1]);
                        if (page == 1)
                            for (String str : Main.clancommandHelp)
                                sender.sendMessage(str);
                        else if (page == 2)
                            for (String str : Main.clancommandHelp2)
                                sender.sendMessage(str);
                        else
                            for (String str : Main.clancommandHelp3)
                                sender.sendMessage(str);
                    } else for (String str : Main.clancommandHelp)
                        sender.sendMessage(str);
                    return true;
                }
                if (s.equalsIgnoreCase("clanlevels")) {
                    for (String str : Main.clanLevelsHelp)
                        sender.sendMessage(str);
                    return true;
                }
                if (s.equalsIgnoreCase("doubleyield")) {
                    for (String str : Main.clanDoubleYieldHelp)
                        sender.sendMessage(str);
                    return true;
                }
        }
        if (cmd.equalsIgnoreCase("pzcommands") || cmd.equalsIgnoreCase("pzcmds")) {
            for (String str : Main.commandsHelp)
                sender.sendMessage(str);
            return true;
        }
        if (cmd.equalsIgnoreCase("invite")) {
            // TODO
            if (sender instanceof Player) {
                Player p = (Player) sender;
                if (args.length < 1) {
                    p.sendMessage(ChatColor.RED + "Error: " + ChatColor.GRAY + "Use as �6/invite �e[Accept/Deny]�7.");
                    return true;
                }
                if (args[0].equalsIgnoreCase("accept") || args[0].equalsIgnoreCase("yes")) {
                    if (!Main.getInviteHandler().hasPendingInvitation(p))
                        Main.getInviteHandler().handleInviteResponse(p, InviteAction.NOT_INVITED);
                    else if (RegionHandler.getClan(p) != null)
                        Main.getInviteHandler().handleInviteResponse(p, InviteAction.ALREADY_IN_CLAN);
                    else if (Main.getInviteHandler().getInviteClan(p) == null)
                        Main.getInviteHandler().handleInviteResponse(p, InviteAction.CLAN_NULLED);
                    else
                        Main.getInviteHandler().handleInviteResponse(p, InviteAction.ACCEPTED);
                } else if (args[0].equalsIgnoreCase("deny") || args[0].equalsIgnoreCase("decline") || args[0].equalsIgnoreCase("no")) {
                    Main.getInviteHandler().handleInviteResponse(p, InviteAction.DENIED);
                } else if (args[0].equalsIgnoreCase("fuck") && args.length > 1 && args[1].equalsIgnoreCase("no")) {
                    Main.getInviteHandler().handleInviteResponse(p, InviteAction.DENIED_WITH_PASSION);
                } else {
                    p.sendMessage(ChatColor.RED + "Error: " + ChatColor.GRAY + "Use as �6/invite �e[Accept/Deny]�7.");
                }
                return true;
            }
        }
        if (cmd.equalsIgnoreCase("map")) {
            if (sender instanceof Player) {
                Player p = (Player) sender;
                if (Utilities.cooledDown(p, CoolDownReason.COMMAND, 3)) {
                    Chunk center = p.getLocation().getChunk();
                    if (!center.getWorld().getName().equals(Main.getOverworld().getName())) {
                        p.sendMessage("�7Only the Overworld may be claimed.");
                        return true;
                    }
                    
                    Location faceNorth = p.getLocation();
                    faceNorth.setYaw(90F);
                    p.teleport(faceNorth, TeleportCause.UNKNOWN);
                    
                    p.sendMessage("�m===============================================");
                    p.sendMessage("�f�lClan Claims:");
                    for (int x = center.getX() - 3; x <= center.getX() + 3; x++) {
                        String line = "";
                        
                        for (int z = center.getZ() + 3; z >= center.getZ() - 3; z--) {
                            Chunk chunk = Main.getOverworld().getChunkAt(x, z);
                            line += RegionHandler.isClaimed(chunk) ? (RegionHandler.getClan(chunk).equals(RegionHandler.getClan(p)) ? (chunk.equals(center) ? "�a" : "�2"): (chunk.equals(center) ? "�c" : "�4")) + "X " : (chunk.equals(center) ? "�7" : "�8") + "O ";
                        }
                        
                        p.sendMessage(line);
                    }
                    p.sendMessage("�f[�2Green X's�a > �7your Clan�f]");
                    p.sendMessage("�f[�4Red X's �c>�7 other Clans�f]");
                } else {
                    p.sendMessage("�7You may only use this command once every 3 seconds.");
                }
            }
            return true;
        }
        if (sender.isOp()) {
            if (cmd.equals("zspawn")) {
                if (args.length == 0) return false;
                if (args[0].equalsIgnoreCase("spellsall")) {
                    for (Player op : Bukkit.getOnlinePlayers()) {
                        op.getInventory().addItem(SpellObject.HOPE.getItem(true));
                        op.getInventory().addItem(SpellObject.POISON.getItem(true));
                        op.getInventory().addItem(SpellObject.LIGHTNING.getItem(true));
                        op.getInventory().addItem(SpellObject.HEALING.getItem(true));
                        op.getInventory().addItem(SpellObject.FLASH.getItem(true));
                        op.getInventory().addItem(SpellObject.FREEZE.getItem(true));
                    }
                }
                
                if (!(sender instanceof Player)) return false;
                String s = args[0];
                Player p = (Player) sender;
                    if (s.equalsIgnoreCase("spell")) {
                        try {
                            p.getInventory().addItem(SpellObject.valueOf(args[1].toUpperCase()).getItem().clone());
                        } catch (Exception e) {
                            
                        }
                        return true;
                    }
                    if (s.equalsIgnoreCase("toxicpipe")) {
                        p.getInventory().addItem(ItemDef.toxicblowpipe.clone());
                        return true;
                    }
                    if (s.equalsIgnoreCase("firepipe")) {
                        p.getInventory().addItem(ItemDef.incendaryblowpipe.clone());
                        return true;
                    }
                    /*if (s.equalsIgnoreCase("xpbook")) {
                        p.getInventory().addItem(ItemDef.getXPBook(Skill.valueOf(args[1].toUpperCase()), Integer.parseInt(args[2])));
                        return true;
                    }*/
                    if (s.equalsIgnoreCase("caskets")) {
                        p.getInventory().addItem(CasketHandler.RustyCasket);
                        p.getInventory().addItem(CasketHandler.PolishedCasket);
                        p.getInventory().addItem(CasketHandler.OccultCasket);
                        return true;
                    }
                    if (s.equalsIgnoreCase("cluescroll")) {
                        p.getInventory().addItem(ClueScrollHandler.getRandomClueScroll(ClueScrollHandler.ClueScrollDifficulty.valueOf(args[1].toUpperCase())).toItem());
                        return true;
                    }
                    if (s.equalsIgnoreCase("item")) {
                        Material mat = Material.valueOf(args[1]);
                        if (mat != null) {
                            int amount = Utilities.isInt(args[2]) ? Integer.parseInt(args[2]) : 1;
                            short data = Utilities.isInt(args[3]) ? Short.parseShort(args[3]) : 0;
                            p.getInventory().addItem(new ItemStack(mat, amount, data));
                        }
                        return true;
                    }
            }
            if (cmd.equals("projectz") || cmd.equals("pz")) {
                if (args.length == 0) return false;
                if (!(sender instanceof Player)) return false;
                String s = args[0];
                Player p = (Player) sender;
                if (s.equalsIgnoreCase("nocooldown")) {
                    p.addAttachment(Main.getPlugin(), "zday.cooldownbypass", true);
                    p.sendMessage(ChatColor.RED + "Cooldowns disabled!");
                    return true;
                }
                if (s.equalsIgnoreCase("cooldown")) {
                    p.addAttachment(Main.getPlugin(), "zday.cooldownbypass", false);
                    p.sendMessage(ChatColor.GREEN + "Cooldowns enabled!");
                    return true;
                }
                if (s.equalsIgnoreCase("playsound")) {
                    Float pitch = args.length <= 2 ? 1.0F : Float.parseFloat(args[2]);
                    p.playSound(p.getLocation(), Sound.valueOf(args[1].toUpperCase()), 1.0F, pitch);
                    return true;
                }
                if (s.equalsIgnoreCase("setspawn")) {
                    Location l = p.getLocation();
                    p.getWorld().setSpawnLocation((int) l.getX(), (int) l.getY(), (int) l.getZ());
                    p.sendMessage(ChatColor.GREEN + "World spawn set to current location!");
                    return true;
                }
                if (s.equalsIgnoreCase("more")) {
                    if (!p.getItemInHand().getType().equals(Material.AIR))
                        p.getItemInHand().setAmount(64);
                    return true;
                }
                if (s.equalsIgnoreCase("less")) {
                    if (!p.getItemInHand().getType().equals(Material.AIR))
                        p.getItemInHand().setAmount(1);
                    return true;
                }
                if (s.equalsIgnoreCase("heal")) {
                    p.setHealth(p.getMaxHealth());
                    p.sendMessage(ChatColor.DARK_RED + "Healed.");
                    return true;
                }
                if (s.equalsIgnoreCase("feed")) {
                    p.setFoodLevel(20);
                    p.sendMessage(ChatColor.DARK_RED + "Om nom.");
                    return true;
                }
                if (s.equalsIgnoreCase("saturate")) {
                    p.setSaturation(args.length <= 1 ? 5.0F : Float.parseFloat(args[1]));
                    p.sendMessage(ChatColor.DARK_RED + "Saturated.");
                    return true;
                }
                if (s.equalsIgnoreCase("fix")) {
                    try {
                        p.getItemInHand().setDurability((short) 0);
                    } catch (Exception e) {
                        
                    }
                    return true;
                }
                if (s.equalsIgnoreCase("unbreakable")) {
                    try {
                        p.getItemInHand().setDurability((short) 0);
                        ItemMeta meta = p.getItemInHand().getItemMeta();
                        meta.spigot().setUnbreakable(true);
                        p.getItemInHand().setItemMeta(meta);
                    } catch (Exception e) {
                        
                    }
                    return true;
                }
                if (s.equalsIgnoreCase("pickup")) {
                    boolean pickup = Boolean.parseBoolean(args[1]);
                    if (!pickup) p.setMetadata("nopickup", new FixedMetadataValue(Main.getPlugin(), 1));
                    else p.removeMetadata("nopickup", Main.getPlugin());
                    p.sendMessage("Pickup status set to: " + pickup);
                    return true;
                }
                /*if (s.equalsIgnoreCase("enchantrecharge")) {
                    if (p.getItemInHand() != null && !p.getItemInHand().getType().equals(Material.AIR)) {
                        int level = Integer.parseInt(args[1]);
                        Utilities.applyCustomEnchantment(p.getItemInHand(), ItemDef.recharge, level);
                        p.sendMessage("Enchantment successful.");
                        return true;
                    }
                }
                if (s.equalsIgnoreCase("enchantmagicpower")) {
                    if (p.getItemInHand() != null && !p.getItemInHand().getType().equals(Material.AIR)) {
                        int level = Integer.parseInt(args[1]);
                        Utilities.applyCustomEnchantment(p.getItemInHand(), ItemDef.magicpower, level);
                        p.sendMessage("Enchantment successful.");
                        return true;
                    }
                }*/
                if (s.equalsIgnoreCase("showtext")) {
                    new ActionBar().sendMessage(p, ChatColor.translateAlternateColorCodes('&', args[1]));
                    return true;
                }
                if (s.equalsIgnoreCase("debug")) {
                    if (debugging.contains(p.getUniqueId())) {
                        debugging.remove(p.getUniqueId());
                        p.sendMessage(ChatColor.RED + "Debugging: FALSE");
                    } else {
                        debugging.add(p.getUniqueId());
                        p.sendMessage(ChatColor.GREEN + "Debugging: TRUE");
                    }
                    return true;
                }
                if (s.equalsIgnoreCase("enchant")) {
                    if (p.getItemInHand() != null && !p.getItemInHand().getType().equals(Material.AIR)) {
                        try {
                            p.getItemInHand().addUnsafeEnchantment(Enchantment.getById(Integer.parseInt(args[1])), Integer.parseInt(args[2]));
                            p.sendMessage(ChatColor.GREEN + "Enchantment succeeded.");
                        } catch (Exception e) {
                            p.sendMessage(ChatColor.RED + "No such enchantment.");
                        }
                    } else {
                        p.sendMessage(ChatColor.RED + "No item held.");
                    }
                    return true;
                }
                if (s.equalsIgnoreCase("setstaffrank")) {
                    int i = Integer.parseInt(args[2]);
                    Main.staffRank.getScore(args[1]).setScore(i);
                    return true;
                }
                if (s.equalsIgnoreCase("setdonorrank")) {
                    int i = Integer.parseInt(args[2]);
                    Main.donationRank.getScore(args[1]).setScore(i);
                    return true;
                }
                if (s.equalsIgnoreCase("spawnboss")) {
                    try {
                        BossMob b = BossMob.valueOf(args[1]);
                        b.spawn(p.getLocation());
                        p.sendMessage(ChatColor.GREEN + "Spawned " + b.getBoss().getName());
                    } catch (Exception e) {
                        
                    }
                    return true;
                }
            }
        }
        if (cmd.equalsIgnoreCase("clans") || cmd.equalsIgnoreCase("clan") || cmd.equalsIgnoreCase("c")) {
            if (!(sender instanceof Player)) return false;
            Player p = (Player) sender;
            int l = args.length;
            if (l == 0 || args == null) {
                p.sendMessage("�cInvalid Command! �7Try �e/help clanCommands�7.");
                return true;
            }
            String s = args[0];
            if (s.equalsIgnoreCase("join")) {
                if (l < 2) {
                    p.sendMessage(ChatColor.RED + "Invalid command usage: �7Use as �6/clan join �e[ClanName (Case Sensitive)]�7.");
                    return true;
                }
                Clan c = RegionHandler.getClan(args[1]);
                if (c == null) {
                    p.sendMessage(ChatColor.RED + "Error: �7" + args[1] + " Clan does not exist.");
                    return true;
                }
                c.sendJoinRequest(p);
                return true;
            }
            if (s.equalsIgnoreCase("create")) {
                if (l < 3) {
                    p.sendMessage(ChatColor.RED + "Invalid command usage: �7Use as �6/clan create �e[ClanName] [ClanTag]�7.");
                    return true;
                }
                Member m = RegionHandler.members.get(p.getUniqueId());
                if (m != null && m.getClan() != null) {
                    p.sendMessage(ChatColor.RED + "Error: �7You cannot create a Clan while in one. Leave that one first.");
                    return true;
                }
                /*if (!p.getInventory().containsAtLeast(new ItemStack(Material.DIAMOND), 8)) {
                    p.sendMessage("�7Creating a Clan costs 8 Diamonds, which you do not have.");
                    return true;
                }*/
                String name = args[1], tag = args[2];
                if (name.length() > 16) {
                    p.sendMessage(ChatColor.RED + "Error: �7Clan names must be 16 characters or less.");
                    return true;
                }
                if (tag.length() > 4) {
                    p.sendMessage(ChatColor.RED + "Error: �7Clan tags must be 4 characters or less.");
                    return true;
                }
                if (RegionHandler.takenTags.contains(tag)) {
                    p.sendMessage(ChatColor.RED + "Error: �7That Clan tag is already taken.");
                    return true;
                }
                if (RegionHandler.claimNames.containsKey(name.toLowerCase())) {
                    p.sendMessage(ChatColor.RED + "Error: �7That Clan already exists.");
                    return true;
                }
                //p.getInventory().removeItem(new ItemStack(Material.DIAMOND, 8));
                Clan c = new Clan(0, name, tag, 1, 1, 1, new HashSet<String>(), new HashMap<UUID, Role>(), new HashMap<Material, Integer>(), 0, 1337, 0);
                Member member = new Member(p.getUniqueId(), name, Role.LEADER);
                c.setLeader(member);
                p.sendMessage(ChatColor.BLUE + "�lClan > �r�7Successfully created new Clan! You are now the Leader of �a" + name + "�7!");
                p.sendMessage(ChatColor.BLUE + "�lClan > �r�7Use �e/help clanCommands �7for Clan Management controls.");
                p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1.0F, 1.2F);
                return true;
            }
            if (s.equalsIgnoreCase("info")) {
                Member member = RegionHandler.getMember(p.getUniqueId());
                if (args.length < 2) {
                    if (member != null) {
                        member.getClan().displayClanInfo(p);
                        return true;
                    }
                    p.sendMessage(ChatColor.GRAY + "You are not in a Clan, so use as �6/clan info �e[name]�7.");
                    return true;
                }
                
                Clan c = RegionHandler.getClan(args[1].toLowerCase());
                if (c == null) {
                    p.sendMessage(ChatColor.GRAY + "A Clan called �f" + args[1] + "�7 does not exist.");
                    return true;
                }
                
                c.displayClanInfo(p);
                return true;
            }
            // if player is in a clan
            if (RegionHandler.members.containsKey(p.getUniqueId())) {
                Member m = RegionHandler.members.get(p.getUniqueId());
                Clan c = m.getClan();
                if (s.equalsIgnoreCase("setname")) {
                    p.sendMessage(ChatColor.RED + "Clan names cannot be changed.");
                    return true;
                }
                if (s.equalsIgnoreCase("settag") || s.equalsIgnoreCase("setprefix")) {
                    if (l < 2) {
                        p.sendMessage(ChatColor.RED + "Invalid command usage: �7Use as �6/clan setTag �e[XXXX]�7.");
                        return true;
                    }
                    if (args[1].length() > 4) {
                        p.sendMessage(ChatColor.RED + "Error: �7Clan tags must be 4 characters or less.");
                        return true;
                    }
                    c.setTag(m, args[1]);
                    return true;
                }
                if (s.equalsIgnoreCase("permission")) {
                    if (l < 3) {
                        p.sendMessage(ChatColor.RED + "Invalid command usage: �7Use as �6/clan permission �e[Recruit/Claim/Resource] [Leader/Co/Captain/Member]�7.");
                        return true;
                    }
                    String topic = args[1],
                            rank = args[2];
                    
                    if (topic.equalsIgnoreCase("recruit")) {
                        if (!c.setWhoCanRecruit(m, rank))
                            p.sendMessage(ChatColor.RED + "Invalid command usage: �7Use as �6/clan permission Recruit �e[Leader/Co/Captain/Member]�7.");
                        else
                            c.sendClanMessage("�e" + Utils.capitalizeFirst(rank).replace("Co", "Co-Leader") + "s �fand up may now �erecuit new members�f!");
                    } else if (topic.equalsIgnoreCase("claim")) {
                        if (!c.setWhoCanClaim(m, rank))
                            p.sendMessage(ChatColor.RED + "Invalid command usage: �7Use as �6/clan permission Claim �e[Leader/Co/Captain/Member]�7.");
                        else
                            c.sendClanMessage("�e" + Utils.capitalizeFirst(rank).replace("Co", "Co-Leader") + "s �fand up may now �eclaim and unclaim land�f!");
                    } else if (topic.equalsIgnoreCase("resource") || topic.equalsIgnoreCase("resources")) {
                        if (!c.setWhoCanWithdraw(m, rank))
                            p.sendMessage(ChatColor.RED + "Invalid command usage: �7Use as �6/clan permission Resource �e[Leader/Co/Captain/Member]�7.");
                        else
                            c.sendClanMessage("�e" + Utils.capitalizeFirst(rank).replace("Co", "Co-Leader") + "s �fand up may now �ewithdraw resources�f!");
                    } else {
                        p.sendMessage(ChatColor.RED + "Invalid command usage: �7Use as �6/clan permission �e[Recruit/Claim/Resource] [Leader/Co/Captain/Member]�7.");
                    }
                    return true;
                }
                /*if (s.equalsIgnoreCase("setpermission")) {
                    if (l < 2) {
                        p.sendMessage(ChatColor.RED + "Invalid command usage: �7Use as '/clan setPermission [Leader/Co/Captain/Member]'.");
                        return true;
                    }
                    if (c.setWhoCanClaim(m, args[1]))
                        c.sendClanMessage("�eClaims�f can now be modified by �eRank " + Role.getRole(Integer.parseInt(args[1])) + " �fand up!");
                    return true;
                }
                if (s.equalsIgnoreCase("setresourceaccess")) {
                    if (l < 2) {
                        p.sendMessage(ChatColor.RED + "Invalid command usage: �7Use as '/clan setResourceAccess [Leader/Co/Captain/Member]'.");
                        return true;
                    }
                    if (c.setWhoCanWithdraw(m, args[1]))
                        c.sendClanMessage("�eResources�f can now be accessed by �eRank " + Role.getRole(Integer.parseInt(args[1])) + " �fand up!");
                    return true;
                }
                if (s.equalsIgnoreCase("setrecruitpermission")) {
                    if (l < 2) {
                        p.sendMessage(ChatColor.RED + "Invalid command usage: �7Use as '/clan setResourceAccess [Leader/Co/Captain/Member]'.");
                        return true;
                    }
                    if (c.setWhoCanWithdraw(m, args[1]))
                        c.sendClanMessage("�eResources�f can now be accessed by �eRank " + Role.getRole(Integer.parseInt(args[1])) + " �fand up!");
                    return true;
                }*/
                if (s.equalsIgnoreCase("leave")) {
                    if (p.hasMetadata("clan.leave")) {
                        c.attemptLeave(m);
                        p.removeMetadata("clan.leave", Main.getPlugin());
                    } else {
                        p.sendMessage(ChatColor.RED + "Are you sure you want to leave?" + ChatColor.GRAY + " Use the command again to confirm.");
                        p.setMetadata("clan.leave", new FixedMetadataValue(Main.getPlugin(), 1));
                    }
                    return true;
                }
                if (s.equalsIgnoreCase("invite")) {
                    // TODO
                    if (l < 2) {
                        p.sendMessage(ChatColor.RED + "Invalid command usage: �7Use as �6/clan invite �e[{Name}/cancel {Name}]�7.");
                        return true;
                    }
                    if (args[1].equalsIgnoreCase("cancel")) {
                        if (l < 3) {
                            p.sendMessage(ChatColor.RED + "Invalid command usage: �7Use as �6/clan invite �e[{Name}/cancel {Name}]�7.");
                            return true;
                        }
                        if (Bukkit.getPlayer(args[2]).isOnline()) {
                            Player p2 = Bukkit.getPlayer(args[2]);
                            if (Main.getInviteHandler().getInviteClan(p2).equals(c))
                                Main.getInviteHandler().handleInviteResponse(p, InviteAction.CANCELLED);
                            else
                                p.sendMessage(ChatColor.GRAY + "That player does not currently have an Invitation to your Clan.");
                            return true;
                        }
                        p.sendMessage(ChatColor.GRAY + "That player is not online.");
                        return true;
                    }
                    Main.getInviteHandler().sendClanInvite(Bukkit.getOfflinePlayer(args[1]), m);
                    return true;
                }
                if (s.equalsIgnoreCase("accept")) {
                    if (l < 2) {
                        p.sendMessage(ChatColor.RED + "Invalid command usage: �7Use as �6/clan accept/deny �e[Name]�7.");
                        return true;
                    }
                    c.handleJoinRequest(m, Bukkit.getOfflinePlayer(args[1]), true);
                    return true;
                }
                if (s.equalsIgnoreCase("deny")) {
                    if (l < 2) {
                        p.sendMessage(ChatColor.RED + "Invalid command usage: �7Use as �6/clan accept/deny �e[Name]�7.");
                        return true;
                    }
                    c.handleJoinRequest(m, Bukkit.getOfflinePlayer(args[1]), false);
                    return true;
                }
                if (s.equalsIgnoreCase("promote")) {
                    if (l < 2) {
                        p.sendMessage(ChatColor.RED + "Invalid command usage: �7Use as �6/clan promote/demote/kick �e[Name]�7.");
                        return true;
                    }
                    Member m2 = RegionHandler.getMember(Bukkit.getOfflinePlayer(args[1]).getUniqueId());
                    if (m2 != null && m2.getClan().equals(c)) {
                        c.attemptPromote(m, m2);
                    } else {
                        p.sendMessage(ChatColor.RED + "Error: �Player either does not exist or is not in your Clan.");
                    }
                    return true;
                }
                if (s.equalsIgnoreCase("demote")) {
                    if (l < 2) {
                        p.sendMessage(ChatColor.RED + "Invalid command usage: �7Use as �6/clan promote/demote/kick �e[Name]�7.");
                        return true;
                    }
                    Member m2 = RegionHandler.getMember(Bukkit.getOfflinePlayer(args[1]).getUniqueId());
                    if (m2 != null && m2.getClan().equals(c)) {
                        c.attemptDemote(m, m2);
                    } else {
                        p.sendMessage(ChatColor.RED + "Error: �7Player either does not exist or is not in your Clan.");
                    }
                    return true;
                }
                if (s.equalsIgnoreCase("kick")) {
                    if (l < 2) {
                        p.sendMessage(ChatColor.RED + "Invalid command usage: �7Use as �6/clan promote/demote/kick �e[Name]�7.");
                        return true;
                    }
                    Member m2 = RegionHandler.getMember(Bukkit.getOfflinePlayer(args[1]).getUniqueId());
                    if (m2 != null && m2.getClan().equals(c)) {
                        c.attemptKick(m, m2);
                    } else {
                        p.sendMessage(ChatColor.RED + "Error: �7Player either does not exist or is not in your Clan.");
                    }
                    return true;
                }
                if (s.equalsIgnoreCase("resources")) {
                    if (RegionHandler.getClan(p.getLocation().getChunk()) == m.getClan())
                        p.openInventory(c.getResourceInterface());
                    else
                        p.sendMessage(ChatColor.GRAY + "You can only access Clan Resources on Clan Territory.");
                    return true;
                }
                if (s.equalsIgnoreCase("perks")) {
                    ClanPerks.displayClanPerks(p);
                    return true;
                }
                if (s.equalsIgnoreCase("upgrade")) {
                    c.attemptLevelUp(m);
                    return true;
                }
                if (s.equalsIgnoreCase("requests")) {
                    c.displayJoinRequests(p);
                    return true;
                }
                if (s.equalsIgnoreCase("members")) {
                    c.displayClanMembers(p, args.length < 2 ? 1 : Integer.parseInt(args[1]));
                    return true;
                }
                if (s.equalsIgnoreCase("chunks")) {
                    c.displayChunksLeft(p);
                    return true;
                }
                if (s.equalsIgnoreCase("message")) {
                    if (l < 2) {
                        p.sendMessage(ChatColor.RED + "Invalid command usage: �7Use as �6/clan message �e[Message]�7.");
                        return true;
                    }
                    String masterstring = "";
                    for (int i = 1; i < args.length; i++)
                        masterstring += args[i] + " ";
                    c.sendClanMessage(m, masterstring);
                    return true;
                }
                if (s.equalsIgnoreCase("claim")) {
                    c.addChunk(m, p.getLocation().getChunk());
                    return true;
                }
                if (s.equalsIgnoreCase("unclaim")) {
                    c.removeChunk(m, p.getLocation().getChunk());
                    return true;
                }
                if (s.equalsIgnoreCase("unclaimall") || s.equalsIgnoreCase("clear")) {
                    c.removeAllChunks(m);
                    return true;
                }
                if (s.equalsIgnoreCase("sethub") || s.equalsIgnoreCase("sethome")) {
                    c.attemptChangeClanSpawn(m);
                    return true;
                }
                if (s.equalsIgnoreCase("home")) {
                    Location to = c.getClanSpawn() == null ? Main.getOverworld().getHighestBlockAt(Main.getOverworld().getSpawnLocation()).getLocation().add(0, 1, 0) : c.getClanSpawn();
                    DelayedTeleport.delayedTeleport(p, to, 10);
                    return true;
                }
            } else {
                p.sendMessage(ChatColor.GRAY + "You are not in a Clan. Use �6/clan create �e[name] [tag]�7 to make one.");
                return true;
            }
        }
        /*if (cmd.equalsIgnoreCase("levels") || cmd.equalsIgnoreCase("stats")) {
            if (!(sender instanceof Player)) return false;
            SkillAssistant.openSkillMenu((Player) sender);
            return true;
        }*/
    return false;
    }

}
