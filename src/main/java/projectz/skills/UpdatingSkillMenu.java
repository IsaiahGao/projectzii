package projectz.skills;

/*import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import zombies.ItemDef;

public class UpdatingSkillMenu {

    private static final ItemStack DEFENSE = new ItemStack(Material.IRON_CHESTPLATE, 1);
    private static final ItemStack MELEE = new ItemStack(Material.IRON_SWORD, 1);
    private static final ItemStack MAGIC = new ItemStack(Material.BLAZE_ROD, 1);
    private static final ItemStack RANGED = new ItemStack(Material.BOW, 1);
    private static final ItemStack KNOWLEDGE = new ItemStack(Material.BOOK, 1);
    private static final ItemStack GATHERING = new ItemStack(Material.IRON_PICKAXE, 1);
    static {
        DEFENSE.addEnchantment(ItemDef.glow, 1);
        MELEE.addEnchantment(ItemDef.glow, 1);
        RANGED.addEnchantment(ItemDef.glow, 1);
        MAGIC.addEnchantment(ItemDef.glow, 1);
        KNOWLEDGE.addEnchantment(ItemDef.glow, 1);
        GATHERING.addEnchantment(ItemDef.glow, 1);
        
        List<String> lores = new ArrayList<String>();
        lores.add(ChatColor.GRAY + "Standard combat skill.");
        lores.add(ChatColor.GRAY + "Deal melee damage to earn experience");
        lores.add("");
        lores.add("");
        lores.add(ChatColor.GREEN + "Current XP: " + ChatColor.RESET + "0");
        lores.add(ChatColor.GOLD + "Next Level: " + ChatColor.YELLOW + "0");
        lores.add("");
        ItemMeta meta = MELEE.getItemMeta();
        meta.setLore(lores);
        meta.setDisplayName(ChatColor.RED + "" + ChatColor.BOLD + "Melee");
        MELEE.setItemMeta(meta);

        lores.clear();
        lores.add(ChatColor.GRAY + "Standard combat skill.");
        lores.add(ChatColor.GRAY + "Deal arrow damage to earn experience");
        lores.add("");
        lores.add("");
        lores.add(ChatColor.GREEN + "Current XP: " + ChatColor.RESET + "0");
        lores.add(ChatColor.GOLD + "Next Level: " + ChatColor.YELLOW + "0");
        lores.add("");
        meta.setLore(lores);
        meta.setDisplayName(ChatColor.GREEN + "" + ChatColor.BOLD + "Ranged");
        RANGED.setItemMeta(meta);

        lores.clear();
        lores.add(ChatColor.GRAY + "Standard combat skill.");
        lores.add(ChatColor.GRAY + "Deal spell damage to earn experience");
        lores.add("");
        lores.add("");
        lores.add(ChatColor.GREEN + "Current XP: " + ChatColor.RESET + "0");
        lores.add(ChatColor.GOLD + " Next Level: " + ChatColor.YELLOW + "0");
        lores.add("");
        meta.setLore(lores);
        meta.setDisplayName(ChatColor.BLUE + "" + ChatColor.BOLD + "Magic");
        MAGIC.setItemMeta(meta);

        lores.clear();
        lores.add(ChatColor.GRAY + "Standard combat skill.");
        lores.add(ChatColor.GRAY + "Survive damage to earn experience");
        lores.add("");
        lores.add("");
        lores.add(ChatColor.GREEN + "Current XP: " + ChatColor.RESET + "0");
        lores.add(ChatColor.GOLD + " Next Level: " + ChatColor.YELLOW + "0");
        lores.add("");
        meta.setLore(lores);
        meta.setDisplayName(ChatColor.WHITE + "" + ChatColor.BOLD + "Defense");
        DEFENSE.setItemMeta(meta);
        

        lores.clear();
        lores.add(ChatColor.GRAY + "Standard artisan skill.");
        lores.add(ChatColor.GRAY + "Kill monsters, forge spells, or");
        lores.add(ChatColor.GRAY + "enchant items to gain experience");
        lores.add("");
        lores.add(ChatColor.GREEN + "Current XP: " + ChatColor.RESET + "0");
        lores.add(ChatColor.GOLD + " Next Level: " + ChatColor.YELLOW + "0");
        lores.add("");
        meta.setLore(lores);
        meta.setDisplayName(ChatColor.LIGHT_PURPLE + "" + ChatColor.BOLD + "Knowledge");
        KNOWLEDGE.setItemMeta(meta);
        

        lores.clear();
        lores.add(ChatColor.GRAY + "Standard artisan skill.");
        lores.add(ChatColor.GRAY + "Mine ores, chop trees, catch fish,");
        lores.add(ChatColor.GRAY + "or harvest crops to gain experience.");
        lores.add("");
        lores.add(ChatColor.GREEN + "Current XP: " + ChatColor.RESET + "0");
        lores.add(ChatColor.GOLD + " Next Level: " + ChatColor.YELLOW + "0");
        lores.add("");
        meta.setLore(lores);
        meta.setDisplayName(ChatColor.YELLOW + "" + ChatColor.BOLD + "Gathering");
        GATHERING.setItemMeta(meta);
    }


    public UpdatingSkillMenu(Player p) {
        this.p = p;
        this.menu = Bukkit.createInventory(null, 36, "Stats (" + p.getEntityId() + ")");
        menu.setMaxStackSize(127);
        setupMenu();
        thingies.add(this.Melee);
        thingies.add(this.Ranged);
        thingies.add(this.Magic);
        thingies.add(this.Defense);
        thingies.add(this.Knowledge);
        thingies.add(this.Gathering);
    }
    
    private Player p;
    private Inventory menu;
    private ItemStack Melee = MELEE.clone();
    private ItemStack Ranged = RANGED.clone();
    private ItemStack Magic = MAGIC.clone();
    private ItemStack Knowledge = KNOWLEDGE.clone();
    private ItemStack Gathering = GATHERING.clone();
    private ItemStack Defense = DEFENSE.clone();
    private List<ItemStack> thingies = new ArrayList<ItemStack>();
    
    private void setupMenu() {
        menu.setMaxStackSize(127);

        menu.setItem(12, this.Melee);
        menu.setItem(13, this.Ranged);
        menu.setItem(14, this.Magic);
        menu.setItem(21, this.Defense);
        menu.setItem(22, this.Knowledge);
        menu.setItem(23, this.Gathering);
    }
    
    public void updateMenu() {
        menu.setMaxStackSize(127);
        for (ItemStack i : thingies) {
            Skill skill = SkillAssistant.Skill.valueOf(ChatColor.stripColor(i.getItemMeta().getDisplayName().toUpperCase()));
            int lvl = skill.getLevel(p);
            i.setAmount(lvl);
            ItemMeta meta = i.getItemMeta();
            List<String> lore = meta.getLore();
                lore.set(4, ChatColor.RED + "" + ChatColor.BOLD + "Level: " + ChatColor.WHITE + "" + ChatColor.BOLD + lvl);
                lore.set(5, ChatColor.GREEN + "Current XP: " + ChatColor.RESET + skill.getCurrentExp(p));
                lore.set(6, ChatColor.GOLD + "Next Level: " + ChatColor.YELLOW + skill.getNextExp(p));
                meta.setLore(lore);
                i.setItemMeta(meta);
        }
        setupMenu();
    }
    
    public Inventory getInventory() {
        return menu;
    }
    
}*/
