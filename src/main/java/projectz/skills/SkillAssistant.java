package projectz.skills;

/*import java.util.Map;
import java.util.WeakHashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.Sound;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;

import zombies.Main;

public class SkillAssistant {
    
    private static final Scoreboard board = Main.getMainScoreboard();
    public static Map<Player, UpdatingSkillMenu> menus = new WeakHashMap<Player, UpdatingSkillMenu>();
    
    private static UpdatingSkillMenu getMenu(Player p) {
        if (menus.containsKey(p))
            return menus.get(p);
        UpdatingSkillMenu m = new UpdatingSkillMenu(p);
        menus.put(p, m);
        return m;
    }
    
    public static void openSkillMenu(Player p) {
        UpdatingSkillMenu menu = getMenu(p);
        menu.updateMenu();
        p.openInventory(menu.getInventory());
    }
    
    public enum Skill {
        DEFENSE(0, "Defense", Color.SILVER, ChatColor.AQUA),
        MELEE(1, "Melee", Color.RED, ChatColor.RED),
        RANGED(2, "Ranged", Color.GREEN, ChatColor.GREEN),
        MAGIC(3, "Magic", Color.BLUE, ChatColor.BLUE),
        KNOWLEDGE(4, "Knowledge", Color.PURPLE, ChatColor.LIGHT_PURPLE),
        GATHERING(5, "Gathering", Color.YELLOW, ChatColor.YELLOW);
        
        private Skill(int id, String objective, Color color, ChatColor chat) {
            this.id = id;
            this.objective = objective;
            this.color = color;
            this.chat = chat;
        }
        
        private int id;
        private String objective;
        private Color color;
        private ChatColor chat;
        private double xpMult = Main.getConfiguration().getDouble("XP-Multiplier");
        
        public void setStarterLevel(Player p) {
            board.getObjective(objective + "_next").getScore(p.getName()).setScore(82);
            board.getObjective(objective + "_lvl").getScore(p.getName()).setScore(1);
        }
        
        public void addXP(Player p, int amount) {
            if (getPlayerClass(p) != null && getPlayerClass(p).equals(this)) amount *= 2;
            amount *= xpMult;
            Score s = board.getObjective(objective + "_exp").getScore(p.getName());
            s.setScore(s.getScore() + amount);
            if (leveledUp(p)) {
            p.sendMessage(ChatColor.STRIKETHROUGH + "" + ChatColor.BOLD + "========================================");
            p.sendMessage(chat + "Congratulations! You've leveled up your " + objective + " skill!");
            p.sendMessage(ChatColor.GRAY + "Your " + objective + " level is now " + (board.getObjective(objective + "_lvl").getScore(p.getName()).getScore() + 1) + ".");
            p.sendMessage(ChatColor.STRIKETHROUGH + "" + ChatColor.BOLD + "========================================");
                Score xp = board.getObjective(objective + "_next").getScore(p.getName());
                Score level = board.getObjective(objective + "_lvl").getScore(p.getName());
                    level.setScore(level.getScore() + 1);
                    xp.setScore((int) (getXPToNextLevel(level.getScore()) + xp.getScore()));
                    SkillAssistant.launchFirework(p.getLocation(), color);
                    if (level.getScore() == 99) {
                        p.getWorld().playSound(p.getLocation(), Sound.ENDERDRAGON_DEATH, 1000000F, 2.0F);
                        Bukkit.getServer().broadcastMessage(ChatColor.DARK_RED + "" + ChatColor.BOLD + "[NEWS] " + ChatColor.RESET + ChatColor.RED + p.getName() + " has just achieved Level 99 " + objective + "!");
                        xp.setScore(1337);
                    }
            }
        }
        
        private boolean leveledUp(Player p) {
        if (board.getObjective(objective + "_lvl").getScore(p.getName()).getScore() >= 99) return false;
            return board.getObjective(objective + "_exp").getScore(p.getName()).getScore() >= board.getObjective(objective + "_next").getScore(p.getName()).getScore(); 
        }
        
        public double getXPToNextLevel(int lvl) {
            double e = Math.pow(2, lvl * 0.125);
            double i = Math.floor(0.250 * (300.000 * e + lvl));
            //Bukkit.getLogger().info(lvl + " => XP: " + i);
            return i;
        }
        
        public int getNextExp(Player p) {
            return board.getObjective(objective + "_next").getScore(p.getName()).getScore();
        }
        
        public int getCurrentExp(Player p) {
            return board.getObjective(objective + "_exp").getScore(p.getName()).getScore();
        }

        public int getLevel(Player p) {
            return board.getObjective(objective + "_lvl").getScore(p.getName()).getScore();
        }
    }
    
    public static void launchFirework(Location loc, Color color) {
        Firework f = (Firework) loc.getWorld().spawn(loc, Firework.class);
        FireworkMeta fm = f.getFireworkMeta();
        fm.addEffect(FireworkEffect.builder()
                        .flicker(true)
                        .trail(true)
                        .with(Type.BALL_LARGE)
                        .withColor(color)
                        .build());
        fm.setPower(0);
        f.setFireworkMeta(fm);
    }
    
    public static void setStarterLevels(Player p) {
        for (Skill s : Skill.values()) {
            s.setStarterLevel(p);
        }
        Main.playerclass.getScore(p.getName()).setScore(69);
    }
    
    public static Skill getPlayerClass(Player p) {
        int i = Main.playerclass.getScore(p.getName()).getScore();
        switch (i) {
        case 0:
            return Skill.DEFENSE;
        case 1:
            return Skill.MELEE;
        case 2:
            return Skill.MAGIC;
        case 3:
            return Skill.RANGED;
        case 4:
            return Skill.KNOWLEDGE;
        case 5:
            return Skill.GATHERING;
            default:
                return null;
        }
    }
    
    public static int getPlayerClassInt(Player p) {
        return Main.playerclass.getScore(p.getName()).getScore();
    }
}*/