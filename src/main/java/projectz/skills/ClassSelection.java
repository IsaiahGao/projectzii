package projectz.skills;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import projectz.Utilities;

public class ClassSelection {
    
    private static Inventory classlist = Bukkit.createInventory(null, 36, "Class Selection");
    
    private static ItemStack
            knight = new ItemStack(Material.STONE_SWORD),
            mage = new ItemStack(Material.STICK), 
            archer = new ItemStack(Material.BOW), 
            tank = new ItemStack(Material.LEATHER_CHESTPLATE), 
            artisan = new ItemStack(Material.STONE_PICKAXE), 
            sage = new ItemStack(Material.BOOK);
    
    private static String[] knightlore = {
            "�5Melee fighter, proficient with a sword!",
            "",
            "�7Receives �eStone Sword�7 to start. XP gains",
            "�7in the Melee skill are doubled.",
            "�7Deals �a1.0�7 extra Melee damage per attack",
            "�7but Magic damage is reduced by �c1.0�7."
    }, magelore = {
            "�5Magic fighter, proficient with spells!",
            "",
            "�7Receives �eStick �7and �eGlass Bottle �7to start.",
            "�7XP gains in the Magic skill are doubled.",
            "�7Deals �a1.0�7 extra magic damage, but",
            "�7Ranged damage is reduced by �c1.0�7."
    }, archerlore = {
            "�5Ranged fighter, proficient with bows!",
            "",
            "�7Receives �eBow �7and �e48 x Arrows �7to start.",
            "�7XP gains in the Ranged skill are doubled.",
            "�7Deals �a1.0�7 extra arrow damage, but",
            "�7Melee damage is reduced by �c1.0�7."
    }, tanklore = {
            "�5Defensive fighter, proficient at defending!",
            "",
            "�7Receives �eFull Leather Armor �7to start.",
            "�7XP gains in the Defense skill are doubled.",
            "�7All damage taken is reduced by �a0.5�7."
    }, artisanlore = {
            "�5Resource gatherer, proficient at collecting!",
            "",
            "�7Receives �eStone Tool Set �7to start.",
            "�7XP gains in the Gathering skill are doubled.",
            "�75% chance to gain double drops while",
            "�7gathering ores, logs, crops, and fish."
    }, sagelore = {
            "�5Wise Guy, proficient at learning things!",
            "",
            "�7Receives �eBook �7and �eGlass Bottle �7to start.",
            "�7XP gains in the Knowledge skill are doubled.",
            "�7XP gains in Vanilla EXP are increased by 25%."
    };
    
    static {
        knight = Utilities.ConstructItemStack(knight, "�c�lKnight", knightlore);
        mage = Utilities.ConstructItemStack(mage, "�9�lMage", magelore);
        archer = Utilities.ConstructItemStack(archer, "�a�lArcher", archerlore);
        tank = Utilities.ConstructItemStack(tank, "�f�lTank", tanklore);
        artisan = Utilities.ConstructItemStack(artisan, "�e�lArtisan", artisanlore);
        sage = Utilities.ConstructItemStack(sage, "�d�lSage", sagelore);
        classlist.setItem(11, knight);
        classlist.setItem(12, archer);
        classlist.setItem(13, mage);
        classlist.setItem(14, tank);
        classlist.setItem(15, artisan);
        classlist.setItem(16, sage);
    }
    
    public static void openClassMenu(Player p) {
        p.openInventory(classlist);
    }

}
