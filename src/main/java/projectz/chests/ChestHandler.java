package projectz.chests;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.block.Sign;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import projectz.Main;
import projectz.clans.RegionHandler;

public class ChestHandler {
    
    public static final List<ItemStack> HUT_LARGE_LOOT = new ArrayList<ItemStack>();
    public static final List<ItemStack> HUT_SMALL_LOOT = new ArrayList<ItemStack>();
    public static final List<ItemStack> SHIPWRECK_LOOT = new ArrayList<ItemStack>();
    public static final List<ItemStack> WATCHTOWER_LOOT = new ArrayList<ItemStack>();
    public static final List<ItemStack> GRAVE_PEASANT_LOOT = new ArrayList<ItemStack>();
    public static final List<ItemStack> GRAVE_NORMAL_LOOT = new ArrayList<ItemStack>();
    public static final List<ItemStack> GRAVE_RICHFAGS_LOOT = new ArrayList<ItemStack>();
    public static final List<ItemStack> OUTPOST_LOOT = new ArrayList<ItemStack>();
    public static final List<ItemStack> ALTAR_LOOT = new ArrayList<ItemStack>();
    public static final List<ItemStack> ICECREAMTRUCK_LOOT = new ArrayList<ItemStack>();
    public static final List<ItemStack> IGLOO_LOOT = new ArrayList<ItemStack>();
    
    private static final Map<String, List<ItemStack>> chestNames = new HashMap<String, List<ItemStack>>();
    static {
        chestNames.put("hut_large", HUT_LARGE_LOOT);
        chestNames.put("hut_small", HUT_SMALL_LOOT);
        chestNames.put("shipwreck", SHIPWRECK_LOOT);
        chestNames.put("watchtower", WATCHTOWER_LOOT);
        chestNames.put("grave_peasant", GRAVE_PEASANT_LOOT);
        chestNames.put("grave_normal", GRAVE_NORMAL_LOOT);
        chestNames.put("grave_richfags", GRAVE_RICHFAGS_LOOT);
        chestNames.put("outpost", OUTPOST_LOOT);
        chestNames.put("altar", ALTAR_LOOT);
        chestNames.put("ice-cream-truck", ICECREAMTRUCK_LOOT);
        chestNames.put("igloo", IGLOO_LOOT);
    }
    
    public enum LootChest { //lootdelay in seconds
        HUT_LARGE("�9�kbighut", HUT_LARGE_LOOT, 86400),
        HUT_SMALL("�9�ksmallhut", HUT_SMALL_LOOT, 86400),
        SHIPWRECK("�9�kshipwreck", SHIPWRECK_LOOT, 86400),
        WATCHTOWER("�9�klolpenis", WATCHTOWER_LOOT, 86400),
        GRAVE_PEASANT("�9�kgravelow", GRAVE_PEASANT_LOOT, 86400),
        GRAVE_NORMAL("�9�kgravemed", GRAVE_NORMAL_LOOT, 86400),
        GRAVE_RICHFAGS("�9�kgravehigh", GRAVE_RICHFAGS_LOOT, 86400),
        OUTPOST("�9�koutpost", OUTPOST_LOOT, 86400),
        ALTAR("�9�kdesertaltar", ALTAR_LOOT, 86400),
        ICECREAMTRUCK("�9�kicecream", ICECREAMTRUCK_LOOT, 86400),
        IGLOO("�9�kigloo", IGLOO_LOOT, 86400);
    
        private String text;
        private List<ItemStack> loot;
        private long lootdelay;
        private static Map<String, LootChest> signtexts = new HashMap<String, LootChest>();
        
        private LootChest(String text, List<ItemStack> loot, long lootdelay) {
            this.text = text;
            this.loot = loot;
            this.lootdelay = lootdelay;
        }
        
        private String getSignText() {
            return text;
        }
        
        private long getDelay() {
            return lootdelay;
        }
        
        public List<ItemStack> getChestLoots() {
            return loot;
        }
        
        static {
            for (LootChest l : LootChest.values()) {
                signtexts.put(l.getSignText(), l);
            }
        }
    
    }
    
    public static boolean isLootable(Chest chest) {
    if (RegionHandler.isClaimed(chest.getBlock().getChunk())) {
        return false;
    }
    Block relative = chest.getBlock().getRelative(0, -2, 0);
        if (relative.getType().equals(Material.WALL_SIGN) || relative.getType().equals(Material.SIGN_POST)) {
            Sign sign = (Sign) relative.getState();
            if (isChestSign(sign))
                if (sign.getLines()[1].equals("") || Long.parseLong(formatTime(System.currentTimeMillis())) - Long.parseLong(sign.getLines()[1]) > LootChest.signtexts.get(sign.getLines()[0]).getDelay())
                    return true;
        }
    return false;
    }
    
    public static boolean isChestSign(Sign sign) {
        return LootChest.signtexts.containsKey(sign.getLines()[0]);
    }
    
    private static String getSignText(Chest chest) {
        Sign sign = (Sign) chest.getBlock().getRelative(0, -2, 0).getState();
        return sign.getLines()[0];
    }
    
    private static LootChest getLootChest(String s) {
        return LootChest.signtexts.get(s);
    }
    
    public static void claimLoot(Chest chest) {
    Inventory i = ((Chest) chest).getBlockInventory();
        LootChest type = getLootChest(getSignText(chest));
        int count = Main.random(Main.chestLootMax - Main.chestLootMin) + Main.chestLootMin;
        for (int j = 0; j < count; j++) {
            ItemStack loot = type.getChestLoots().get(Main.random(type.getChestLoots().size() - 1)).clone();
            loot.setAmount(Main.random(loot.getAmount() - 1) + 1);
            i.setItem(Main.random(26), loot);
        }
    Sign sign = (Sign) chest.getBlock().getRelative(0, -2, 0).getState();
    String time = formatTime(System.currentTimeMillis());
    sign.setLine(1, time);
    sign.update();
    }
    
    private static String formatTime(long currentTime) {
        currentTime = (long) (currentTime/1000);
        String time = Long.toString(currentTime);
        return time.length() > 15 ? time.substring(time.length() - 15) : time;
    }
    
    public static List<ItemStack> getLootTable(String s) {
        return chestNames.get(s);
    }

}
