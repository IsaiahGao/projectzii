package projectz.bosses;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import projectz.Main;

public class BossHandler {
    
    public static final String PREFIX = "�c�lBoss > �f";
    
    private static Map<String, BossAbility> BossAbilities = new HashMap<String, BossAbility>();
    private static Map<LivingEntity, Boss> LivingBosses = new WeakHashMap<LivingEntity, Boss>();
    private static Map<LivingEntity, BukkitTask> AbilityCycles = new WeakHashMap<LivingEntity, BukkitTask>();
    
    public static void load() {
        /*BossAbilities.put("TNTSpray", new TNTSpray());
        BossAbilities.put("ThrowNearby", new ThrowNearby());
        BossAbilities.put("PullDistant", new PullDistant());
        BossAbilities.put("Smash", new Smash());
        BossAbilities.put("SummonBabyZombies", new SummonBabyZombie());
        BossAbilities.put("SummonNazis", new SummonNaziZombie());
        BossAbilities.put("Sieghail", new SiegHail());
        BossAbilities.put("SprayArrows", new SprayArrows());
        BossAbilities.put("Gasnearby", new GasNearby());
        BossAbilities.put("Roottarget", new RootTarget());
        BossAbilities.put("Shock", new Shock());
        BossAbilities.put("Bleed", new Bleed());
        BossAbilities.put("FreezeRandom", new FreezeRandom());
        BossAbilities.put("FreezeAll", new FreezeAll());
        BossAbilities.put("SprayLightning", new SprayLightning());
        BossAbilities.put("Berserk", new Berserk());
        BossAbilities.put("WebAll", new WebAll());
        BossAbilities.put("HeavyPoison", new HeavyPoison());
        BossAbilities.put("SprayFire", new SprayFire());
        BossAbilities.put("SprayMagma", new SprayMagma());
        BossAbilities.put("IgniteNearby", new IgniteNearby());
        BossAbilities.put("NoScope", new NoScope());
        BossAbilities.put("TeleportToRandom", new TeleportToRandom());*/
    }
    
    public enum BossMob {
        /*BONECRUSHER(true, new Bonecrusher(), EntityType.ZOMBIE),
        HITLER(true, new SkeletalHitler(), EntityType.SKELETON),
        TELSA(true, new Telsa(), EntityType.PIG_ZOMBIE),
        SPODER(true, new Spoder(), EntityType.SPIDER, EntityType.CAVE_SPIDER),
        BLAZE(true, new InfernalBlaze(), EntityType.BLAZE),
        ENDERMON(true, new EnderBeast(), EntityType.ENDERMAN);*/
        ;
        
        private BossMob(boolean enabled, Boss b, EntityType... type) {
            this.e = enabled;
            this.b = b;
            this.type = new HashSet<EntityType>();
            for (EntityType et : type)
                this.type.add(et);
        }
        
        private Boss b;
        private boolean e;
        private Set<EntityType> type;
        
        public Boss getBoss() {
            return b;
        }
        
        public boolean isEnabled() {
            return e;
        }
        
        public void spawn(Location loc) {
            b.spawn(loc);
        }
        
        private static Map<EntityType, BossMob> byType = new HashMap<EntityType, BossMob>();
        static {
            for (BossMob bm : BossMob.values())
                for (EntityType et : bm.type)
                    byType.put(et, bm);
        }
        
        public static BossMob get(EntityType type) {
            return byType.get(type);
        }
    }
    
    public static void setAbilityCycle(final LivingEntity ent, boolean start) {
        final Boss boss = LivingBosses.get(ent);
        if (boss == null) return;
        if (start) {
            if (AbilityCycles.containsKey(ent)) return;
            BukkitTask abilityCycle = new BukkitRunnable() {
                @Override
                public void run() {
                    if (ent.isDead()) {
                        AbilityCycles.remove(ent);
                        this.cancel();
                        return;
                    }
                    BossAbility a = boss.getAbilities().get(Main.random(boss.getAbilities().size() - 1));
                    if (a instanceof BossProjectileAbility)
                        ((BossProjectileAbility) a).activate(ent);
                    if (a instanceof BossTargetedAbility)
                        ((BossTargetedAbility) a).activate(ent);
                }
            }.runTaskTimer(Main.getPlugin(), boss.getAbilityDelay() * 20, boss.getAbilityDelay() * 20);
            AbilityCycles.put(ent, abilityCycle);
        } else {
            if (AbilityCycles.get(ent) == null) return;
            AbilityCycles.get(ent).cancel();
            AbilityCycles.remove(ent);
        }
    }
    
    public static void addBoss(LivingEntity e, Boss b) {
        LivingBosses.put(e, b);
    }
    
    public static BossAbility getAbility(String s) {
        return BossAbilities.get(s);
    }
    
    public static Boss getBossByEntity(LivingEntity e) {
        return LivingBosses.get(e);
    }
    
    public static boolean isBoss(LivingEntity e) {
        return LivingBosses.containsKey(e);
    }
    
    public static BukkitTask getAbilityCycle(LivingEntity e) {
        return AbilityCycles.get(e);
    }

}
