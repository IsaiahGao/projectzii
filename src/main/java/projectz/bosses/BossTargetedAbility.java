package projectz.bosses;

import org.bukkit.entity.LivingEntity;

public abstract class BossTargetedAbility extends BossAbility {
    
    public abstract void activate(LivingEntity boss);

}
