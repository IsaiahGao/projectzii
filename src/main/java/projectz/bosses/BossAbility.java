package projectz.bosses;

public abstract class BossAbility {
    
    public abstract String getName();
    
    public abstract boolean announce();
    public abstract String getMessage();

    public abstract double getMinDamage();
    public abstract double getMaxDamage();

}
