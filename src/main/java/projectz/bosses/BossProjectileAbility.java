package projectz.bosses;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Projectile;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public abstract class BossProjectileAbility extends BossAbility {
    
    public abstract double getVelocity1();
    public abstract double getVelocity2();
    
    public abstract void activate(LivingEntity boss);

    public abstract void impact(Projectile projectile);
    public abstract void impact(Projectile projectile, LivingEntity victim, EntityDamageByEntityEvent e);

}
