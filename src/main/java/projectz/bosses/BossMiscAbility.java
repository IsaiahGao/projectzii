package projectz.bosses;

import org.bukkit.entity.LivingEntity;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public abstract class BossMiscAbility extends BossAbility {

    public abstract void activate(LivingEntity boss);
    public abstract void activate(LivingEntity boss, LivingEntity target, EntityDamageByEntityEvent event);

}
