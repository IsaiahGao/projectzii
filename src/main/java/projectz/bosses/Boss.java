package projectz.bosses;

import java.util.List;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;

public interface Boss {
    
    public String getName();
    
    public boolean nameVisible();
    
    public double getMaxHealth();
    
    public int getRespawnTime(); // Set as 0 for 'NO RESPAWN'
    
    public int getAbilityDelay();
    
    public EntityType getType();
    
    public PotionEffect[] getEffects();
    
    public ItemStack[] getArmor();
    
    public ItemStack getWeapon();
    
    public List<ItemStack> getDrops();
    
    public List<ItemStack> getAlwaysDrops();
    
    public int getXPDrop();
    
    public int droppedItemAmount();
    
    public int getContactAbilityChance();
    
    public List<BossMiscAbility> getContactAbilities();
    
    public List<BossAbility> getAbilities();
    
    public Set<DamageCause> getImmunities();
    
    public LivingEntity spawn(Location loc);
    
    public void die(LivingEntity boss);

}
