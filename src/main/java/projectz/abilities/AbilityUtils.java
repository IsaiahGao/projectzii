package projectz.abilities;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import projectz.Main;
import utilities.gui.ActionBar;
import utilities.particles.ParticleEffects;
import utilities.particles.ParticleEffects.OrdinaryColor;

public class AbilityUtils {
    
    private static final DecimalFormat df = new DecimalFormat("0.00");
    
    public static PotionEffectType[] badeffects = {
            PotionEffectType.CONFUSION, PotionEffectType.WITHER,
            PotionEffectType.POISON, PotionEffectType.SLOW,
            PotionEffectType.HUNGER, PotionEffectType.SLOW_DIGGING,
            PotionEffectType.WEAKNESS, PotionEffectType.CONFUSION,
            PotionEffectType.BLINDNESS
    };
    
    public static List<PotionEffectType> badeffectslist = new ArrayList<PotionEffectType>();
    static {
        for (PotionEffectType p : badeffects)
            badeffectslist.add(p);
    }
    
    public static void applyVulnerability(final Entity victim, final int duration, final int amplitude) {
        victim.setMetadata("vulnerability", new FixedMetadataValue(Main.getPlugin(), amplitude));
        final BukkitTask task = new BukkitRunnable() {
            public void run() {
                if (!victim.isDead()) {
                    for (int i = 0; i < 4; i++)
                        ParticleEffects.MOB_SPELL.display(new OrdinaryColor(0, 0, 0), victim.getLocation(), 256);
                }
            }
        }.runTaskTimer(Main.getPlugin(), 0L, 5L);
        victim.setMetadata("vulnerabilityparticles", new FixedMetadataValue(Main.getPlugin(), task.getTaskId()));
        new BukkitRunnable() {
            public void run() {
                task.cancel();
                if (victim.hasMetadata("vulnerability")) {
                    if (victim instanceof Player) {
                        if (!((Player) victim).isOnline())
                            return;
                    }
                    if (!victim.isDead()) {
                        victim.removeMetadata("vulnerability", Main.getPlugin());
                    }
                }
            }
        }.runTaskLater(Main.getPlugin(), duration * 20L);
    }
    
    public static boolean notifyCooledDown(final Player p, double d, final String reason) {
        if (p.hasMetadata("cooldown." + reason)) {
            new ActionBar().sendMessage(p, "�c" + reason + " Ability recharging " + "�6�l> " + ChatColor.RESET + "" + ChatColor.WHITE + df.format(((p.getMetadata("cooldown." + reason).get(0).asLong() + d*1000) - System.currentTimeMillis())/1000) + ChatColor.GRAY + " seconds");
            p.playSound(p.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0F, 1.0F);
            return false;
        }
        p.setMetadata("cooldown." + reason, new FixedMetadataValue(Main.getPlugin(), System.currentTimeMillis()));
        new BukkitRunnable() {
            public void run() {
                if (p.isOnline()) {
                    new ActionBar().sendMessage(p, "�a" + reason + " Ability �fhas finished recharging!");
                    p.playSound(p.getLocation(), Sound.BLOCK_NOTE_PLING, 0.2F, 1.6F);
                    p.removeMetadata("cooldown." + reason, Main.getPlugin());
                }
            }
        }.runTaskLater(Main.getPlugin(), (long) (d * 20L));
        return true;
    }
}
